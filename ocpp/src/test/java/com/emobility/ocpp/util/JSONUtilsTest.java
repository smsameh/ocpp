package com.emobility.ocpp.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.emobility.ocpp.socket.message.model.AuthorizationRequestPayload;
import com.emobility.ocpp.socket.message.model.AvailabilityStatus;
import com.emobility.ocpp.socket.message.model.AvailabilityType;
import com.emobility.ocpp.socket.message.model.BootNotificationRequestPayload;
import com.emobility.ocpp.socket.message.model.ChangeAvailabilityConfirmPayload;
import com.emobility.ocpp.socket.message.model.ChangeAvailabilityRequestPayload;
import com.emobility.ocpp.socket.message.model.ChargePointStatus;
import com.emobility.ocpp.socket.message.model.ChargingProfile;
import com.emobility.ocpp.socket.message.model.ChargingProfileKindType;
import com.emobility.ocpp.socket.message.model.ChargingProfilePurposeType;
import com.emobility.ocpp.socket.message.model.ChargingProfileStatus;
import com.emobility.ocpp.socket.message.model.ChargingRateUnitType;
import com.emobility.ocpp.socket.message.model.ChargingSchedule;
import com.emobility.ocpp.socket.message.model.ChargingSchedulePeriod;
import com.emobility.ocpp.socket.message.model.DataTransferConfirmPayload;
import com.emobility.ocpp.socket.message.model.DataTransferRequestPayload;
import com.emobility.ocpp.socket.message.model.DataTransferStatus;
import com.emobility.ocpp.socket.message.model.DiagnosticsStatus;
import com.emobility.ocpp.socket.message.model.DiagnosticsStatusNotificationRequestPayload;
import com.emobility.ocpp.socket.message.model.FirmwareStatus;
import com.emobility.ocpp.socket.message.model.FirmwareStatusNotificationRequestPayload;
import com.emobility.ocpp.socket.message.model.IdToken;
import com.emobility.ocpp.socket.message.model.Location;
import com.emobility.ocpp.socket.message.model.Measurand;
import com.emobility.ocpp.socket.message.model.MeterValue;
import com.emobility.ocpp.socket.message.model.MeterValuesRequestPayload;
import com.emobility.ocpp.socket.message.model.Phase;
import com.emobility.ocpp.socket.message.model.Reason;
import com.emobility.ocpp.socket.message.model.SampledValue;
import com.emobility.ocpp.socket.message.model.SetChargingProfileConfirmPayload;
import com.emobility.ocpp.socket.message.model.SetChargingProfileRequestPayload;
import com.emobility.ocpp.socket.message.model.StartTransactionRequestPayload;
import com.emobility.ocpp.socket.message.model.StatusNotificationRequestPayload;
import com.emobility.ocpp.socket.message.model.StopTransactionRequestPayload;
import com.emobility.ocpp.socket.message.model.UnitOfMeasure;
import com.emobility.ocpp.socket.message.model.ValueFormat;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * JSON Utilies test
 * 
 * @author Salah Abu Msameh
 */
public class JSONUtilsTest {

	@Test
	public void testAuthorizationPayloadParsing() {
		
		//authorization
		IdToken token = new IdToken("user001");
		AuthorizationRequestPayload authReq = new AuthorizationRequestPayload(token);
		
		assertEquals("{\"idToken\":{\"idToken\":\"user001\"}}", JSONUtils.parse(authReq));
		assertNotNull(JSONUtils.parse("{\"idToken\":{\"idToken\":\"token-value\"}}", AuthorizationRequestPayload.class));
		
		//Boot notification 
		BootNotificationRequestPayload bootNotificationReq = new BootNotificationRequestPayload();
		
		bootNotificationReq.setChargeBoxSerialNumber("CPBOXSRL10219555452");
		bootNotificationReq.setChargePointModel("cp-model-001");
		bootNotificationReq.setChargePointVendor("cp-vendor-x");
		bootNotificationReq.setChargePointSerialNumber("SRL5989892952566");
		bootNotificationReq.setFirmwareVersion("1.0");
		
		assertEquals("{\"chargeBoxSerialNumber\":\"CPBOXSRL10219555452\",\"chargePointModel\":\"cp-model-001\","
				+ "\"chargePointSerialNumber\":\"SRL5989892952566\",\"chargePointVendor\":\"cp-vendor-x\",\"firmwareVersion\":\"1.0\"}", JSONUtils.parse(bootNotificationReq));
		assertNotNull(JSONUtils.parse("{\"chargeBoxSerialNumber\":\"CPBOXSRL10219555452\",\"chargePointModel\":\"cp-model-001\","
				+ "\"chargePointSerialNumber\":\"SRL5989892952566\",\"chargePointVendor\":\"cp-vendor-x\",\"firmwareVersion\":\"1.0\"}", BootNotificationRequestPayload.class));
		
	}
	
	/**
	 * Test stop transaction json payload parsing 
	 */
	@Test
	public void testDataTransferPayloadParsing() {
		
		DataTransferRequestPayload dataTransferReq = new DataTransferRequestPayload();
		
		dataTransferReq.setMessageId("123456");
		dataTransferReq.setVendorId("ven-001");
		dataTransferReq.setData("AddNewOperation");
		
		assertEquals("{\"vendorId\":\"ven-001\",\"messageId\":\"123456\",\"data\":\"AddNewOperation\"}", JSONUtils.parse(dataTransferReq));
		assertNotNull(JSONUtils.parse("{\"vendorId\":\"ven-001\",\"messageId\":\"123456\",\"data\":\"AddNewOperation\"}", DataTransferRequestPayload.class));
		
		DataTransferConfirmPayload dataTransferConfirm = new DataTransferConfirmPayload();
		
		dataTransferConfirm.setData("AddNewOperation");
		dataTransferConfirm.setStatus(DataTransferStatus.Accepted);
		
		assertEquals("{\"status\":\"Accepted\",\"data\":\"AddNewOperation\"}", JSONUtils.parse(dataTransferConfirm));
		assertNotNull(JSONUtils.parse("{\"status\":\"Accepted\",\"data\":\"AddNewOperation\"}", DataTransferConfirmPayload.class));
	}
	
	@Test
	public void testDiagnosticsStatusNotificationPayloadParsing() {
		
		DiagnosticsStatusNotificationRequestPayload diagnosticsPayload = new DiagnosticsStatusNotificationRequestPayload(DiagnosticsStatus.Uploaded);
		
		assertEquals("{\"diagnosticsStatus\":\"Uploaded\"}", JSONUtils.parse(diagnosticsPayload));
		assertNotNull(JSONUtils.parse("{\"diagnosticsStatus\":\"Uploaded\"}", DiagnosticsStatusNotificationRequestPayload.class));
	}
	
	@Test
	public void testFirmwareStatusNotificationPayloadParsing() {
		
		FirmwareStatusNotificationRequestPayload firmwaresPayload = new FirmwareStatusNotificationRequestPayload(FirmwareStatus.Downloaded);
		
		assertEquals("{\"firmwareStatus\":\"Downloaded\"}", JSONUtils.parse(firmwaresPayload));
		assertNotNull(JSONUtils.parse("{\"firmwareStatus\":\"Downloaded\"}", DiagnosticsStatusNotificationRequestPayload.class));
	}
	
	@Test
	public void testMeterValuePayloadParsing() {
		
		MeterValuesRequestPayload meterValuePayload = new MeterValuesRequestPayload();
		
		meterValuePayload.setConnectorId(0);
		meterValuePayload.setTransactionId(123456);
		
		MeterValue mv = new MeterValue();
		SampledValue sv = new SampledValue();
		
		sv.setValue("62.025");
		sv.setLocation(Location.Body);
		sv.setMeasurand(Measurand.Current_Offered);
		sv.setPhase(Phase.L1);
		sv.setUnit(UnitOfMeasure.Celsius);
		sv.setValueFormat(ValueFormat.Raw);
		
		mv.setTimestamp(1512742262229L);
		mv.setSampleValue(sv);
		
		meterValuePayload.setMeterValue(mv);
		
		assertEquals("{\"connectorId\":0,\"transactionId\":123456,\"meterValue\":{\"timestamp\":1512742262229,"
				+ "\"sampleValue\":{\"value\":\"62.025\",\"valueFormat\":\"Raw\",\"measurand\":\"Current_Offered\","
				+ "\"phase\":\"L1\",\"location\":\"Body\",\"unit\":\"Celsius\"}}}", JSONUtils.parse(meterValuePayload));
		
		assertNotNull(JSONUtils.parse("{\"connectorId\":0,\"transactionId\":123456,\"meterValue\":{\"timestamp\":1512742262229,"
				+ "\"sampleValue\":{\"value\":\"62.025\",\"valueFormat\":\"Raw\",\"measurand\":\"Current_Offered\","
				+ "\"phase\":\"L1\",\"location\":\"Body\",\"unit\":\"Celsius\"}}}", MeterValuesRequestPayload.class));
	}
	
	/**
	 * Test start transaction json payload parsing 
	 */
	@Test
	public void testStartTransactionPayloadParsing() {
		
		StartTransactionRequestPayload startTransReq = new StartTransactionRequestPayload();
		
		startTransReq.setConnectorId(0);
		startTransReq.setIdToken(new IdToken("user001"));
		startTransReq.setMeterStart(152);
		startTransReq.setTimestamp(1512743462229L);
		startTransReq.setReservationId(99123598);
		
		assertEquals("{\"connectorId\":0,\"idToken\":{\"idToken\":\"user001\"},\"meterStart\":152,"
				+ "\"reservationId\":99123598,\"timestamp\":1512743462229}", JSONUtils.parse(startTransReq));
		assertNotNull(JSONUtils.parse("{\"connectorId\":0,\"idToken\":{\"idToken\":\"token-id\"},\"meterStart\":0,"
				+ "\"reservationId\":99123598,\"timestamp\":1512743462229}", StartTransactionRequestPayload.class));
	}
	
	@Test
	public void testStatusNotificationPayloadParsing() {
		
		StatusNotificationRequestPayload statusNotifPayload = new StatusNotificationRequestPayload();
		
		statusNotifPayload.setConnectorId(0);
		statusNotifPayload.setInfo("info");
		statusNotifPayload.setStatus(ChargePointStatus.Available);
		statusNotifPayload.setVendorId("vendor-X");
		statusNotifPayload.setTimestamp(1512803462229L);
		
		assertEquals("{\"connectorId\":0,\"info\":\"info\",\"status\":\"Available\",\"timestamp\":1512803462229,\"vendorId\":\"vendor-X\"}", 
				JSONUtils.parse(statusNotifPayload));
		assertNotNull(JSONUtils.parse("{\"connectorId\":0,\"info\":\"info\",\"status\":\"Available\",\"timestamp\":1512803462229,\"vendorId\":\"vendor-X\"}", 
				StatusNotificationRequestPayload.class));
	}
	
	/**
	 * Test stop transaction json payload parsing 
	 */
	@Test
	public void testStopTransactionPayloadParsing() {
		
		StopTransactionRequestPayload stopTransReq = new StopTransactionRequestPayload();
		
		stopTransReq.setIdToken(new IdToken("user001"));
		stopTransReq.setMeterStop(5165454);
		stopTransReq.setReason(new Reason(1));
		stopTransReq.setTransactionId(454545);
		stopTransReq.setTimestamp(1512749552229L);
		
		assertEquals("{\"idToken\":{\"idToken\":\"user001\"},\"meterStop\":5165454,\"timestamp\":1512749552229,"
				+ "\"transactionId\":454545,\"reason\":{\"reasonCode\":1}}", JSONUtils.parse(stopTransReq));
		assertNotNull(JSONUtils.parse("{\"idToken\":{\"idToken\":\"token-id\"},\"meterStop\":5165454,\"timestamp\":1512749552229,"
				+ "\"transactionId\":454545,\"reason\":{\"reasonCode\":1}}", StopTransactionRequestPayload.class));
	}
	
	@Test
	public void testChangeAvailability() {
		
		ChangeAvailabilityRequestPayload reqPayload = new ChangeAvailabilityRequestPayload(0, AvailabilityType.Operative);
		
		assertEquals("{\"connectorId\":0,\"type\":\"Operative\"}", JSONUtils.parse(reqPayload));
		assertNotNull(JSONUtils.parse("{\"connectorId\":0,\"type\":\"Operative\"}", ChangeAvailabilityRequestPayload.class));
		
		ChangeAvailabilityConfirmPayload confPayload = new ChangeAvailabilityConfirmPayload(AvailabilityStatus.Accepted);
		
		assertEquals("{\"status\":\"Accepted\"}", JSONUtils.parse(confPayload));
		assertNotNull(JSONUtils.parse("{\"status\":\"Accepted\"}", ChangeAvailabilityConfirmPayload.class));
	}
	
	@Test
	public void testRemoteStartTransaction() {
		
		SetChargingProfileRequestPayload reqPayload = new SetChargingProfileRequestPayload();
		
		reqPayload.setConnectorId(0);
		reqPayload.setChargingProfile(getTestChargingProfile());
		
		assertEquals("{\"connectorId\":0,\"chargingProfile\":{\"chargingProfileId\":1,\"transactionId\":0,\"stackLevel\":0,"
				+ "\"chargingProfilePurpose\":\"TxProfile\",\"chargingProfileKind\":\"Relative\","
				+ "\"chargingSchedule\":{\"duration\":0,\"startSchedule\":0,\"chargingRateUnit\":\"W\","
				+ "\"chargingSchedulePeriod\":{\"startPeriod\":100,\"limit\":200.0,\"numberPhases\":0},\"minChargingRate\":0.0}}}", JSONUtils.parse(reqPayload));
		assertNotNull(JSONUtils.parse("{\"connectorId\":0,\"chargingProfile\":{\"chargingProfileId\":1,\"transactionId\":0,"
				+ "\"stackLevel\":0,\"chargingProfilePurpose\":\"TxProfile\",\"chargingProfileKind\":\"Relative\","
				+ "\"chargingSchedule\":{\"duration\":0,\"startSchedule\":0,\"chargingRateUnit\":\"W\","
				+ "\"chargingSchedulePeriod\":{\"startPeriod\":100,\"limit\":200.0,\"numberPhases\":0},\"minChargingRate\":0.0}}}", SetChargingProfileRequestPayload.class));
		
		SetChargingProfileConfirmPayload confPayload = new SetChargingProfileConfirmPayload(ChargingProfileStatus.Accepted);
		
		assertEquals("{\"status\":\"Accepted\"}", JSONUtils.parse(confPayload));
		assertNotNull(JSONUtils.parse("{\"status\":\"Accepted\"}", SetChargingProfileConfirmPayload.class));
	}
	
	/**
	 * 
	 * @return
	 */
	private ChargingProfile getTestChargingProfile() {
		
		ChargingProfile chargingProfile = new ChargingProfile();
		
		chargingProfile.setChargingProfileId(1);
		chargingProfile.setChargingProfilePurpose(ChargingProfilePurposeType.TxProfile);
		chargingProfile.setChargingProfileKind(ChargingProfileKindType.Relative);
		
		ChargingSchedule chargeSchedule = new ChargingSchedule();
		
		chargeSchedule.setChargingRateUnit(ChargingRateUnitType.W);
		chargeSchedule.setChargingSchedulePeriod(new ChargingSchedulePeriod(100, 200, 0));
		
		chargingProfile.setChargingSchedule(chargeSchedule);
		
		return chargingProfile;
	}

	@Test
	public void testGetJsonAttKeyValue() {
		
		String json1 = "{\r\n" + 
				"	\"id\": \"0001\",\r\n" + 
				"	\"type\": \"donut\",\r\n" + 
				"	\"name\": \"Cake\",\r\n" + 
				"	\"ppu\": 0.55,\r\n" + 
				"	\"batters\":\r\n" + 
				"		{\r\n" + 
				"			\"batter\":\r\n" + 
				"				[\r\n" + 
				"					{ \"id1\": \"1001\", \"type1\": \"Regular\" },\r\n" + 
				"					{ \"id2\": \"1002\", \"type2\": \"Chocolate\" },\r\n" + 
				"					{ \"id3\": \"1003\", \"type3\": \"Blueberry\" },\r\n" + 
				"					{ \"id4\": \"1004\", \"type4\": \"Devil-Food\" }\r\n" + 
				"				]\r\n" + 
				"		},\r\n" + 
				"	\"topping\":\r\n" + 
				"		[\r\n" + 
				"			{ \"id5\": \"5001\", \"type5\": \"None\" },\r\n" + 
				"			{ \"id6\": \"5002\", \"type6\": \"Glazed\" },\r\n" + 
				"			{ \"id7\": \"5005\", \"type4\": \"Sugar\" },\r\n" + 
				"			{ \"id8\": \"5007\", \"type8\": \"Powdered Sugar\" },\r\n" + 
				"			{ \"id9\": \"5006\", \"type9\": \"Chocolate with Sprinkles\" },\r\n" + 
				"			{ \"id10\": \"5003\", \"type10\": \"Chocolate\" },\r\n" + 
				"			{ \"id11\": \"5004\", \"type4\": \"Maple\" }\r\n" + 
				"		]\r\n" + 
				"}";
		
		JsonElement jsonElement = new JsonParser().parse(json1);
		List<String> values = new ArrayList<String>();
		JSONUtils.getJsonAttKeyValue(jsonElement,"type4", values);
		
		assertEquals(3, values.size());
		
		String json2 = "[\r\n" + 
				"	{\r\n" + 
				"		\"id\": \"0001\",\r\n" + 
				"		\"type\": \"donut\",\r\n" + 
				"		\"name\": \"Cake\",\r\n" + 
				"		\"ppu\": 0.55,\r\n" + 
				"		\"batters\":\r\n" + 
				"			{\r\n" + 
				"				\"batter\":\r\n" + 
				"					[\r\n" + 
				"						{ \"id\": \"1001\", \"type\": \"Regular\" },\r\n" + 
				"						{ \"id\": \"1002\", \"type\": \"Chocolate\" },\r\n" + 
				"						{ \"id\": \"1003\", \"type\": \"Blueberry\" },\r\n" + 
				"						{ \"id\": \"1004\", \"type\": \"Devil's Food\" }\r\n" + 
				"					]\r\n" + 
				"			},\r\n" + 
				"		\"topping\":\r\n" + 
				"			[\r\n" + 
				"				{ \"id\": \"5001\", \"type\": \"None\" },\r\n" + 
				"				{ \"id\": \"5002\", \"type\": \"Glazed\" },\r\n" + 
				"				{ \"id\": \"5005\", \"type\": \"Sugar\" },\r\n" + 
				"				{ \"id\": \"5007\", \"type\": \"Powdered Sugar\" },\r\n" + 
				"				{ \"id\": \"5006\", \"type\": \"Chocolate with Sprinkles\" },\r\n" + 
				"				{ \"id\": \"5003\", \"type\": \"Chocolate\" },\r\n" + 
				"				{ \"id\": \"5004\", \"type\": \"Maple\" }\r\n" + 
				"			]\r\n" + 
				"	},\r\n" + 
				"	{\r\n" + 
				"		\"id\": \"0002\",\r\n" + 
				"		\"type\": \"donut\",\r\n" + 
				"		\"name\": \"Raised\",\r\n" + 
				"		\"ppu\": 0.55,\r\n" + 
				"		\"batters\":\r\n" + 
				"			{\r\n" + 
				"				\"batter\":\r\n" + 
				"					[\r\n" + 
				"						{ \"id\": \"1001\", \"type\": \"Regular\" }\r\n" + 
				"					]\r\n" + 
				"			},\r\n" + 
				"		\"topping\":\r\n" + 
				"			[\r\n" + 
				"				{ \"id\": \"5001\", \"type\": \"None\" },\r\n" + 
				"				{ \"id\": \"5002\", \"type\": \"Glazed\" },\r\n" + 
				"				{ \"id\": \"5005\", \"type\": \"Sugar\" },\r\n" + 
				"				{ \"id\": \"5003\", \"type\": \"Chocolate\" },\r\n" + 
				"				{ \"id\": \"5004\", \"type\": \"Maple\" }\r\n" + 
				"			]\r\n" + 
				"	},\r\n" + 
				"	{\r\n" + 
				"		\"id\": \"0003\",\r\n" + 
				"		\"type\": \"donut\",\r\n" + 
				"		\"name\": \"Old Fashioned\",\r\n" + 
				"		\"ppu\": 0.55,\r\n" + 
				"		\"batters\":\r\n" + 
				"			{\r\n" + 
				"				\"batter\":\r\n" + 
				"					[\r\n" + 
				"						{ \"id\": \"1001\", \"type\": \"Regular\" },\r\n" + 
				"						{ \"id\": \"1002\", \"type\": \"Chocolate\" }\r\n" + 
				"					]\r\n" + 
				"			},\r\n" + 
				"		\"topping\":\r\n" + 
				"			[\r\n" + 
				"				{ \"id\": \"5001\", \"type\": \"None\" },\r\n" + 
				"				{ \"id\": \"5002\", \"type\": \"Glazed\" },\r\n" + 
				"				{ \"id\": \"5003\", \"type\": \"Chocolate\" },\r\n" + 
				"				{ \"id\": \"5004\", \"type\": \"Maple\" }\r\n" + 
				"			]\r\n" + 
				"	}\r\n" + 
				"]";
		
		JsonElement jsonElement2 = new JsonParser().parse(json2);
		List<String> values2 = new ArrayList<String>();
		JSONUtils.getJsonAttKeyValue(jsonElement2,"type", values2);
		
		assertEquals(26, values2.size());
	}
	
	@Test
	public void testGetJsonAttKeyValues() {
		
		String json1 = "{\r\n" + 
				"	\"key\": [\"key1\", \"key2\", \"key3\"]\r\n" + 
				"}";
		
		JsonElement jsonElement = new JsonParser().parse(json1);
		List<String> values = new ArrayList<String>();
		JSONUtils.getJsonAttKeyValue(jsonElement,"key", values);
		
		assertEquals(3, values.size());
	}
}
