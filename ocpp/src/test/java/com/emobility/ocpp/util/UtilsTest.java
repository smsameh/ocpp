package com.emobility.ocpp.util;

import static org.junit.Assert.assertEquals;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

/**
 * Utilities test class
 * 
 * @author Salah Abu Msameh
 */
public class UtilsTest {
	
	@Test
	public void testExtractConnectionUniqueIdentifier() {
		
		assertEquals("", Utils.extractConnectionUniqueIdentifier(null));
		assertEquals("", Utils.extractConnectionUniqueIdentifier("ws://mywebsite:8080/register/main"));
		assertEquals("cp-simulator", Utils.extractConnectionUniqueIdentifier("ws://localhost:8080/ocpp/cp-simulator"));
	}

	@Test
	public void testExtractAction() {
		
		String s = "[2,\r\n" + 
				"\"19223201\",\r\n" + 
				"\"BootNotification\",\r\n" + 
				"{\"chargePointVendor\": \"VendorX\", \"chargePointModel\": \"SingleSocketCharger\"}\r\n" + 
				"]";
		
		assertEquals("BootNotification", Utils.extractAction(s));
	}
	
	@Test
	public void testGenerateTransactionId() {
		
		Set<Integer> trxIds = new TreeSet<Integer>();
		
		for(int i = 0; i < 500; i++) {
			trxIds.add(Utils.generateTransactionId());
		}
		
		assertEquals(500, trxIds.size());
	}
}
