package com.emobility.ocpp.socket.message.parse;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageType;
import com.emobility.ocpp.socket.message.model.AuthorizationRequestPayload;
import com.emobility.ocpp.socket.message.model.BootNotificationRequestPayload;
import com.emobility.ocpp.socket.message.validate.JSONMessageValidator;

/**
 * 
 * @author Salah Abu Msameh
 */
public class JSONMessageParserTest {
	
	private static JSONMessageParser jsonMsgParser = new JSONMessageParser();
	
	static {
		jsonMsgParser.setMsgValidator(new JSONMessageValidator());
	}

	@Test
	public void testParseMessage() {
		
		//Boot Notification request case
		String message = "[2, \"5555555\", \"BootNotification\", {\"chargeBoxSerialNumber\":\"CPBOXSRL10219555452\","
                + "\"chargePointModel\":\"cp-model-001\",\"chargePointSerialNumber\":\"SRL5989892952566\","
                + "\"chargePointVendor\":\"cp-vendor-x\",\"firmwareVersion\":\"1.0\"}]";
		
		Message msg = jsonMsgParser.parse(message);
		
		assertEquals(MessageType.getTypeById(2), msg.getMessageType());
		assertEquals("5555555", msg.getUniqueId());
		
		assertEquals("cp-vendor-x", ((BootNotificationRequestPayload) msg.getPayload()).getChargePointVendor());
		
		//Authentication request message
		Message authMsg = jsonMsgParser.parse("[2, \"6666666\", \"Authorize\", {\"idToken\":{\"idToken\":\"user-token-value\"}}]");
		
		assertEquals(MessageType.getTypeById(2), authMsg.getMessageType());
		assertEquals("6666666", authMsg.getUniqueId());
		
		assertEquals("user-token-value", ((AuthorizationRequestPayload) authMsg.getPayload()).getIdToken().getIdToken());
	}
}
