package com.emobility.ocpp.util;

import org.apache.commons.logging.LogFactory;

/**
 * Central log class 
 * 
 * @author Salah Abu Msameh
 */
public class Log {

	/**
	 * Log given info message 
	 * 
	 * @param clazz
	 * @param infoMessage
	 */
	public static void info(Class<?> clazz, String infoMessage) {
		getLogger(clazz).info(infoMessage);
	}
	
	/**
	 * Log given info message 
	 * 
	 * @param clazz
	 * @param infoMessage
	 */
	public static void debug(Class<?> clazz, String debugMessage) {
		getLogger(clazz).info(debugMessage);
	}
	
	/**
	 * Log given info message 
	 * 
	 * @param clazz
	 * @param infoMessage
	 */
	public static void error(Class<?> clazz, String errorMessage) {
		getLogger(clazz).info(errorMessage);
	}
	
	/**
	 * 
	 * @param clazz
	 * @return
	 */
	private static org.apache.commons.logging.Log getLogger(Class<?> clazz) {
		return LogFactory.getLog(clazz);
	}
}
