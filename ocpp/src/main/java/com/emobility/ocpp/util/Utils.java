package com.emobility.ocpp.util;

import java.util.Date;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.util.StringUtils;

/**
 * Utilities methods
 * 
 * @author Salah Abu Msameh
 */
public class Utils {
	
	private static final String OCPP_URL_PART = "/ocpp/";
	
	private static Random random = new Random();
	private static Set<Integer> transactionIds = new TreeSet<Integer>();
	
	/**
	 * 
	 * @param connectionUrl
	 * @return
	 */
	public static String extractConnectionUniqueIdentifier(String connectionUrl) {
		
		if(StringUtils.isEmpty(connectionUrl)) {
			return "";
		}
		
		int startIndex = connectionUrl.indexOf(OCPP_URL_PART);
		
		if(startIndex == -1) {
			return "";
		}
		
		return connectionUrl.substring(startIndex + OCPP_URL_PART.length(), connectionUrl.length());
	}

	/**
	 * Extracts the action from the given web socket message
	 * 
	 * @param message
	 * @return
	 */
	public static String extractAction(String message) {
		
		String[] values = message.split(",");
		String action = values[2];
		
		action = action.substring(action.indexOf("\"") + 1);
		action = action.substring(0, action.indexOf("\""));
		
		return action;
	}
	
	/**
	 * 
	 * @return
	 */
	public static String generateMessageId() {
		return String.valueOf(new Date().getTime());
	}
	
	/**
	 * Generate a unique transaction id as int
	 * @return
	 */
	public static int generateTransactionId() {
		
		int trxId = 0;
		
		do {
			trxId = random.nextInt(Integer.MAX_VALUE - 10) + 10;
		} while(transactionIds.contains(trxId));
		
		transactionIds.add(trxId);
		
		return trxId;
	}
}
