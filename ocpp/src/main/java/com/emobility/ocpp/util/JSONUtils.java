package com.emobility.ocpp.util;

import java.io.StringReader;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;

/**
 * JSON Related utilities
 * 
 * @author Salah Abu Msameh
 */
public class JSONUtils {

	/**
	 * parse given object to json string
	 * 
	 * @param jsonObj
	 * @return
	 */
	public static <T> String parse(Object jsonObj) {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(jsonObj);
	}
	
	/**
	 * Parse given string to the given object type
	 * 
	 * @param jsonString
	 * @param type
	 * @return
	 */
	public static <T> T parse(String jsonString, Class<T> type) {
		
		Gson gson = new GsonBuilder().create();
		JsonReader reader = new JsonReader(new StringReader(jsonString));
		reader.setLenient(true);
		
		return gson.fromJson(reader, type);
	}
	
	/**
	 * find all values of the give json attribute key
	 * 
	 * @param jsonElement
	 * @param jsonAttributeKey
	 * @return
	 */
	public static void getJsonAttKeyValue(JsonElement jsonElement, String jsonAttributeKey, List<String> values) {
		
		if(jsonElement.isJsonArray()) {
			for(JsonElement subElement : jsonElement.getAsJsonArray()) {
				getJsonAttKeyValue(subElement, jsonAttributeKey, values);
			}
		} else {
			
			//get json object entries 
			Set<Entry<String, JsonElement>> entrySet = jsonElement.getAsJsonObject().entrySet();
			
	        for (Entry<String, JsonElement> entry : entrySet) {
	        	
	        	String key = entry.getKey();
	        	JsonElement subElelemt = entry.getValue();
	        	
	        	if(key.equals(jsonAttributeKey) && subElelemt.isJsonArray()) {
	        		for(JsonElement je : subElelemt.getAsJsonArray()) {
	        			if(je.isJsonPrimitive()) {
	        				values.add(je.getAsString());
	        			}
	        		}
	        	} else if(!subElelemt.isJsonPrimitive()) {
	        		getJsonAttKeyValue(subElelemt, jsonAttributeKey, values);
	        	} else if (entry.getKey().equals(jsonAttributeKey)) {
	                values.add(subElelemt.getAsString());
	            }
	        }
		}
	}
}
