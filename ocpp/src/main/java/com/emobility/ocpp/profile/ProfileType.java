package com.emobility.ocpp.profile;

/**
 * Different profile types
 * 
 * @author Salah Abu Msameh
 */
public enum ProfileType {
	CORE,
	FIRMWARE_MANAGMENT,
	LOCAL_AUTH_LIST_MANAGMENT,
	CHARGE_POINTS,
	RESERVATION,
	SMART_CHARGING,
	REMOTE_TRIGGER
}
