package com.emobility.ocpp.profile.config;

import com.emobility.ocpp.profile.operation.AuthorizationOperation;
import com.emobility.ocpp.profile.operation.BootNotificationOperation;
import com.emobility.ocpp.profile.operation.CancelReservationOperation;
import com.emobility.ocpp.profile.operation.ChangeAvailabilityOperation;
import com.emobility.ocpp.profile.operation.ChangeConfigurationOperation;
import com.emobility.ocpp.profile.operation.ClearCacheOperation;
import com.emobility.ocpp.profile.operation.ClearChargingProfileOperation;
import com.emobility.ocpp.profile.operation.DataTransferOperartion;
import com.emobility.ocpp.profile.operation.DiagnosticsStatusNotificationOperation;
import com.emobility.ocpp.profile.operation.FirmwareStatusNotificationOperation;
import com.emobility.ocpp.profile.operation.GetCompositeScheduleOperation;
import com.emobility.ocpp.profile.operation.GetConfigurationOperation;
import com.emobility.ocpp.profile.operation.GetDiagnosticsOperation;
import com.emobility.ocpp.profile.operation.GetLocalListVersionOperation;
import com.emobility.ocpp.profile.operation.HeartbeatOperation;
import com.emobility.ocpp.profile.operation.MeterValuesOperation;
import com.emobility.ocpp.profile.operation.RemoteStartTransactionOperation;
import com.emobility.ocpp.profile.operation.RemoteStopTransactionOperation;
import com.emobility.ocpp.profile.operation.ReserveNowOperation;
import com.emobility.ocpp.profile.operation.ResetOperation;
import com.emobility.ocpp.profile.operation.SendLocalListOperation;
import com.emobility.ocpp.profile.operation.SetChargingProfileOperation;
import com.emobility.ocpp.profile.operation.StartTransactionOperation;
import com.emobility.ocpp.profile.operation.StatusNotificationOperation;
import com.emobility.ocpp.profile.operation.StopTransactionOperation;
import com.emobility.ocpp.profile.operation.TriggerMessageOperation;
import com.emobility.ocpp.profile.operation.UnlockConnectorOperation;
import com.emobility.ocpp.profile.operation.UpdateFirmwareOperation;

/**
 * 
 * @author Salah Abu Msameh
 */
//@Configuration
public abstract class OperationsBeansConfig {

	/**
	 * Initialize and return {@link CancelReservationOperation} object
	 * @return
	 */
	public abstract CancelReservationOperation getCancelReservationOperation();
	
	/**
	 * Initialize and return {@link AuthorizationOperation} object
	 * @return
	 */
	public abstract AuthorizationOperation getAuthorizationConfirmOperation();
	
	/**
	 * Initialize and return {@link BootNotificationOperation} object
	 * @return
	 */
	public abstract BootNotificationOperation getBootNotificationOperation();
	
	/**
	 * Initialize and return {@link DataTransferOperartion} object 
	 * @return
	 */
	public abstract DataTransferOperartion getDataTransferOperartion();
	
	/**
	 * Initialize and return {@link StartTransactionOperation} object
	 * @return
	 */
	public abstract StartTransactionOperation getStartTransactionOperation();
	
	/**
	 * Initialize and return {@link StatusNotificationOperation} object
	 * @return
	 */
	public abstract StatusNotificationOperation getStatusNotificationOperation();
	
	/**
	 * Initialize and return {@link StopTransactionOperation} object
	 * @return
	 */
	public abstract StopTransactionOperation getStopTransactionOperation();
	
	/**
	 * Initialize and return {@link HeartbeatOperation} object
	 * @return
	 */
	public abstract HeartbeatOperation getHeartbeatOperation();
	
	/**
	 * Initialize and return {@link DiagnosticsStatusNotificationOperation} object
	 * @return
	 */
	public abstract DiagnosticsStatusNotificationOperation getDiagnosticsStatusNotificationOperation();
	
	/**
	 * Initialize and return {@link FirmwareStatusNotificationOperation} object
	 * @return
	 */
	public abstract FirmwareStatusNotificationOperation getFirmwareStatusNotificationOperation();
	
	/**
	 * Initialize and return {@link MeterValuesOperation} object
	 * @return
	 */
	public abstract MeterValuesOperation getMeterValuesOperation();
	
	/**
	 * Initialize and return {@link RemoteStartTransactionOperation} object
	 * @return
	 */
	public abstract RemoteStartTransactionOperation getRemoteStartTransactionOperation();
	
	/**
	 * Initialize and return {@link RemoteStopTransactionOperation} object
	 * @return
	 */
	public abstract RemoteStopTransactionOperation getRemoteStopTransactionOperation();
	
	/**
	 * Initialize and return {@link ChangeAvailabilityOperation} object
	 * @return
	 */
	public abstract ChangeAvailabilityOperation getChangeAvailabilityOperation();
	
	/**
	 * Initialize and return {@link SetChargingProfileOperation} object
	 * @return
	 */
	public abstract SetChargingProfileOperation getSetChargingProfileOperation();
	
	/**
	 * Initialize and return {@link ChangeConfigurationOperation} object
	 * @return
	 */
	public abstract ChangeConfigurationOperation getChangeConfigurationOperation();
	
	/**
	 * Initialize and return {@link ClearCacheOperation} object
	 * @return
	 */
	public abstract ClearCacheOperation getClearCacheOperation();
	
	/**
	 * Initialize and return {@link ClearChargingProfileOperation} object
	 * @return
	 */
	public abstract ClearChargingProfileOperation getClearChargingProfileOperation();
	
	/**
	 * Initialize and return {@link GetCompositeScheduleOperation} object
	 * @return
	 */
	public abstract GetCompositeScheduleOperation getGetCompositeScheduleOperation();
	
	/**
	 * Initialize and return {@link GetConfigurationOperation} object
	 * @return
	 */
	public abstract GetConfigurationOperation getGetConfigurationOperation();
	
	/**
	 * Initialize and return {@link GetDiagnosticsOperation} object
	 * @return
	 */
	public abstract GetDiagnosticsOperation getGetDiagnosticsOperation();
	
	/**
	 * Initialize and return {@link GetLocalListVersionOperation} object
	 * @return
	 */
	public abstract GetLocalListVersionOperation getGetLocalListVersionOperation();
	
	/**
	 * Initialize and return {@link ReserveNowOperation} object
	 * @return
	 */
	public abstract ReserveNowOperation getReserveNowOperation();
	
	/**
	 * Initialize and return {@link ResetOperation} object
	 * @return
	 */
	public abstract ResetOperation getResetOperation();
	
	/**
	 * Initialize and return {@link SendLocalListOperation} object
	 * @return
	 */
	public abstract SendLocalListOperation getSendLocalListOperation();
	
	/**
	 * Initialize and return {@link TriggerMessageOperation} object
	 * @return
	 */
	public abstract TriggerMessageOperation getTriggerMessageOperation();
	
	/**
	 * Initialize and return {@link UnlockConnectorOperation} object
	 * @return
	 */
	public abstract UnlockConnectorOperation getUnlockConnectorOperation();
	
	/**
	 * Initialize and return {@link UpdateFirmwareOperation} object
	 * @return
	 */
	public abstract UpdateFirmwareOperation getUpdateFirmwareOperation();
}
