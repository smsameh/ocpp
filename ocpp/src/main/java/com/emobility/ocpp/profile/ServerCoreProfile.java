package com.emobility.ocpp.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.emobility.ocpp.profile.operation.AuthorizationOperation;
import com.emobility.ocpp.profile.operation.BootNotificationOperation;
import com.emobility.ocpp.profile.operation.CancelReservationOperation;
import com.emobility.ocpp.profile.operation.ChangeAvailabilityOperation;
import com.emobility.ocpp.profile.operation.ChangeConfigurationOperation;
import com.emobility.ocpp.profile.operation.ClearCacheOperation;
import com.emobility.ocpp.profile.operation.ClearChargingProfileOperation;
import com.emobility.ocpp.profile.operation.DataTransferOperartion;
import com.emobility.ocpp.profile.operation.DiagnosticsStatusNotificationOperation;
import com.emobility.ocpp.profile.operation.FirmwareStatusNotificationOperation;
import com.emobility.ocpp.profile.operation.GetCompositeScheduleOperation;
import com.emobility.ocpp.profile.operation.GetConfigurationOperation;
import com.emobility.ocpp.profile.operation.GetDiagnosticsOperation;
import com.emobility.ocpp.profile.operation.GetLocalListVersionOperation;
import com.emobility.ocpp.profile.operation.HeartbeatOperation;
import com.emobility.ocpp.profile.operation.MeterValuesOperation;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.profile.operation.RemoteStartTransactionOperation;
import com.emobility.ocpp.profile.operation.RemoteStopTransactionOperation;
import com.emobility.ocpp.profile.operation.ReserveNowOperation;
import com.emobility.ocpp.profile.operation.ResetOperation;
import com.emobility.ocpp.profile.operation.SendLocalListOperation;
import com.emobility.ocpp.profile.operation.SetChargingProfileOperation;
import com.emobility.ocpp.profile.operation.StartTransactionOperation;
import com.emobility.ocpp.profile.operation.StatusNotificationOperation;
import com.emobility.ocpp.profile.operation.StopTransactionOperation;
import com.emobility.ocpp.profile.operation.TriggerMessageOperation;
import com.emobility.ocpp.profile.operation.UnlockConnectorOperation;
import com.emobility.ocpp.profile.operation.UpdateFirmwareOperation;
import com.emobility.ocpp.socket.message.ConfirmMessage;
import com.emobility.ocpp.socket.message.ErrorMessage;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageErrors;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.EmptyPayload;
import com.emobility.ocpp.socket.message.parse.JSONMessageParser;
import com.emobility.ocpp.socket.message.validate.InvalidMessageFormat;

/**
 * This class represents the core profile which support the following features:
 * 
 * <ol>
 * 	<li>Cancel Reservation</li>
 * </ol>
 * 
 * @author Salah Abu Msameh
 */
@Component
public class ServerCoreProfile extends AbstractProfile {
	
	@Autowired
	private JSONMessageParser msgConverter;
	
	@Autowired
	private MessageTracker msgTracker;
	
	/**
	 * constructor
	 */
	public ServerCoreProfile() {
		addOperationsGroup();
	}
	
	@Override
	public void addOperationsGroup() {
		
		//Supported charge point requests
		this.supportedOperations.put(OperationActions.AUTHORIZATION, AuthorizationOperation.class);
		this.supportedOperations.put(OperationActions.BOOT_NOTIFICATION, BootNotificationOperation.class);
		this.supportedOperations.put(OperationActions.HEARTBEAT, HeartbeatOperation.class);
		this.supportedOperations.put(OperationActions.DATA_TRANSFER, DataTransferOperartion.class);
		this.supportedOperations.put(OperationActions.DIAGNOSTICS_STATUS_TRANSACTION, DiagnosticsStatusNotificationOperation.class);
		this.supportedOperations.put(OperationActions.FIRMWARE_STATUS_TRANSACTION, FirmwareStatusNotificationOperation.class);
		this.supportedOperations.put(OperationActions.METER_VALUES, MeterValuesOperation.class);
		this.supportedOperations.put(OperationActions.START_TRANSACTION, StartTransactionOperation.class);
		this.supportedOperations.put(OperationActions.STATUS_NOTIFICATION, StatusNotificationOperation.class);
		this.supportedOperations.put(OperationActions.STOP_TRANSACTION, StopTransactionOperation.class);
		
		
		//Supported central system operations
		this.supportedOperations.put(OperationActions.REMOTE_START_TRANSACTION, RemoteStartTransactionOperation.class);
		this.supportedOperations.put(OperationActions.REMOTE_STOP_TRANSACTION, RemoteStopTransactionOperation.class);
		this.supportedOperations.put(OperationActions.CANCEL_RESERVATION, CancelReservationOperation.class);
		this.supportedOperations.put(OperationActions.CHANGE_AVAILABILITY, ChangeAvailabilityOperation.class);
		this.supportedOperations.put(OperationActions.SET_CHARGING_PROFILE, SetChargingProfileOperation.class);
		this.supportedOperations.put(OperationActions.CHANGE_CONFIGURATION_PROFILE, ChangeConfigurationOperation.class);
		this.supportedOperations.put(OperationActions.CLEAR_CACHE, ClearCacheOperation.class);
		this.supportedOperations.put(OperationActions.CLEAR_CHARGING_PROFILE, ClearChargingProfileOperation.class);
		this.supportedOperations.put(OperationActions.GET_COMPOSITE_SCHEDULE, GetCompositeScheduleOperation.class);
		this.supportedOperations.put(OperationActions.GET_CONFIGURATION, GetConfigurationOperation.class);
		this.supportedOperations.put(OperationActions.GET_DIAGNOSTICS, GetDiagnosticsOperation.class);
		this.supportedOperations.put(OperationActions.GET_LOCAL_LIST_VERSION, GetLocalListVersionOperation.class);
		this.supportedOperations.put(OperationActions.RESERVE_NOW, ReserveNowOperation.class);
		this.supportedOperations.put(OperationActions.RESET, ResetOperation.class);
		this.supportedOperations.put(OperationActions.SEND_LOCAL_LIST, SendLocalListOperation.class);
		this.supportedOperations.put(OperationActions.TRIGGER_MESSAGE, TriggerMessageOperation.class);
		this.supportedOperations.put(OperationActions.UNLOCK_CONNECTOR, UnlockConnectorOperation.class);
		this.supportedOperations.put(OperationActions.UPDATE_FIRMWARE, UpdateFirmwareOperation.class);
	}
	
	/**
	 * 
	 * @param message
	 * @return
	 * @throws InvalidMessageFormat 
	 * @throws UnsupportedMessageType 
	 */
	public Message getOperationMessage(String message) {
		
		Message msg = msgConverter.parse(message);
		
		switch (msg.getMessageType()) {
			case CALL: {
				
				RequestMessage reqMsg = (RequestMessage) msg;
				
				if(!this.supportedOperations.containsKey(reqMsg.getAction())) {
					return new ErrorMessage(msg.getUniqueId(), MessageErrors.UNSUPPORTED_MSG_OPERATION.getErrorCode(),
							MessageErrors.UNSUPPORTED_MSG_OPERATION.getErrorDesc(), new EmptyPayload());
				}
				
				break;
			}
			case CALLRESULT: {
				
				ConfirmMessage confirmMsg = (ConfirmMessage) msg;
				
				if(!this.supportedOperations.containsKey(msgTracker.getSubmittedMessage(confirmMsg.getUniqueId()).getAction())) {
					return new ErrorMessage(msg.getUniqueId(), MessageErrors.UNSUPPORTED_MSG_OPERATION.getErrorCode(),
							MessageErrors.UNSUPPORTED_MSG_OPERATION.getErrorDesc(), new EmptyPayload());
				}
				
				break;
			}
			default: {
				return msg;
			}
		}
		
		return msg;
	}
}
