package com.emobility.ocpp.profile.operation;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.socket.message.handler.MessageTransmitter;

/**
 * Common operation behavior 
 * 
 * @author Salah Abu Msameh
 */
public abstract class AbstractOperation implements Operation {

	@Autowired
	protected MessageTransmitter msgTransmitter;
	
}
