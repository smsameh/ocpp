package com.emobility.ocpp.profile.config;

import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import com.emobility.ocpp.socket.message.handler.ServerMessageHandler;

/**
 * This class contains the spring beans configuration, this configuration class allows any spring application that 
 * using this library to extends the functionality for any bean defined in this class
 * 
 * @author Salah Abu Msameh
 */
public interface OcppConfig {
	
	/**
	 * Server websocket handler<br/>
	 * 
	 * If the system that using this library doesn't want to have a custom message handler, then 
	 * it should return the default message handler {@link ServerMessageHandler}
	 * @return
	 */
	public WebSocketHandler getWebSocketHandler();
	
	/**
	 * Server handshake interceptor<br/>
	 * 
	 * If the system that using this library doesn't want to have a custom handshake interceptor, then 
	 * it should return the default interceptor {@link HttpSessionHandshakeInterceptor}
	 * @return
	 */
	public HandshakeInterceptor getHandshakeInterceptor();
 }
