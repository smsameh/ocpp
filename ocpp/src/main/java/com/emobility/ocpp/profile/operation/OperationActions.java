package com.emobility.ocpp.profile.operation;

/**
 * Actions keys for all the different operations
 * 
 * @author Salah Abu Msameh
 */
public class OperationActions {
	
	//central system requests
	public static final String REMOTE_START_TRANSACTION = "RemoteStartTransaction";
	public static final String REMOTE_STOP_TRANSACTION = "RemoteStopTransaction";
	public static final String CANCEL_RESERVATION = "CancelReservation";
	public static final String CHANGE_AVAILABILITY = "ChangeAvailability";
	public static final String SET_CHARGING_PROFILE = "SetChargingProfile";
	public static final String CHANGE_CONFIGURATION_PROFILE = "ChangeConfiguration";
	public static final String CLEAR_CACHE = "ClearCache";
	public static final String CLEAR_CHARGING_PROFILE = "ClearChargingProfile";
	public static final String GET_COMPOSITE_SCHEDULE = "GetCompositeSchedule";
	public static final String GET_CONFIGURATION = "GetConfiguration";
	public static final String GET_DIAGNOSTICS = "GetDiagnostics";
	public static final String GET_LOCAL_LIST_VERSION = "GetLocalListVersion";
	public static final String RESERVE_NOW = "ReserveNow";
	public static final String RESET = "Reset";
	public static final String SEND_LOCAL_LIST = "SendLocalList";
	public static final String TRIGGER_MESSAGE = "TriggerMessage";
	public static final String UNLOCK_CONNECTOR = "UnlockConnector";
	public static final String UPDATE_FIRMWARE = "UpdateFirmware";
	
	
	//charge point requests
	public static final String AUTHORIZATION = "Authorize";
	public static final String BOOT_NOTIFICATION = "BootNotification";
	public static final String DATA_TRANSFER = "DataTransfer";
	public static final String HEARTBEAT = "Heartbeat";
	public static final String DIAGNOSTICS_STATUS_TRANSACTION = "DiagnosticsStatusNotification";
	public static final String FIRMWARE_STATUS_TRANSACTION = "FirmwareStatusNotification";
	public static final String METER_VALUES = "MeterValues";
	public static final String START_TRANSACTION = "StartTransaction";
	public static final String STATUS_NOTIFICATION = "StatusNotification";
	public static final String STOP_TRANSACTION = "StopTransaction";
}
