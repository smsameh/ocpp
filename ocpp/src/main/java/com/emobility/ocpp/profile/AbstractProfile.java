package com.emobility.ocpp.profile;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.emobility.ocpp.profile.operation.Operation;

/**
 * 
 * @author Salah Abu Msameh
 */
public abstract class AbstractProfile implements Profile {
	
	@Autowired 
	protected ApplicationContext ctx;
	
	/**Operations/Features Group*/
	protected Map<String, Class<? extends Operation>> supportedOperations = new HashMap<String, Class<? extends Operation>>();
	
	@Override
	public Operation getOperation(String action) {
		
		Class<? extends Operation> operationType = supportedOperations.get(action);
		
		if(operationType == null) {
			return null;
		}
		
		return ctx.getBean(operationType);
	}
}
