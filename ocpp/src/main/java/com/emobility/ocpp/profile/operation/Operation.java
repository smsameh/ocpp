package com.emobility.ocpp.profile.operation;

import com.emobility.ocpp.socket.message.Message;

/**
 * This interface defines a single operation/feature
 * 
 * @author Salah Abu Msameh
 */
public interface Operation {

	/**
	 * handle message submission
	 * 
	 * @param message
	 */
	public void submitMessage(Message message);
	
	/**
	 * handle received operation related message
	 * 
	 * @param message
	 */
	public void handleReceivedMessage(Message message);
}
