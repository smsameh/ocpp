package com.emobility.ocpp.profile;

import com.emobility.ocpp.profile.operation.Operation;

/**
 * This interface defines the behavior for ocpp profile, the ocpp 1.6 spec defines the profile as a group of features<br/>
 * 
 * In this implementation, the profile plays as the entry point for any request to be handled,
 * it determine the current request type, then delegate it to the handler.
 * 
 * @author Salah Abu Msameh
 */
public interface Profile {
	
	//TODO, add Charging Schedule which is the definition of charging limits as per the documentation
	
	/**
	 * 
	 * @param action
	 * @return operation for the given action
	 */
	public Operation getOperation(String action);
	
	/**
	 * Adds the profile related operations
	 */
	public void addOperationsGroup();
}
