package com.emobility.ocpp.profile.operation;

/**
 * Cancel reservation operation
 * 
 * @author Salah Abu Msameh
 */
public abstract class CancelReservationOperation extends AbstractOperation {

}
