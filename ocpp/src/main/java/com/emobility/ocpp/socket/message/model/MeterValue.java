package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class MeterValue {
	
	@XmlElement
	private long timestamp;
	
	@XmlElement
	private SampledValue sampleValue;

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public SampledValue getSampleValue() {
		return sampleValue;
	}

	public void setSampleValue(SampledValue sampleValue) {
		this.sampleValue = sampleValue;
	}
}
