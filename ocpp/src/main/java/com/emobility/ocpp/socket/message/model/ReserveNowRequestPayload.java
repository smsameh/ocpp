package com.emobility.ocpp.socket.message.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class ReserveNowRequestPayload implements Payload {

	private int connectorId;
	private Date expiryDate;
	private IdToken idTag;
	private IdToken parentIdTag;
	private int reservationId;
	
	public int getConnectorId() {
		return connectorId;
	}
	
	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}
	
	public Date getExpiryDate() {
		return expiryDate;
	}
	
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public IdToken getIdTag() {
		return idTag;
	}
	
	public void setIdTag(IdToken idTag) {
		this.idTag = idTag;
	}
	
	public IdToken getParentIdTag() {
		return parentIdTag;
	}
	
	public void setParentIdTag(IdToken parentIdTag) {
		this.parentIdTag = parentIdTag;
	}
	
	public int getReservationId() {
		return reservationId;
	}
	
	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}
}
