package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class StopTransactionRequestPayload implements Payload {

	@XmlElement
	private IdToken idToken;
	
	@XmlElement
	private int meterStop;
	
	@XmlElement
	private long timestamp;
	
	@XmlElement
	private int transactionId;
	
	@XmlElement
	private Reason reason;
	
	@XmlElement
	private MeterValue transactionData;

	public IdToken getIdToken() {
		return idToken;
	}

	public void setIdToken(IdToken idToken) {
		this.idToken = idToken;
	}

	public int getMeterStop() {
		return meterStop;
	}

	public void setMeterStop(int meterStop) {
		this.meterStop = meterStop;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public Reason getReason() {
		return reason;
	}

	public void setReason(Reason reason) {
		this.reason = reason;
	}

	public MeterValue getTransactionData() {
		return transactionData;
	}

	public void setTransactionData(MeterValue transactionData) {
		this.transactionData = transactionData;
	}
}
