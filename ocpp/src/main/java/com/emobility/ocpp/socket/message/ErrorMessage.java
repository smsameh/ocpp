package com.emobility.ocpp.socket.message;

import com.emobility.ocpp.socket.message.model.Payload;
import com.emobility.ocpp.util.JSONUtils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ErrorMessage implements Message {
	
	private String uniqueId;
	private Payload payload;
	private String errorCode;
	private String errorDescription;
	private String sessionId;
	
	/**
	 * 
	 */
	public ErrorMessage() {}
	
	/**
	 * 
	 * @param uniqueId
	 * @param payload
	 * @param errorCode
	 * @param errorDescription
	 */
	public ErrorMessage(String uniqueId, String errorCode, String errorDescription, Payload payload) {
		this.uniqueId = uniqueId;
		this.payload = payload;
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}



	@Override
	public MessageType getMessageType() {
		return MessageType.CALLERROR;
	}

	@Override
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	@Override
	public String getUniqueId() {
		return this.uniqueId;
	}

	@Override
	public void setPayload(Payload payload) {
		this.payload = payload;
	}

	@Override
	public Payload getPayload() {
		return this.payload;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
	@Override
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String getSessionId() {
		return this.sessionId;
	}
	
	@Override
	public String build() {
		return new StringBuilder("[")
				.append(getMessageType().getMessageTypeId())
				.append(", ")
				.append("\"").append(getUniqueId()).append("\"")
				.append(", ")
				.append("\"").append(getErrorCode()).append("\"")
				.append(", ")
				.append("\"").append(getErrorDescription()).append("\"")
				.append(", ")
				.append("\"").append(JSONUtils.parse(getPayload())).append("\"")
				.append("]")
				.toString();
	}
}
