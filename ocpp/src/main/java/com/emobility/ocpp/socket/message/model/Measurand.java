package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum Measurand {

	Current_Export, //Current.Export Instantaneous current flow from EV
	Current_Import, //Current.Import Instantaneous current flow to EV
	Current_Offered, //Current.Offered Maximum current offered to EV
	Energy_Active_Export_Register, //Energy.Active.Export.Register Energy exported by EV (Wh or kWh)
	Energy_Active_Import_Register; //Energy.Active.Import.Register Energy imported by EV (Wh or kWh)
}
