package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class StartTransactionRequestPayload implements Payload {
	
	@XmlElement(required = true)
	private int connectorId;
	
	@XmlElement(required = true, name = "IdToken")
	private IdToken idToken;
	
	@XmlElement(required = true)
	private int meterStart;
	
	@XmlElement
	private int reservationId;
	
	@XmlElement(required = true)
	private long timestamp;

	public int getConnectorId() {
		return connectorId;
	}

	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}

	public IdToken getIdToken() {
		return idToken;
	}

	public void setIdToken(IdToken idToken) {
		this.idToken = idToken;
	}

	public int getMeterStart() {
		return meterStart;
	}

	public void setMeterStart(int meterStart) {
		this.meterStart = meterStart;
	}

	public int getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
}
