package com.emobility.ocpp.socket.message.validate;

import org.springframework.stereotype.Component;

/**
 * 
 * @author Salah Abu Msameh
 */
@Component
public class JSONMessageValidator {
	
	private static final int UNIQUE_ID_LENGTH = 36;
	
	/**
	 * 
	 * @param jsonMessage
	 * @return
	 */
	public boolean isValid(String jsonMessage) {
		
		//TODO replace with a regex pattern validation
		if(jsonMessage.startsWith("[")
				&& jsonMessage.endsWith("]")) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param uniqueId
	 * @return
	 */
	public boolean isValidMessageUniqueId(String uniqueId) {
		return uniqueId.length() <= UNIQUE_ID_LENGTH;
	}
}
