package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum MessageTrigger {

	BootNotification, //To trigger a BootNotification request
	DiagnosticsStatusNotification, //To trigger a DiagnosticsStatusNotification request
	FirmwareStatusNotification, //To trigger a FirmwareStatusNotification request 
	Heartbeat, //To trigger a Heartbeat request 100
	MeterValues, //To trigger a MeterValues request
	StatusNotification; //To trigger a StatusNotification request
}
