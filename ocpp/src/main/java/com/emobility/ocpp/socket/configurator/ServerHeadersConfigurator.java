package com.emobility.ocpp.socket.configurator;

import java.util.List;
import java.util.Map;

import javax.websocket.ClientEndpointConfig;

/**
 * Socket server request header configurator
 * 
 * @author Salah Abu Msameh
 */
public class ServerHeadersConfigurator extends ClientEndpointConfig.Configurator {

	@Override
	public void beforeRequest(Map<String, List<String>> headers) {
		
		// TODO add socket request header as descriped in the json spec document
		/**
		 * HTTP/1.1 101 Switching Protocols
		 * Upgrade: websocket
		 * Connection: Upgrade
		 * Sec-WebSocket-Accept: s3pPLMBiTxaQ9kYGzzhZRbK+xOo=
		 * Sec-WebSocket-Protocol: ocpp1.6
		 */
		super.beforeRequest(headers);
	}
}
