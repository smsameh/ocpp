package com.emobility.ocpp.socket.message.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class UpdateFirmwareRequestPayload implements Payload {
	
	private String location;//This contains a string containing a URI pointing to a location from which to retrieve the firmware
	private int retries;
	private Date retrieveDate;
	private int retryInterval;
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public int getRetries() {
		return retries;
	}
	
	public void setRetries(int retries) {
		this.retries = retries;
	}
	
	public Date getRetrieveDate() {
		return retrieveDate;
	}
	
	public void setRetrieveDate(Date retrieveDate) {
		this.retrieveDate = retrieveDate;
	}
	
	public int getRetryInterval() {
		return retryInterval;
	}
	
	public void setRetryInterval(int retryInterval) {
		this.retryInterval = retryInterval;
	}
}
