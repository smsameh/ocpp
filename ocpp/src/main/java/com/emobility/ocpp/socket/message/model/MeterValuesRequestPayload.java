package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class MeterValuesRequestPayload implements Payload {

	@XmlElement
	private int connectorId;
	
	@XmlElement
	private int transactionId;
	
	@XmlElement
	private MeterValue meterValue;

	public int getConnectorId() {
		return connectorId;
	}

	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public MeterValue getMeterValue() {
		return meterValue;
	}

	public void setMeterValue(MeterValue meterValue) {
		this.meterValue = meterValue;
	}
}
