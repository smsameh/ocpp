package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ConfigurationStatus {

	Accepted, //Configuration key supported and setting has been changed.
	Rejected, //Configuration key supported, but setting could not be changed.
	RebootRequired, //Configuration key supported and setting has been changed, 
					//but change will be available after reboot (Charge Point will not reboot itself)
	NotSupported; //Configuration key is not supported.
}
