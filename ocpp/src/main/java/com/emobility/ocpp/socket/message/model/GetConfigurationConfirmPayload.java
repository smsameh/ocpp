package com.emobility.ocpp.socket.message.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class GetConfigurationConfirmPayload implements Payload {

	private KeyValue configurationKey;
	private List<String> unknownKey;
	
	public KeyValue getConfigurationKey() {
		return configurationKey;
	}
	
	public void setConfigurationKey(KeyValue configurationKey) {
		this.configurationKey = configurationKey;
	}
	
	public List<String> getUnknownKey() {
		return unknownKey;
	}
	
	public void setUnknownKey(List<String> unknownKey) {
		this.unknownKey = unknownKey;
	}
}
