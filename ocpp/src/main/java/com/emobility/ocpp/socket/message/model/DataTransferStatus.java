package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum DataTransferStatus {
	
	Accepted, //Accepted Message has been accepted and the contained request is accepted.
	Rejected, //Rejected Message has been accepted but the contained request is rejected.
	UnknownMessageId, //UnknownMessageId Message could not be interpreted due to unknown messageId string.
	UnknownVendorId, //UnknownVendorId
}
