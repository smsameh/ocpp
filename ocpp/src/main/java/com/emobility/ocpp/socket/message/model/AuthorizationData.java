package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class AuthorizationData {

	private IdToken idTag;
	private IdTagInfo idTagInfo;
	
	/**
	 * 
	 * @param idTag
	 * @param idTagInfo
	 */
	public AuthorizationData(IdToken idTag, IdTagInfo idTagInfo) {
		this.idTag = idTag;
		this.idTagInfo = idTagInfo;
	}

	@XmlElement
	public IdToken getIdTag() {
		return idTag;
	}
	
	public void setIdTag(IdToken idTag) {
		this.idTag = idTag;
	}
	
	@XmlElement
	public IdTagInfo getIdTagInfo() {
		return idTagInfo;
	}
	
	public void setIdTagInfo(IdTagInfo idTagInfo) {
		this.idTagInfo = idTagInfo;
	}
}
