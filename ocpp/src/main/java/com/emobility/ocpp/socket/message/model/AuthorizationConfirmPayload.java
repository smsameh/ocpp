package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class AuthorizationConfirmPayload implements Payload {
	
	private long expiryDate; 
	private String parentIdTag;
	private String status;
	
	/**
	 * 
	 * @param expiryDate
	 * @param parentIdTag
	 * @param status
	 */
	private AuthorizationConfirmPayload(long expiryDate, String parentIdTag, String status) {
		this.expiryDate = expiryDate;
		this.parentIdTag = parentIdTag;
		this.status = status;
	}
	
	@XmlElement
	public long getExpiryDate() {
		return expiryDate;
	}
	
	public void setExpiryDate(long expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	@XmlElement
	public String getParentIdTag() {
		return parentIdTag;
	}
	
	public void setParentIdTag(String parentIdTag) {
		this.parentIdTag = parentIdTag;
	}
	
	@XmlElement
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * 
	 * @return AuthorizationConfirmPayload with accepted status 
	 */
	public static AuthorizationConfirmPayload create(long expiryDate, String parentIdTag, AuthorizationStatus status) {
		return new AuthorizationConfirmPayload(expiryDate, parentIdTag, status.name());
	}
}
