package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Cancel reservation request 
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class CancelReservationRequestPayload implements Payload {
	
	@XmlElement(required = true)
	private int reservationId;
	
	/**
	 * 
	 * @param reservationId
	 */
	public CancelReservationRequestPayload(int reservationId) {
		this.reservationId = reservationId;
	}
	
	public int getReservationId() {
		return reservationId;
	}
	
	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}
}
