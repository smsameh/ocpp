package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class FirmwareStatusNotificationRequestPayload implements Payload {
	
	@XmlElement
	private FirmwareStatus firmwareStatus;
	
	/**
	 * 
	 * @param firmwareStatus
	 */
	public FirmwareStatusNotificationRequestPayload(FirmwareStatus firmwareStatus) {
		this.firmwareStatus = firmwareStatus;
	}

	public FirmwareStatus getFirmwareStatus() {
		return firmwareStatus;
	}

	public void setFirmwareStatus(FirmwareStatus firmwareStatus) {
		this.firmwareStatus = firmwareStatus;
	}
}
