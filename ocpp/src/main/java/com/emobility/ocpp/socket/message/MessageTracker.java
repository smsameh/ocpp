package com.emobility.ocpp.socket.message;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@Component
public class MessageTracker {

	private Map<String, RequestMessage> submittedMessages = new ConcurrentHashMap<String, RequestMessage>();
	
	/**
	 * 
	 * @param uniqueId
	 * @param reqMessage
	 */
	public void registerSubmittedMessage(String uniqueId, RequestMessage reqMessage) {
		
		if(StringUtils.isEmpty(uniqueId) || reqMessage == null) {
			Log.error(MessageTracker.class, "Failed to register submitted message in message tracker");
			return;
		}
		
		this.submittedMessages.put(uniqueId, reqMessage);
	}
	
	/**
	 * 
	 * @param uniqueId
	 * @return
	 */
	public RequestMessage getSubmittedMessage(String uniqueId) {
		
		if(StringUtils.isEmpty(uniqueId)) {
			return null;
		}
		
		return this.submittedMessages.get(uniqueId);
	}
	
	/**
	 * 
	 * @param uniqueId
	 */
	public void removeMessage(String uniqueId) {
		
		if(StringUtils.isEmpty(uniqueId)) {
			return;
		}
		
		this.submittedMessages.remove(uniqueId);
	}
}
