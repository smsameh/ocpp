package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class ChargingSchedulePeriod {
	
	@XmlElement
	private int startPeriod;
	
	@XmlElement
	private double limit;
	
	@XmlElement
	private int numberPhases;
	
	/**
	 * 
	 * @param startPeriod
	 * @param limit
	 * @param numberPhases
	 */
	public ChargingSchedulePeriod(int startPeriod, double limit, int numberPhases) {
		this.startPeriod = startPeriod;
		this.limit = limit;
		this.numberPhases = numberPhases;
	}
	
	public int getStartPeriod() {
		return startPeriod;
	}

	public void setStartPeriod(int startPeriod) {
		this.startPeriod = startPeriod;
	}

	public double getLimit() {
		return limit;
	}

	public void setLimit(double limit) {
		this.limit = limit;
	}

	public int getNumberPhases() {
		return numberPhases;
	}

	public void setNumberPhases(int numberPhases) {
		this.numberPhases = numberPhases;
	}
}
