package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ResetConfirmPayload implements Payload {

	private ResetStatus status;
	
	public ResetStatus getStatus() {
		return status;
	}
	
	public void setStatus(ResetStatus status) {
		this.status = status;
	}
}
