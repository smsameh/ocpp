package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ResetStatus {

	Accepted, //Command will be executed.
	Rejected; //Command will not be executed.
}
