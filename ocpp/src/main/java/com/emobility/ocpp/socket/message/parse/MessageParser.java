package com.emobility.ocpp.socket.message.parse;

import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.UnsupportedOperationMessage;
import com.emobility.ocpp.socket.message.validate.InvalidMessageFormat;

/**
 * Defines socket message convert behavior
 * 
 * @author Salah Abu Msameh
 */
public interface MessageParser {
	
	/**
	 * Convert the given message string to request object
	 * 
	 * @param msg
	 * @return
	 * @throws UnsupportedOperationMessage
	 */
	public Message parse(String message) throws UnsupportedOperationMessage, InvalidMessageFormat;
	
	/**
	 * Covert given response to string
	 * 
	 * @param response
	 * @return
	 */
	public String parse(Message response);
}
