package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ChargingProfileKindType {

	Absolute, //Schedule periods are relative to a fixed point in time defined in the schedule.
	Recurring, //The schedule restarts periodically at the first schedule period.
	Relative; //Schedule periods are relative to a situation specific
	          //start point (such as the start of a session) that is determined by the charge point.
}
