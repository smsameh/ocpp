package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class BootNotificationRequestPayload implements Payload {

	private String chargeBoxSerialNumber;
	private String chargePointModel;//20
	private String chargePointSerialNumber;//25
	private String chargePointVendor;//20
	private String firmwareVersion;//50
	private String iccid;//20
	private String imsi;//20
	private String meterSerialNumber;//25
	private String meterType;//25
	
	@XmlElement
	public String getChargeBoxSerialNumber() {
		return chargeBoxSerialNumber;
	}
	
	public void setChargeBoxSerialNumber(String chargeBoxSerialNumber) {
		this.chargeBoxSerialNumber = chargeBoxSerialNumber;
	}
	
	@XmlElement
	public String getChargePointModel() {
		return chargePointModel;
	}
	
	public void setChargePointModel(String chargePointModel) {
		this.chargePointModel = chargePointModel;
	}
	
	@XmlElement
	public String getChargePointSerialNumber() {
		return chargePointSerialNumber;
	}
	
	public void setChargePointSerialNumber(String chargePointSerialNumber) {
		this.chargePointSerialNumber = chargePointSerialNumber;
	}
	
	@XmlElement
	public String getChargePointVendor() {
		return chargePointVendor;
	}
	
	public void setChargePointVendor(String chargePointVendor) {
		this.chargePointVendor = chargePointVendor;
	}
	
	@XmlElement
	public String getFirmwareVersion() {
		return firmwareVersion;
	}
	
	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}
	
	@XmlElement
	public String getIccid() {
		return iccid;
	}
	
	public void setIccid(String iccid) {
		this.iccid = iccid;
	}
	
	@XmlElement
	public String getImsi() {
		return imsi;
	}
	
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	
	@XmlElement
	public String getMeterSerialNumber() {
		return meterSerialNumber;
	}
	
	public void setMeterSerialNumber(String meterSerialNumber) {
		this.meterSerialNumber = meterSerialNumber;
	}
	
	@XmlElement
	public String getMeterType() {
		return meterType;
	}
	
	public void setMeterType(String meterType) {
		this.meterType = meterType;
	}
}
