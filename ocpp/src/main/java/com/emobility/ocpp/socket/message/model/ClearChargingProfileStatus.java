package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ClearChargingProfileStatus {
	
	Accepted, //Request has been accepted and will be executed.
	Unknown; //No Charging Profile(s) were found matching the request.
}
