package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum GetCompositeScheduleStatus {

	Accepted, //Request has been accepted and will be executed
	Rejected, //Request has not been accepted and will not beexecuted.
}
