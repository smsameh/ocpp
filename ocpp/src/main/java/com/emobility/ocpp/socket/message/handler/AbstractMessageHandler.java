package com.emobility.ocpp.socket.message.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.emobility.ocpp.profile.ServerCoreProfile;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageProcessor;
import com.emobility.ocpp.socket.session.SessionManager;
import com.emobility.ocpp.util.Log;

/**
 * Defines all common behavior for message handling 
 * 
 * @author Salah Abu Msameh
 */
public abstract class AbstractMessageHandler extends TextWebSocketHandler {
	
	@Autowired
	protected ServerCoreProfile profile;
	
	@Autowired
	protected MessageProcessor msgProcessor;
	
	@Autowired
	protected SessionManager sessionManager;
	
	/**
	 * Handle message
	 * 
	 * @param string
	 */
	protected void handleMessage(String sessionId, String message) throws MessageHandlingException {
		
		Log.info(AbstractMessageHandler.class, "Handling message >> " + message);
		
		//1. get message
		Message msg = profile.getOperationMessage(message);
		
		//2. add related session
		msg.setSessionId(sessionId);
		
		//3. delegate message to the messages processor
		msgProcessor.process(msg);
	}
}
