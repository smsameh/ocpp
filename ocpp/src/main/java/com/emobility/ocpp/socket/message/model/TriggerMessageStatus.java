package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum TriggerMessageStatus {

	Accepted, //Requested notification will be sent.
	Rejected, //Requested notification will not be sent.
	NotImplemented; //Requested notification cannot be sent because it is either not implemented or unknown.
}
