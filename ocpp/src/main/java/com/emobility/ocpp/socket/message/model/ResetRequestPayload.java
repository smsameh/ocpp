package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class ResetRequestPayload implements Payload {

	private ResetType type;
	
	/**
	 * 
	 * @param type2
	 */
	public ResetRequestPayload(ResetType type) {
		this.type = type;
	}

	public ResetType getType() {
		return type;
	}
	
	public void setType(ResetType type) {
		this.type = type;
	}
}
