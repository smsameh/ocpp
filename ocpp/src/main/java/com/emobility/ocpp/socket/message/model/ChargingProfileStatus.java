package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ChargingProfileStatus {
	
	Accepted, //Request has been accepted and will be executed.
	Rejected, //Request has not been accepted and will not be executed.
	NotSupported, //Charge Point indicates that the request is not supported.
}
