package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ClearChargingProfileRequestPayload implements Payload {
	
	private int id;
	private int connectorId;
	private ChargingProfilePurposeType chargingProfilePurpose;
	private int stackLevel;
	
	@XmlElement
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@XmlElement
	public int getConnectorId() {
		return connectorId;
	}
	
	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}
	
	@XmlElement
	public ChargingProfilePurposeType getChargingProfilePurpose() {
		return chargingProfilePurpose;
	}
	
	public void setChargingProfilePurpose(ChargingProfilePurposeType chargingProfilePurpose) {
		this.chargingProfilePurpose = chargingProfilePurpose;
	}
	
	@XmlElement
	public int getStackLevel() {
		return stackLevel;
	}
	
	public void setStackLevel(int stackLevel) {
		this.stackLevel = stackLevel;
	}
}
