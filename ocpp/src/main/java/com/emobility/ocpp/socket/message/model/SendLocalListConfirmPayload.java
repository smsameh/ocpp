package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class SendLocalListConfirmPayload implements Payload {

	private UpdateStatus status;
	
	@XmlElement
	public UpdateStatus getStatus() {
		return status;
	}
	
	public void setStatus(UpdateStatus status) {
		this.status = status;
	}
}
