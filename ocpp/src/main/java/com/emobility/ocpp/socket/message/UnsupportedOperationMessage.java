package com.emobility.ocpp.socket.message;

/**
 * 
 * @author Salah Abu Msameh
 */
public class UnsupportedOperationMessage extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 * @param string
	 */
	public UnsupportedOperationMessage(String message) {
		super(message);
	}
	
	/**
	 * 
	 * @param cause
	 */
	public UnsupportedOperationMessage(Exception cause) {
		super(cause);
	}
}
