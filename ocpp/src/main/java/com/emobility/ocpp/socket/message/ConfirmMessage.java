package com.emobility.ocpp.socket.message;

import com.emobility.ocpp.socket.message.model.Payload;
import com.emobility.ocpp.util.JSONUtils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ConfirmMessage implements Message {
	
	private String uniqueId;
	private Payload payload;
	private String sessionId;
	
	/**
	 * 
	 * @param sessionId
	 * @param uniqueId
	 * @param payload
	 */
	public ConfirmMessage(String sessionId, String uniqueId, Payload payload) {
		this.sessionId = sessionId;
		this.uniqueId = uniqueId;
		this.payload = payload;
	}

	@Override
	public MessageType getMessageType() {
		return MessageType.CALLRESULT;
	}

	@Override
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	@Override
	public String getUniqueId() {
		return this.uniqueId;
	}

	@Override
	public void setPayload(Payload payload) {
		this.payload = payload;
	}

	@Override
	public Payload getPayload() {
		return this.payload;
	}
	
	@Override
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String getSessionId() {
		return this.sessionId;
	}

	@Override
	public String build() {
		return new StringBuilder("[")
				.append(getMessageType().getMessageTypeId())
				.append(", ")
				.append("\"").append(getUniqueId()).append("\"")
				.append(", ")
				.append("\"").append(JSONUtils.parse(getPayload())).append("\"")
				.append("]")
				.toString();
	}
}
