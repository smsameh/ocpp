package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ValueFormat {
	Raw, // Data is to be interpreted as integer/decimal numeric data.
	SignedData, // Data is represented as a signed binary data block, encoded as hex data.
}
