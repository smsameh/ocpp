package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class DiagnosticsStatusNotificationRequestPayload implements Payload {
	
	@XmlElement
	private DiagnosticsStatus diagnosticsStatus;
	
	/**
	 * 
	 * @param diagnosticsStatus
	 */
	public DiagnosticsStatusNotificationRequestPayload(DiagnosticsStatus diagnosticsStatus) {
		this.diagnosticsStatus = diagnosticsStatus;
	}

	public DiagnosticsStatus getDiagnosticsStatus() {
		return diagnosticsStatus;
	}

	public void setDiagnosticsStatus(DiagnosticsStatus diagnosticsStatus) {
		this.diagnosticsStatus = diagnosticsStatus;
	}
}
