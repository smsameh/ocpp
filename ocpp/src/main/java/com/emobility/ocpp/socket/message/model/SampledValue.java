package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class SampledValue {

	@XmlElement
	private String value;
	
	@XmlElement
	private ReadingContext readingContext;

	@XmlElement
	private ValueFormat valueFormat;
	
	@XmlElement
	private Measurand measurand;
	
	@XmlElement
	private Phase phase;
	
	@XmlElement
	private Location location;
	
	@XmlElement
	private UnitOfMeasure unit;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ReadingContext getReadingContext() {
		return readingContext;
	}

	public void setReadingContext(ReadingContext readingContext) {
		this.readingContext = readingContext;
	}

	public ValueFormat getValueFormat() {
		return valueFormat;
	}

	public void setValueFormat(ValueFormat valueFormat) {
		this.valueFormat = valueFormat;
	}

	public Measurand getMeasurand() {
		return measurand;
	}

	public void setMeasurand(Measurand measurand) {
		this.measurand = measurand;
	}

	public Phase getPhase() {
		return phase;
	}

	public void setPhase(Phase phase) {
		this.phase = phase;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public UnitOfMeasure getUnit() {
		return unit;
	}

	public void setUnit(UnitOfMeasure unit) {
		this.unit = unit;
	}
}
