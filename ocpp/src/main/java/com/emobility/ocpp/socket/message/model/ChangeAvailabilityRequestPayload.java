package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class ChangeAvailabilityRequestPayload implements Payload {
	
	@XmlElement
	private int connectorId;
	
	@XmlElement
	private AvailabilityType type;
	
	/**
	 * 
	 * @param connectorId
	 * @param type
	 */
	public ChangeAvailabilityRequestPayload(int connectorId, AvailabilityType type) {
		this.connectorId = connectorId;
		this.type = type;
	}

	public int getConnectorId() {
		return connectorId;
	}

	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}

	public AvailabilityType getType() {
		return type;
	}

	public void setType(AvailabilityType type) {
		this.type = type;
	}
}
