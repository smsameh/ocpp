package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class GetCompositeScheduleRequestPayload implements Payload {

	private int connectorId;
	private int duration;
	private ChargingRateUnitType chargingRateUnit;
	
	@XmlElement
	public int getConnectorId() {
		return connectorId;
	}
	
	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}
	
	@XmlElement
	public int getDuration() {
		return duration;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	@XmlElement
	public ChargingRateUnitType getChargingRateUnit() {
		return chargingRateUnit;
	}
	
	public void setChargingRateUnit(ChargingRateUnitType chargingRateUnit) {
		this.chargingRateUnit = chargingRateUnit;
	}
}
