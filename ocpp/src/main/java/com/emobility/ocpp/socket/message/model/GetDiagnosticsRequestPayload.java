package com.emobility.ocpp.socket.message.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class GetDiagnosticsRequestPayload implements Payload {

	private String location;//url for where the diagnostics file should be uploaded into
	private int retries;
	private int retryInterval;
	private Date startTime;
	private Date stopTime;
	
	@XmlElement
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	@XmlElement
	public int getRetries() {
		return retries;
	}
	
	public void setRetries(int retries) {
		this.retries = retries;
	}
	
	@XmlElement
	public int getRetryInterval() {
		return retryInterval;
	}
	
	public void setRetryInterval(int retryInterval) {
		this.retryInterval = retryInterval;
	}
	
	@XmlElement
	public Date getStartTime() {
		return startTime;
	}
	
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	@XmlElement
	public Date getStopTime() {
		return stopTime;
	}
	
	public void setStopTime(Date stopTime) {
		this.stopTime = stopTime;
	}
}
