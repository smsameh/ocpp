package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum AvailabilityType {

	Inoperative, //Charge point is not available for charging.
	Operative, //Charge point is available for charging.
}
