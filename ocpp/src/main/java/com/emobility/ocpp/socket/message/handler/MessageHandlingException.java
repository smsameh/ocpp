package com.emobility.ocpp.socket.message.handler;

/**
 * 
 * @author Salah Abu Msameh
 */
public class MessageHandlingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 * @param cause
	 */
	public MessageHandlingException(Exception cause) {
		super(cause);
	}
}
