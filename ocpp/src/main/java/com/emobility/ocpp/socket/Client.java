package com.emobility.ocpp.socket;

import javax.websocket.ClientEndpoint;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

/**
 * 
 * @author Salah Abu Msameh
 */
@ClientEndpoint(configurator = ClientHeadersConfigurator.class)
public class Client {
	
	@OnOpen
	public void onOpen(Session session) {
		System.out.println("Connected to endpoint: " + session.getBasicRemote());
	}

	@OnMessage
	public void processMessage(String message) {
		System.out.println("Received message in client: " + message);
	}

	@OnError
	public void processError(Throwable t) {
		t.printStackTrace();
	}
}
