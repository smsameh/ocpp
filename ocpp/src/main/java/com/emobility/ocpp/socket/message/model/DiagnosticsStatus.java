package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum DiagnosticsStatus {

	Idle, //Idle Charge Point is not performing diagnostics related tasks.
		  //Status Idle SHALL only be used as in a DiagnosticsStatusNotification.req that was triggered by a TriggerMessage.req
	Uploaded, //Uploaded Diagnostics information has been uploaded.
	UploadFailed, //UploadFailed Uploading of diagnostics failed.
	Uploading, //Uploading File is being uploaded.
}
