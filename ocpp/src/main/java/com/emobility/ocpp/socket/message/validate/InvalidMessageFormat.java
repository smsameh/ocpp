package com.emobility.ocpp.socket.message.validate;

/**
 * 
 * @author Salah Abu Msameh
 */
public class InvalidMessageFormat extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 * @param cause
	 */
	public InvalidMessageFormat(String cause) {
		super(cause);
	}
}
