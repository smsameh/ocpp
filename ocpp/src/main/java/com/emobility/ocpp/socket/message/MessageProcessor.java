package com.emobility.ocpp.socket.message;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import com.emobility.ocpp.profile.ServerCoreProfile;
import com.emobility.ocpp.profile.operation.Operation;
import com.emobility.ocpp.socket.message.handler.MessageTransmitter;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;

/**
 * ServerMessageProcessor responsible for processing incoming request message from clients<br>
 * For now the processor adds message to work queue then processing them via separate thread
 * 
 * @author Salah Abu Msameh
 */
@Component
public class MessageProcessor {
	
	@Autowired
	private ServerCoreProfile profile;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Autowired
	private MessageTransmitter msgTransmitter;
	
	private final BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(100);
	private ExecutorService executor = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, queue);
	
	/**
	 * 
	 * @param message
	 */
	public void process(Message message) {
		executor.execute(new MessageTask(message));
	}
	
	/**
	 * shutdown processor executor
	 */
	public void shutdown() {
		executor.shutdown();
	}
	
	/**
	 * helper class
	 */
	@Configurable
	class MessageTask implements Runnable {
		
		private Message message;
		
		/**
		 * 
		 * @param message
		 */
		public MessageTask(Message message) {
			this.message = message;
		}
		
		@Override
		public void run() {
			
			//process message
			Operation operation = null;
			
			switch (this.message.getMessageType()) {
				case CALL: {
					
					RequestMessage reqMessage = (RequestMessage) this.message;
					
					operation = profile.getOperation(reqMessage.getAction());
					operation.handleReceivedMessage(this.message);
					
					break;
				}
				case CALLRESULT: {
					
					RequestMessage reqMessage = msgTracker.getSubmittedMessage(this.message.getUniqueId());
					
					operation = profile.getOperation(reqMessage.getAction());
					operation.handleReceivedMessage(this.message);
					
					break;
				}
				case CALLERROR: {
					
					try {
						msgTransmitter.sendMessage(this.message);
						
					} catch (NoSessionMessageFound e) {
						Log.error(MessageTask.class, e.getMessage());
						e.printStackTrace();
					} catch (IOException e) {
						Log.error(MessageTask.class, e.getMessage());
						e.printStackTrace();
					}
					
					break;
				}
			}
		}
	}
}
