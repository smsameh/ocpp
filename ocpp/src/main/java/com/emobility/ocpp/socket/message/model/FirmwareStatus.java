package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum FirmwareStatus {

	Downloaded, //Downloaded New firmware has been downloaded by Charg Point.
	DownloadFailed, //DownloadFailed Charge point failed to download firmware.
	Downloading, //Downloading Firmware is being downloaded.
	Idle, //Idle Charge Point is not performing firmware update related tasks. Status Idle SHALL only be used as in
	      //a FirmwareStatusNotification.req that was triggered by a TriggerMessage.req
	Installed, //Installed New firmware
	Installing, //Installing Firmware is being installed.
	InstallationFailed; //InstallationFailed Installation of new firmware has failed.
}
