package com.emobility.ocpp.socket.session;

/**
 * 
 * @author Salah Abu Msameh
 */
public class NoSessionFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 * @param cause
	 */
	public NoSessionFoundException(String cause) {
		super(cause);
	}
}
