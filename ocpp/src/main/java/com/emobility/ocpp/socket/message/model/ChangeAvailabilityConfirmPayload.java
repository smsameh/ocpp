package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class ChangeAvailabilityConfirmPayload implements Payload {
	
	@XmlElement
	private AvailabilityStatus status;
	
	/**
	 * 
	 * @param status
	 */
	public ChangeAvailabilityConfirmPayload(AvailabilityStatus status) {
		this.status = status;
	}

	public AvailabilityStatus getStatus() {
		return status;
	}

	public void setStatus(AvailabilityStatus status) {
		this.status = status;
	}
}
