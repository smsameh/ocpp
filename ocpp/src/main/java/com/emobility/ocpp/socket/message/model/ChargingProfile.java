package com.emobility.ocpp.socket.message.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ChargingProfile {

	@XmlElement
	private int chargingProfileId;
	
	@XmlElement
	private int transactionId;
	
	@XmlElement
	private int stackLevel;
	
	@XmlElement
	private ChargingProfilePurposeType chargingProfilePurpose;
	
	@XmlElement
	private ChargingProfileKindType chargingProfileKind;
	
	@XmlElement
	private RecurrencyKindType recurrencyKind;
	
	@XmlElement
	private Date validFrom;
	
	@XmlElement
	private Date validTo;
	
	@XmlElement
	private ChargingSchedule chargingSchedule;

	public int getChargingProfileId() {
		return chargingProfileId;
	}

	public void setChargingProfileId(int chargingProfileId) {
		this.chargingProfileId = chargingProfileId;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public int getStackLevel() {
		return stackLevel;
	}

	public void setStackLevel(int stackLevel) {
		this.stackLevel = stackLevel;
	}

	public ChargingProfilePurposeType getChargingProfilePurpose() {
		return chargingProfilePurpose;
	}

	public void setChargingProfilePurpose(ChargingProfilePurposeType chargingProfilePurpose) {
		this.chargingProfilePurpose = chargingProfilePurpose;
	}

	public ChargingProfileKindType getChargingProfileKind() {
		return chargingProfileKind;
	}

	public void setChargingProfileKind(ChargingProfileKindType chargingProfileKind) {
		this.chargingProfileKind = chargingProfileKind;
	}

	public RecurrencyKindType getRecurrencyKind() {
		return recurrencyKind;
	}

	public void setRecurrencyKind(RecurrencyKindType recurrencyKind) {
		this.recurrencyKind = recurrencyKind;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public ChargingSchedule getChargingSchedule() {
		return chargingSchedule;
	}

	public void setChargingSchedule(ChargingSchedule chargingSchedule) {
		this.chargingSchedule = chargingSchedule;
	}
}
