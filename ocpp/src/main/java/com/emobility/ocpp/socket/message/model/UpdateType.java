package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum UpdateType {

	Differential, //Indicates that the current Local Authorization List must be updated with the values in this message.
	Full, //Indicates that the current Local Authorization List must be replaced by the values in this message.
}
