package com.emobility.ocpp.socket.session;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

/**
 * Session Manager
 * 
 * @author Salah Abu Msameh
 */
@Component
public class SessionManager {
	
	private Map<String, WebSocketSession> sessions = new ConcurrentHashMap<String, WebSocketSession>();
//	private Map<String, String> sessionMessages = new ConcurrentHashMap<String, String>();

	/**
	 * 
	 * @param session
	 */
	public void addSession(WebSocketSession session) {
		sessions.put(session.getId(), session);
	}
	
	/**
	 * 
	 * @param sessionId
	 */
	public void removeSession(String sessionId) {
		sessions.remove(sessionId);
	}
	
	/**
	 * 
	 * @param sessionId
	 * @return
	 */
	public WebSocketSession getSession(String sessionId) {
		return sessions.get(sessionId);
	}
//	
//	/**
//	 * 
//	 * @param messageUniqueId
//	 * @param id
//	 */
//	public void addSessionMessage(String uniqueId, String id) {
//		
//		if(StringUtils.isEmpty(uniqueId)) {
//			return;
//		}
//		
//		this.sessionMessages.put(uniqueId, id);
//	}
//	
//	/**
//	 * 
//	 * @param uniqueId
//	 * @return
//	 * @throws NoSessionMessageFound
//	 */
//	public WebSocketSession getSessionByMessageId(String msgUniqueId) throws NoSessionMessageFound {
//		
//		String sessionId = this.sessionMessages.get(msgUniqueId);
//		
//		if(StringUtils.isEmpty(sessionId)) {
//			throw new NoSessionMessageFound("No session message found for message id [" + msgUniqueId + "]");
//		}
//		
//		return this.sessions.get(sessionId);
//	}
//	
//	/**
//	 * Remove a session message
//	 * @param msgUniqueId
//	 */
//	public void removeSessionMessage(String msgUniqueId) {
//		this.sessionMessages.remove(msgUniqueId);
//	}
}
