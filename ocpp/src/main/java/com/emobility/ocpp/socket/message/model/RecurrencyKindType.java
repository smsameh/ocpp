package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum RecurrencyKindType {

	Daily, //The schedule restarts at the beginning of the next day.
	Weekly; //The schedule restarts at the beginning of the next week (defined as Monday morning)
}
