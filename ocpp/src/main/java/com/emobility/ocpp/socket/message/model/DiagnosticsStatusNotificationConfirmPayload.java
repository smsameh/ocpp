package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class DiagnosticsStatusNotificationConfirmPayload implements Payload {

}
