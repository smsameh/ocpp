package com.emobility.ocpp.socket;

import java.net.URI;

import javax.websocket.ContainerProvider;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.emobility.ocpp.socket.message.handler.ClientMessageHandler;


/**
 * 
 * @author Salah Abu Msameh
 */
public class WebSocketClientConnector {
	
	private Session session;
	private ClientMessageHandler msgHandler;
	
	/**
	 * 
	 * @param msgHandler
	 */
	public WebSocketClientConnector(ClientMessageHandler msgHandler) {
		this.msgHandler = msgHandler;
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public void connect(String uri) throws Exception {
		
		WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        session = container.connectToServer(Client.class, URI.create(uri));
        
        if(session != null) {
        	session.addMessageHandler(msgHandler);
        	msgHandler.setSession(session);
        }
	}
	
	/**
	 * 
	 * @param message
	 * @throws Exception
	 */
	public void sendMessage(String message) throws Exception {
		session.getBasicRemote().sendText(message);
	}
}
