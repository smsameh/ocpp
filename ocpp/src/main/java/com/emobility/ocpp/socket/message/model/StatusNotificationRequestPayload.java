package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class StatusNotificationRequestPayload implements Payload {
	
	@XmlElement
	private int connectorId;
	
	@XmlElement
	private ChargePointErrorCode errorCode;
	
	@XmlElement
	private String info;
	
	@XmlElement
	private ChargePointStatus status;
	
	@XmlElement
	private long timestamp;
	
	@XmlElement
	private String vendorId;
	
	@XmlElement
	private String vendorErrorCode;

	public int getConnectorId() {
		return connectorId;
	}

	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}

	public ChargePointErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ChargePointErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public ChargePointStatus getStatus() {
		return status;
	}

	public void setStatus(ChargePointStatus status) {
		this.status = status;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorErrorCode() {
		return vendorErrorCode;
	}

	public void setVendorErrorCode(String vendorErrorCode) {
		this.vendorErrorCode = vendorErrorCode;
	}
}
