package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class TriggerMessageRequestPayload implements Payload {

	private int connectorId;
	private MessageTrigger requestedMessage;
	
	@XmlElement
	public int getConnectorId() {
		return connectorId;
	}
	
	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}
	
	@XmlElement
	public MessageTrigger getRequestedMessage() {
		return requestedMessage;
	}
	
	public void setRequestedMessage(MessageTrigger requestedMessage) {
		this.requestedMessage = requestedMessage;
	}
}
