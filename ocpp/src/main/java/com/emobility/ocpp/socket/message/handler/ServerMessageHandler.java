package com.emobility.ocpp.socket.message.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import com.emobility.ocpp.socket.session.SessionManager;
import com.emobility.ocpp.util.Log;

/**
 * Default message request handler<br/>
 * 
 * In case extended this class for custom behavior, the extender should still call super method
 * 
 * @author Salah Abu Msameh
 */
public class ServerMessageHandler extends AbstractMessageHandler {
	
	@Autowired
	private SessionManager sessionManager;
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		
		sessionManager.addSession(session);
		
		String path = session.getUri().getPath();
		String chargePointId = path.substring(path.lastIndexOf("/") + 1, path.length());
		
		Log.info(ServerMessageHandler.class, new StringBuilder("New Connection opened for (session id: ")
				.append(session.getId())
				.append(", Charge Point Id: ")
				.append(chargePointId)
				.append(")").toString());
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessionManager.removeSession(session.getId());
		Log.info(ServerMessageHandler.class, "Connection closed for (session id: " + session.getId() + ")");
	}
	
	/**
	 * handle a server incoming message
	 */
	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
		handleMessage(session.getId(), message.getPayload().toString());
	}
}
