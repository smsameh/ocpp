package com.emobility.ocpp.socket.message.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class HeartbeatConfirmPayload implements Payload {

	@XmlElement
	private Date currentTime;

	public HeartbeatConfirmPayload(Date currentTime) {
		super();
		this.currentTime = currentTime;
	}
}
