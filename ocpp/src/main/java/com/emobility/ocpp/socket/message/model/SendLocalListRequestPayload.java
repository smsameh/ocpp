package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class SendLocalListRequestPayload implements Payload {

	private int listVersion;
	private AuthorizationData localAuthorizationList;
	private UpdateType updateType;
	
	@XmlElement
	public int getListVersion() {
		return listVersion;
	}
	
	public void setListVersion(int listVersion) {
		this.listVersion = listVersion;
	}
	
	@XmlElement
	public AuthorizationData getLocalAuthorizationList() {
		return localAuthorizationList;
	}
	
	public void setLocalAuthorizationList(AuthorizationData localAuthorizationList) {
		this.localAuthorizationList = localAuthorizationList;
	}
	
	@XmlElement
	public UpdateType getUpdateType() {
		return updateType;
	}
	
	public void setUpdateType(UpdateType updateType) {
		this.updateType = updateType;
	}
}
