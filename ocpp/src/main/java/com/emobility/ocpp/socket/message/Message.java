package com.emobility.ocpp.socket.message;

import com.emobility.ocpp.socket.message.model.Payload;

/**
 * Represents a message socket behaviour
 * 
 * @author Salah Abu Msameh
 */
public interface Message {
	
	/**
	 * 
	 * @see MessageType
	 * @return current message type
	 */
	public MessageType getMessageType();
	
	/**
	 * 
	 * @return current message unique Id
	 */
	public void setUniqueId(String uniqueId);
	
	/**
	 * 
	 * @return current message unique Id
	 */
	public String getUniqueId();
	
	/**
	 * 
	 * @return message payload
	 */
	public void setPayload(Payload payload);
	
	/**
	 * 
	 * @return message payload
	 */
	public Payload getPayload();
	
	/**
	 * 
	 * @param sessionId
	 * @return
	 */
	public void setSessionId(String sessionId);
	
	/**
	 * 
	 * @return
	 */
	public String getSessionId();
	
	/**
	 * Build standard Request/Confirm message
	 * @return
	 */
	public String build();
}
