package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum UnitOfMeasure {

	Wh, // Watt-hours (energy). Default.
	kWh, // kiloWatt-hours (energy).
	varh, // Var-hours (reactive energy).
	kvarh, // kilovar-hours (reactive energy).
	W, // Watts (power).
	kW, // kilowatts (power).
	VA, // VoltAmpere (apparent power).
	kVA, // kiloVolt Ampere (apparent power).
	var, // Vars (reactive power).
	kvar, // kilovars (reactive power).
	A, // Amperes (current).
	V, // Voltage (r.m.s. AC).
	Celsius, // Degrees (temperature).
	Fahrenheit, // Degrees (temperature).
	K, // Degrees Kelvin (temperature).
	Percent; // Percentage.
}
