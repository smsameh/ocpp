package com.emobility.ocpp.socket;

/**
 * Contains the different server modes
 * 
 * @author Salah Abu Msameh
 */
public enum ServerMode {
	SOAP, JSON
}
