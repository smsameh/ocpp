package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum AvailabilityStatus {

	Accepted, //Request has been accepted and will be executed.
	Rejected, //Request has not been accepted and will not be executed.
	Scheduled, //Request has been accepted and will be executed when transaction(s) in progress have finished.
}
