package com.emobility.ocpp.socket.message;

import java.util.HashMap;
import java.util.Map;

/**
 * Different message type which determine the source and destination parts
 * 
 * @author Salah Abu Msameh
 */
public enum MessageType {

	CALL(2), //Client-to-Server
	CALLRESULT(3), //Server-to-Client
	CALLERROR(4); //Server-to-Client
	
	private static Map<Integer, MessageType> typesById = new HashMap<Integer, MessageType>(); 
	private int messageTypeId;
	
	static {
		for(MessageType type : MessageType.values()) {
			typesById.put(type.getMessageTypeId(), type);
		}
	}
	
	/**
	 * 
	 * @param messageTypeId
	 */
	MessageType(int messageTypeId) {
		this.messageTypeId = messageTypeId;
	}
	
	/**
	 * 
	 * @param messageTypeId
	 * @return
	 */
	public static MessageType getTypeById(int messageTypeId) {
		return typesById.get(messageTypeId);
	}
	
	/**
	 * 
	 * @return
	 */
	public int getMessageTypeId() {
		return messageTypeId;
	}
}
