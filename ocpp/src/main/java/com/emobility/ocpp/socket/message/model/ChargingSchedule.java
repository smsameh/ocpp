package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class ChargingSchedule {
	
	@XmlElement
	private int duration;
	
	@XmlElement
	private long startSchedule;
	
	@XmlElement
	private ChargingRateUnitType chargingRateUnit;
	
	@XmlElement
	private ChargingSchedulePeriod chargingSchedulePeriod;
	
	@XmlElement
	private double minChargingRate;

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public long getStartSchedule() {
		return startSchedule;
	}

	public void setStartSchedule(long startSchedule) {
		this.startSchedule = startSchedule;
	}

	public ChargingRateUnitType getChargingRateUnit() {
		return chargingRateUnit;
	}

	public void setChargingRateUnit(ChargingRateUnitType chargingRateUnit) {
		this.chargingRateUnit = chargingRateUnit;
	}

	public ChargingSchedulePeriod getChargingSchedulePeriod() {
		return chargingSchedulePeriod;
	}

	public void setChargingSchedulePeriod(ChargingSchedulePeriod chargingSchedulePeriod) {
		this.chargingSchedulePeriod = chargingSchedulePeriod;
	}

	public double getMinChargingRate() {
		return minChargingRate;
	}

	public void setMinChargingRate(double minChargingRate) {
		this.minChargingRate = minChargingRate;
	}
}
