package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class SetChargingProfileConfirmPayload implements Payload {
	
	private ChargingProfileStatus status;
	
	/**
	 * 
	 * @param status
	 */
	public SetChargingProfileConfirmPayload(ChargingProfileStatus status) {
		this.status = status;
	}

	@XmlElement
	public ChargingProfileStatus getStatus() {
		return status;
	}
	
	public void setStatus(ChargingProfileStatus status) {
		this.status = status;
	}
}
