package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class RemoteStartTransactionRequestPayload implements Payload {
	
	@XmlElement
	private int connectorId;
	
	@XmlElement
	private IdToken idToken;
	
	@XmlElement
	private ChargingProfile chargingProfile;

	public int getConnectorId() {
		return connectorId;
	}

	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}

	public IdToken getIdToken() {
		return idToken;
	}

	public void setIdToken(IdToken idToken) {
		this.idToken = idToken;
	}

	public ChargingProfile getChargingProfile() {
		return chargingProfile;
	}

	public void setChargingProfile(ChargingProfile chargingProfile) {
		this.chargingProfile = chargingProfile;
	}
}
