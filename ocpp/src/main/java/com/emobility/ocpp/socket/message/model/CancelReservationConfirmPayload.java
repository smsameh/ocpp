package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Cancel reservation confirm payload model object
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class CancelReservationConfirmPayload implements Payload {
	
	@XmlElement
	private CancelReservationStatus status;
	
	/**
	 * 
	 * @param status
	 */
	public CancelReservationConfirmPayload(CancelReservationStatus status) {
		this.status = status;
	}
	
	public CancelReservationStatus getStatus() {
		return status;
	}
	
	public void setStatus(CancelReservationStatus status) {
		this.status = status;
	}
}
