package com.emobility.ocpp.socket.message;

import com.emobility.ocpp.socket.message.model.Payload;
import com.emobility.ocpp.util.JSONUtils;

/**
 * Request Message common behavior
 * 
 * @author Salah Abu Msameh
 */
public class RequestMessage implements Message {
	
	private String uniqueId;
	private String action;
	private Payload payload;
	private String sessionId;
	
	/**
	 * 
	 * @param uniqueId
	 * @param action
	 * @param payload
	 */
	public RequestMessage(String uniqueId, String action, Payload payload) {
		this.uniqueId = uniqueId;
		this.action = action;
		this.payload = payload;
	}

	@Override
	public MessageType getMessageType() {
		return MessageType.CALL;
	}

	@Override
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	@Override
	public String getUniqueId() {
		return this.uniqueId;
	}

	@Override
	public void setPayload(Payload payload) {
		this.payload = payload;
	}

	@Override
	public Payload getPayload() {
		return this.payload;
	}
	
	/**
	 * 
	 * @return current message action
	 */
	public void setAction(String action) {
		this.action = action;
	}
	
	/**
	 * 
	 * @return current message action
	 */
	public String getAction() {
		return this.action;
	}
	
	@Override
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String getSessionId() {
		return this.sessionId;
	}
	
	@Override
	public String build() {
		return new StringBuilder("[")
			.append(getMessageType().getMessageTypeId())
			.append(", ")
			.append("\"").append(getUniqueId()).append("\"")
			.append(", ")
			.append("\"").append(getAction()).append("\"")
			.append(", ")
			.append("\"").append(JSONUtils.parse(getPayload())).append("\"")
			.append("]")
			.toString();
	}
}
