package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class IdToken {

	private String idToken;
	
	/**
	 * 
	 * @param idToken
	 */
	public IdToken(String idToken) {
		this.idToken = idToken;
	}

	@XmlElement(required = true)
	public String getIdToken() {
		return idToken;
	}

	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
}
