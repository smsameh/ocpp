package com.emobility.ocpp.socket.message;

/**
 * 
 * @author Salah Abu Msameh
 */
public class NoMessageTypeFound extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 * @param string
	 */
	public NoMessageTypeFound(String cause) {
		super(cause);
	}
	
	/**
	 * 
	 * @param cause
	 */
	public NoMessageTypeFound(Exception cause) {
		super(cause);
	}
}
