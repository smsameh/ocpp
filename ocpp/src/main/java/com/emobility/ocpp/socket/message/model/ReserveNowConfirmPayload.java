package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ReserveNowConfirmPayload implements Payload {

	private ReservationStatus status;
	
	/**
	 * 
	 * @param status
	 */
	public ReserveNowConfirmPayload(ReservationStatus status) {
		this.status = status;
	}

	public ReservationStatus getStatus() {
		return status;
	}
	
	public void setStatus(ReservationStatus status) {
		this.status = status;
	}
}
