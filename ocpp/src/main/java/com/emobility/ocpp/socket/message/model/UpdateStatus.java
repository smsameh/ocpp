package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum UpdateStatus {

	Accepted, //Local Authorization List successfully updated.
	Failed, //Failed to update the Local Authorization List.
	NotSupported, //Update of Local Authorization List is not supported by Charge Point.
	VersionMismatch; //Version number in the request for a differential update is less or equal then version number of current list.
}
