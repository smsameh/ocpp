package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class SetChargingProfileRequestPayload implements Payload {
	
	private int connectorId;
	private ChargingProfile chargingProfile;
	
	@XmlElement
	public int getConnectorId() {
		return connectorId;
	}
	
	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}
	
	@XmlElement
	public ChargingProfile getChargingProfile() {
		return chargingProfile;
	}
	
	public void setChargingProfile(ChargingProfile chargingProfile) {
		this.chargingProfile = chargingProfile;
	}
}
