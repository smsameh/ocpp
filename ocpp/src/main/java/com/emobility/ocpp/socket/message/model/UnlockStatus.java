package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum UnlockStatus {

	Unlocked, //Connector has successfully been unlocked.
	UnlockFailed, //Failed to unlock the connector.
	NotSupported; //Charge Point has no connector lock.
}
