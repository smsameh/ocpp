package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum CancelReservationStatus {

	Accepted, //Reservation for the identifier has been cancelled.
	Rejected, //Reservation could not be cancelled, because there is no reservation active for the identifier.
}
