package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Authorization request
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class AuthorizationRequestPayload implements Payload {
	
	private IdToken idToken;
	
	/**
	 * 
	 * @param idToken
	 */
	public AuthorizationRequestPayload(IdToken idToken) {
		super();
		this.idToken = idToken;
	}

	@XmlElement
	public IdToken getIdToken() {
		return idToken;
	}

	public void setIdToken(IdToken idToken) {
		this.idToken = idToken;
	}
}
