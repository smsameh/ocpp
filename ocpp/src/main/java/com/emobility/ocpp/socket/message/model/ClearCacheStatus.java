package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ClearCacheStatus {

	Accepted, //Command has been executed.
	Rejected; //Command has not been executed.
}
