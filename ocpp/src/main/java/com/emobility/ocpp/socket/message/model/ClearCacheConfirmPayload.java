package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ClearCacheConfirmPayload implements Payload {
	
	private ClearCacheStatus status;
	
	/**
	 * 
	 * @param status
	 */
	public ClearCacheConfirmPayload(ClearCacheStatus status) {
		this.status = status;
	}

	public ClearCacheStatus getStatus() {
		return status;
	}
	
	public void setStatus(ClearCacheStatus status) {
		this.status = status;
	}
}
