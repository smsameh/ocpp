package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ReadingContext {
	
	Interruption_Begin, //Interruption.Begin Value taken at start of interruption.
	Interruption_End, //Interruption.End Value taken when resuming after interruption.
	Other, //Other Value for any other situations.
	Sample_Clock, //Sample.Clock Value taken at clock aligned interval.
	Sample_Periodic, //Sample.Periodic Value taken as periodic sample relative to start time of transaction.
	Transaction_Begin, //Transaction.Begin Value taken at end of transaction.
	Transaction_End, //Transaction.End Value taken at start of transaction.
	Trigger; //Trigger Value taken in response to a TriggerMessage.req
}
