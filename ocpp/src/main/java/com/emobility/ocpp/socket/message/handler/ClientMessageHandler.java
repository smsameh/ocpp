package com.emobility.ocpp.socket.message.handler;

import java.io.IOException;

import javax.websocket.MessageHandler;
import javax.websocket.Session;

/**
 * Client message handler
 * 
 * @author Salah Abu Msameh
 */
public class ClientMessageHandler extends AbstractMessageHandler implements MessageHandler.Whole<String> {

	//any client will have only one session at a time
	private Session session;
	
	@Override
	public void onMessage(String message) {
		try {
			handleMessage(session.getId(), message);
		} catch (MessageHandlingException e) {
			//log error
		}
	}
	
	/**
	 * 
	 * @param session
	 */
	public void setSession(Session session) {
		this.session = session;
	}
	
	/**
	 * 
	 * @param message
	 */
	public void sendMessage(String message) {
		try {
			this.session.getBasicRemote().sendText(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
