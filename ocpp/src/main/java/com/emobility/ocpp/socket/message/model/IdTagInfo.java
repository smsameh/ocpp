package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Id Tag Info
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
@XmlType(propOrder = {"parentIdTag", "status", "expiryDate"})
public class IdTagInfo {
	
	/**
	 * Optional, this contains the date at which idTag should be removed from the Authorization Cache
	 */
	@XmlElement
	private long expiryDate;
	
	/**
	 * Optional, this contains the parent-identifier.
	 */
	@XmlElement
	private String parentIdTag;
	
	/**
	 * AuthorizationStatus Required, this contains whether the idTag has been 
	 * accepted or not by the Central System.
	 */
	@XmlElement(required = true)
	private String status;
	
	/**
	 * 
	 * @param expiryDate
	 * @param parentIdTag
	 * @param status
	 */
	private IdTagInfo(long expiryDate, String parentIdTag, String status) {
		this.expiryDate = expiryDate;
		this.parentIdTag = parentIdTag;
		this.status = status;
	}
	
	/**
	 * 
	 * @param expiryDate
	 * @param parentIdTag
	 * @return
	 */
	public static IdTagInfo create(long expiryDate, String parentIdTag, AuthorizationStatus status) {
		return new IdTagInfo(expiryDate, parentIdTag, status.name());
	}
}
