package com.emobility.ocpp.socket.message.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class GetCompositeScheduleConfirmPayload implements Payload {

	private GetCompositeScheduleStatus status;
	private int connectorId;
	private Date scheduleStart;
	private ChargingSchedule chargingSchedule;
	
	public GetCompositeScheduleStatus getStatus() {
		return status;
	}
	
	public void setStatus(GetCompositeScheduleStatus status) {
		this.status = status;
	}
	
	public int getConnectorId() {
		return connectorId;
	}
	
	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}
	
	public Date getScheduleStart() {
		return scheduleStart;
	}
	
	public void setScheduleStart(Date scheduleStart) {
		this.scheduleStart = scheduleStart;
	}
	
	public ChargingSchedule getChargingSchedule() {
		return chargingSchedule;
	}
	
	public void setChargingSchedule(ChargingSchedule chargingSchedule) {
		this.chargingSchedule = chargingSchedule;
	}
}
