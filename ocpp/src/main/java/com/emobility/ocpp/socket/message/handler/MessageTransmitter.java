package com.emobility.ocpp.socket.message.handler;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.session.NoSessionFoundException;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.socket.session.SessionManager;

/**
 * This class responsible for transmitting message to different destinations<br/>
 * 
 * This class contain a decouple transmitting message to destination in order to simplify the design for handling
 * the outgoing messages
 * 
 * @author Salah Abu Msameh
 */
@Component
public class MessageTransmitter {
	
	@Autowired
	private SessionManager sessionManager;
	
	/**
	 * Send message to a specific session 
	 * 
	 * @param sessionId
	 * @param message
	 * @throws NoSessionFoundException 
	 * @throws IOException 
	 */
	public void sendMessage(Message message) throws NoSessionMessageFound, IOException {
		
		WebSocketSession session = sessionManager.getSession(message.getSessionId());
		session.sendMessage(new TextMessage(message.build()));
	}
}
