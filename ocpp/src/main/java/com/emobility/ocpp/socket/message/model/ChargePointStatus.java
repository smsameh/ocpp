package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ChargePointStatus {

	Available, //When a Connector becomes available for a new user (Operative)
	Preparing, //When a Connector becomes no longer available for a new user but no charging session is active. 
	
			   //Typically a Connector is occupied when a user presents a tag, inserts a cable or a vehicle occupies the parking bay (Operative)
	Charging, //When the contactor of a Connector closes, allowing the vehicle to charge (Operative)
	
	SuspendedEVSE, // When the contactor of a Connector opens upon request of the EVSE, 
				   //e.g. due to a smart charging restriction or as the result of StartTransaction.conf indicating that charging is not allowed (Operative)
	SuspendedEV, //When the EVSE is ready to deliver energy but contactor is open, e.g. the EV is not ready.
	
	Finishing, //When a charging session has stopped at a Connector, but the Connector is not yet available for a new user, e.g. the cable has not been
			   //removed or the vehicle has not left the parking bay (Operative)
	
	Reserved, //When a Connector becomes reserved as a result of a Reserve Now command (Operative)
	
	Unavailable, //When a Connector becomes unavailable as the result of a Change Availability command or an event upon which the Charge Point transitions to
				 //unavailable at its discretion. Upon receipt of a Change Availability command, the status MAY change immediately or the change MAY be
				 //scheduled. When scheduled, the Status Notification shall be send when the availability change becomes effective (Inoperative)
	
	Faulted; //When a Charge Point or connector has reported an error and is not available for energy delivery (Inoperative).
}
