package com.emobility.ocpp.socket.message.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class BootNotificationConfirmPayload implements Payload {

	private Date currentTime;
	private int interval;
	private RegistrationStatus status;
	
	@XmlElement
	public Date getCurrentTime() {
		return currentTime;
	}
	
	public void setCurrentTime(Date currentTime) {
		this.currentTime = currentTime;
	}
	
	@XmlElement
	public int getInterval() {
		return interval;
	}
	
	public void setInterval(int interval) {
		this.interval = interval;
	}
	
	@XmlElement
	public RegistrationStatus getStatus() {
		return status;
	}
	
	public void setStatus(RegistrationStatus status) {
		this.status = status;
	}
}
