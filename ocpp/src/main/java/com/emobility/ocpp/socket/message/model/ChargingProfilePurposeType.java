package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ChargingProfilePurposeType {

	ChargePointMaxProfile,
	TxDefaultProfile,
	TxProfile;
}
