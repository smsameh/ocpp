package com.emobility.ocpp.socket.message;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum MessageErrors {

	EMPTY_MESSAGE("ER-001", "empty message"),
	MISSING_MSG_TYPE("ER-002", "missing message type id"),
	MISSING_MSG_UNIQUE_ID("ER-003", "missing message unique id"),
	MISSING_MSG_ACTION("ER-004", "missing message action"),
	MISSING_MSG_PAYLOAD("ER-005", "missing message payload/content"),
	INVALID_MSG_FORMAT("ER-006", "Invalid message format"),
	INVALID_MSG_TYPE("ER-007", "Invalid message Type"),
	UNSUPPORTED_MSG_OPERATION("ER-008", "Unsupported message action"),
	INVALID_MSG_UNIQUE_ID("ER-009", "Invalid message unique id, the message unique id max length is 36 char"),
	UNSUPPORTED_MSG_TYPE("ER-010", "Unsupported message type"),
	NO_REQ_MSG_FOR_MSG_ID("ER-011", "No request message found for the given message unique id");
	
	private String errorCode;
	private String errorDesc;
	
	/**
	 * 
	 * @param errorCode
	 * @param errorDesc
	 */
	private MessageErrors(String errorCode, String errorDesc) {
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}
}
