package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class KeyValue {

	private String key;
	private boolean readonly;
	private String value;
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public boolean isReadonly() {
		return readonly;
	}
	
	public void setReadonly(boolean readonly) {
		this.readonly = readonly;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}
