package com.emobility.ocpp.socket;

import java.util.List;
import java.util.Map;

import javax.websocket.ClientEndpointConfig;

/**
 * Socket client request header configurator
 * 
 * @author Salah Abu Msameh
 */
public class ClientHeadersConfigurator extends ClientEndpointConfig.Configurator {
	
	@Override
	public void beforeRequest(Map<String, List<String>> headers) {
		
		// TODO add socket request header as descriped in the json spec document
		/**
		 * GET /webServices/ocpp/CP3211 HTTP/1.1
		 * Host: some.server.com:33033
		 * Upgrade: websocket
		 * Connection: Upgrade
		 * Sec-WebSocket-Key: x3JJHMbDL1EzLkh9GBhXDw==
		 * Sec-WebSocket-Protocol: ocpp1.6, ocpp1.5
		 * Sec-WebSocket-Version: 13
		 */
		super.beforeRequest(headers);
	}
}
