package com.emobility.ocpp.socket.message.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class GetConfigurationRequestPayload implements Payload {
	
	private List<String> key;
	
	@XmlElement
	public List<String> getKey() {
		return key;
	}

	public void setKey(List<String> key) {
		this.key = key;
	}
}
