package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class ClearChargingProfileConfirmPayload implements Payload {

	private ClearChargingProfileStatus status;
	
	@XmlElement
	public ClearChargingProfileStatus getStatus() {
		return status;
	}
	
	public void setStatus(ClearChargingProfileStatus status) {
		this.status = status;
	}
}
