package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ChargingRateUnitType {

	W, //Watts (power).
	A, //Amperes (current).
}
