package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class RemoteStopTransactionRequestPayload implements Payload {
	
	@XmlElement
	private int transactionId;
	
	/**
	 * 
	 * @param transactionId
	 */
	public RemoteStopTransactionRequestPayload(int transactionId) {
		this.transactionId = transactionId;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
}
