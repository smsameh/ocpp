package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ReservationStatus {

	Accepted, //Reservation has been made.
	Faulted, //Reservation has not been made, because connectors or specified connector are in a faulted state.
	Occupied, //Reservation has not been made. All connectors or the specified connector are occupied.
	Rejected, //Reservation has not been made. Charge Point is not configured to accept reservations.
	Unavailable, //Reservation has not been made, because connectors or specified connector are in an unavailable state.
}
