package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum Location {

	Body, //Body Measurement inside body of Charge Point (e.g. Temperature)
	Cable, //Cable Measurement taken from cable between EV and Charge Point
	EV, //EV Measurement taken by EV
	Inlet, //Inlet Measurement at network (�grid�) inlet connection
	Outlet; //Outlet
}
