package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class DataTransferConfirmPayload implements Payload {

	@XmlElement
	private DataTransferStatus status;
	
	@XmlElement
	private String data;

	public DataTransferStatus getStatus() {
		return status;
	}

	public void setStatus(DataTransferStatus status) {
		this.status = status;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
