package com.emobility.ocpp.socket.message.parse;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.socket.message.ConfirmMessage;
import com.emobility.ocpp.socket.message.ErrorMessage;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageErrors;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.MessageType;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.AuthorizationRequestPayload;
import com.emobility.ocpp.socket.message.model.BootNotificationRequestPayload;
import com.emobility.ocpp.socket.message.model.CancelReservationConfirmPayload;
import com.emobility.ocpp.socket.message.model.ChangeAvailabilityConfirmPayload;
import com.emobility.ocpp.socket.message.model.ChangeConfigurationConfirmPayload;
import com.emobility.ocpp.socket.message.model.ClearCacheConfirmPayload;
import com.emobility.ocpp.socket.message.model.ClearChargingProfileConfirmPayload;
import com.emobility.ocpp.socket.message.model.DataTransferConfirmPayload;
import com.emobility.ocpp.socket.message.model.DataTransferRequestPayload;
import com.emobility.ocpp.socket.message.model.DiagnosticsStatusNotificationRequestPayload;
import com.emobility.ocpp.socket.message.model.EmptyPayload;
import com.emobility.ocpp.socket.message.model.FirmwareStatusNotificationRequestPayload;
import com.emobility.ocpp.socket.message.model.GetCompositeScheduleConfirmPayload;
import com.emobility.ocpp.socket.message.model.GetConfigurationConfirmPayload;
import com.emobility.ocpp.socket.message.model.GetDiagnosticsConfirmPayload;
import com.emobility.ocpp.socket.message.model.GetLocalListVersionConfirmPayload;
import com.emobility.ocpp.socket.message.model.HeartbeatRequestPayload;
import com.emobility.ocpp.socket.message.model.MeterValuesRequestPayload;
import com.emobility.ocpp.socket.message.model.Payload;
import com.emobility.ocpp.socket.message.model.RemoteStartTransactionConfirmPayload;
import com.emobility.ocpp.socket.message.model.RemoteStopTransactionConfirmPayload;
import com.emobility.ocpp.socket.message.model.ResetConfirmPayload;
import com.emobility.ocpp.socket.message.model.SendLocalListConfirmPayload;
import com.emobility.ocpp.socket.message.model.SetChargingProfileConfirmPayload;
import com.emobility.ocpp.socket.message.model.StartTransactionRequestPayload;
import com.emobility.ocpp.socket.message.model.StatusNotificationRequestPayload;
import com.emobility.ocpp.socket.message.model.StopTransactionRequestPayload;
import com.emobility.ocpp.socket.message.model.TriggerMessageConfirmPayload;
import com.emobility.ocpp.socket.message.model.UnlockConnectorConfirmPayload;
import com.emobility.ocpp.socket.message.model.UpdateFirmwareConfirmPayload;
import com.emobility.ocpp.socket.message.validate.JSONMessageValidator;
import com.emobility.ocpp.util.JSONUtils;
import com.emobility.ocpp.util.Log;

/**
 * JSON message converter
 * 
 * @author Salah Abu Msameh
 */
@Component
public class JSONMessageParser implements MessageParser {
	
	@Autowired
	private JSONMessageValidator msgValidator;
	
	@Autowired
	private MessageTracker msgTracker;
	
	/**Types Mapping*/
	private Map<String, Class<? extends Payload>> reqTypesMapping = new HashMap<String, Class<? extends Payload>>();
	private Map<String, Class<? extends Payload>> confTypesMapping = new HashMap<String, Class<? extends Payload>>();
	
	/**
	 * 
	 */
	public JSONMessageParser() {
		initTypeMapping();
	}

	@Override
	public Message parse(String message) {
		
		message = message.trim();
		
		if(StringUtils.isEmpty(message)) {
			return new ErrorMessage("", MessageErrors.EMPTY_MESSAGE.getErrorCode(),
					MessageErrors.EMPTY_MESSAGE.getErrorDesc(), new EmptyPayload());
		}
		
		if(!msgValidator.isValid(message)) {
			return new ErrorMessage("", MessageErrors.INVALID_MSG_FORMAT.getErrorCode(),
					MessageErrors.INVALID_MSG_FORMAT.getErrorDesc(), new EmptyPayload());
		}
 		
		String messageType = extractMessageType(message);
		
		if(StringUtils.isEmpty(messageType)) {
			return new ErrorMessage("", MessageErrors.MISSING_MSG_TYPE.getErrorCode(),
					MessageErrors.MISSING_MSG_TYPE.getErrorDesc(), new EmptyPayload());
		}
		
		switch (MessageType.getTypeById(Integer.parseInt(messageType))) {
			case CALL: {
				return validateAndGetCallMessage(message);
			}
			case CALLRESULT: {
				return validateAndGetCallResultMessage(message);
			}
			case CALLERROR: {
				return validateAndGetErrorMessage(message);
			}
			default: {
				return new ErrorMessage("", MessageErrors.INVALID_MSG_TYPE.getErrorCode(),
						MessageErrors.INVALID_MSG_TYPE.getErrorDesc(), new EmptyPayload());
			}
		}
	}
	
	@Override
	public String parse(Message resMessage) {
		return null;
	}

	/**
	 * Validate call message type content and return weather a valid call message or error message if 
	 * one of the validation fails
	 * 
	 * @param message
	 * @return
	 */
	private Message validateAndGetCallMessage(String message) {
		
		//1. get and validate unique id
		String uniqueId = extractUniqueId(message);
		
		if(StringUtils.isEmpty(uniqueId)) {
			return new ErrorMessage("", MessageErrors.MISSING_MSG_UNIQUE_ID.getErrorCode(),
					MessageErrors.MISSING_MSG_UNIQUE_ID.getErrorDesc(), new EmptyPayload());
		}
		
		if(!msgValidator.isValidMessageUniqueId(uniqueId)) {
			return new ErrorMessage(uniqueId, MessageErrors.MISSING_MSG_UNIQUE_ID.getErrorCode(),
					MessageErrors.MISSING_MSG_UNIQUE_ID.getErrorDesc(), new EmptyPayload());
		}
		
		//2. get and validate action
		String action = extractAction(message);
		
		if(StringUtils.isEmpty(action)) {
			return new ErrorMessage(uniqueId, MessageErrors.MISSING_MSG_ACTION.getErrorCode(),
					MessageErrors.MISSING_MSG_ACTION.getErrorDesc(), new EmptyPayload());
		}
		
		//3. get and validate payload
		String payload = extractPayload(message);
		
		if(StringUtils.isEmpty(payload)) {
			return new ErrorMessage(uniqueId, MessageErrors.MISSING_MSG_PAYLOAD.getErrorCode(),
					MessageErrors.MISSING_MSG_PAYLOAD.getErrorDesc(), new EmptyPayload());
		}
		
		//4. return a request message
		try {
			return new RequestMessage(uniqueId, action, JSONUtils.parse(payload, reqTypesMapping.get(action)));
			
		} catch (Exception ex) {
			Log.error(JSONMessageParser.class, ex.getMessage());
			return new ErrorMessage(uniqueId, MessageErrors.INVALID_MSG_FORMAT.getErrorCode(),
					MessageErrors.INVALID_MSG_FORMAT.getErrorDesc(), new EmptyPayload());
		}
	}
	
	/**
	 * Validate result call message type content and return weather a valid call message or error message if 
	 * one of the validation fails
	 * 
	 * @param message
	 * @return
	 */
	private Message validateAndGetCallResultMessage(String message) {
		
		//1. get and validate unique id
		String uniqueId = extractUniqueId(message);
		
		if(StringUtils.isEmpty(uniqueId)) {
			return new ErrorMessage("", MessageErrors.MISSING_MSG_UNIQUE_ID.getErrorCode(),
					MessageErrors.MISSING_MSG_UNIQUE_ID.getErrorDesc(), new EmptyPayload());
		}
		
		RequestMessage relatedSubmittedReqMsg = msgTracker.getSubmittedMessage(uniqueId);
		
		if(relatedSubmittedReqMsg == null) {
			return new ErrorMessage("", MessageErrors.NO_REQ_MSG_FOR_MSG_ID.getErrorCode(),
					MessageErrors.NO_REQ_MSG_FOR_MSG_ID.getErrorDesc(), new EmptyPayload());
		}
		
		//2. get and validate payload
		String payload = extractPayload(message);
		
		if(StringUtils.isEmpty(payload)) {
			return new ErrorMessage(uniqueId, MessageErrors.MISSING_MSG_PAYLOAD.getErrorCode(),
					MessageErrors.MISSING_MSG_PAYLOAD.getErrorDesc(), new EmptyPayload());
		}
		
		//3. return a confirm message
		try {
			return new ConfirmMessage("", uniqueId, JSONUtils.parse(payload, confTypesMapping.get(relatedSubmittedReqMsg.getAction())));
		} catch (Exception ex) {
			return new ErrorMessage(uniqueId, MessageErrors.INVALID_MSG_FORMAT.getErrorCode(),
					MessageErrors.INVALID_MSG_FORMAT.getErrorDesc(), new EmptyPayload());
		}
	}
	
	/**
	 * 
	 * @param message
	 * @return
	 */
	private Message validateAndGetErrorMessage(String message) {
		
		String uniqueId = extractUniqueId(message);
		String action = extractAction(message);
		String payload = extractPayload(message);
		
		try {
			return new RequestMessage(uniqueId, action, JSONUtils.parse(payload, reqTypesMapping.get(action)));
		} catch (Exception ex) {
			return new ErrorMessage(uniqueId, MessageErrors.INVALID_MSG_FORMAT.getErrorCode(),
					MessageErrors.INVALID_MSG_FORMAT.getErrorDesc(), new EmptyPayload());
		}
	}
	
	/**
	 * 
	 * @param message
	 */
	private String extractMessageType(String message) {
		
		Pattern pattern = Pattern.compile("(\\[)+\\s*([2-4])+\\s*(\\,)+");
		Matcher matcher = pattern.matcher(message);
		
		if(matcher.find()) {
			return matcher.group(2);
		}
		
		return "";
	}
	
	/**
	 * 
	 * @param message
	 * @return
	 */
	private String extractUniqueId(String message) {
		
		Pattern pattern = Pattern.compile("(\\,)+\\s*(\\\")+\\s*([0-9]+)\\s*(\\\")\\s*(\\,)+");
		Matcher matcher = pattern.matcher(message);
		
		if(matcher.find()) {
			return matcher.group(3);
		}
		
		return "";
	}
	
	/**
	 * 
	 * @param message
	 * @return
	 */
	private String extractAction(String message) {
		
		Pattern pattern = Pattern.compile("(\\,)+\\s*(\\\")+\\s*([a-z A-Z]+)\\s*(\\\")\\s*(\\,)+");
		Matcher matcher = pattern.matcher(message);
		
		if(matcher.find()) {
			return matcher.group(3);
		}
		
		return "";
	}
	
	/**
	 * 
	 * @param message
	 * @return
	 */
	private String extractPayload(String message) {
		
		int startIndex = message.indexOf("{");
		int endIndex = message.lastIndexOf("}");
		
		if(startIndex == -1 || endIndex == -1) {
			return "";
		}
		
		return message.substring(startIndex, endIndex + 1);
	}
	
	/**
	 * used in junit test only
	 * @param msgValidator
	 */
	public void setMsgValidator(JSONMessageValidator msgValidator) {
		this.msgValidator = msgValidator;
	}
	
	/**
	 * 
	 */
	private void initTypeMapping() {
		
		//charge point requests
		this.reqTypesMapping.put(OperationActions.BOOT_NOTIFICATION, BootNotificationRequestPayload.class);
		this.reqTypesMapping.put(OperationActions.DATA_TRANSFER, DataTransferRequestPayload.class);
		this.reqTypesMapping.put(OperationActions.HEARTBEAT, HeartbeatRequestPayload.class);
		this.reqTypesMapping.put(OperationActions.AUTHORIZATION, AuthorizationRequestPayload.class);
		this.reqTypesMapping.put(OperationActions.START_TRANSACTION, StartTransactionRequestPayload.class);
		this.reqTypesMapping.put(OperationActions.STATUS_NOTIFICATION, StatusNotificationRequestPayload.class);
		this.reqTypesMapping.put(OperationActions.STOP_TRANSACTION, StopTransactionRequestPayload.class);
		this.reqTypesMapping.put(OperationActions.DIAGNOSTICS_STATUS_TRANSACTION, DiagnosticsStatusNotificationRequestPayload.class);
		this.reqTypesMapping.put(OperationActions.FIRMWARE_STATUS_TRANSACTION, FirmwareStatusNotificationRequestPayload.class);
		this.reqTypesMapping.put(OperationActions.METER_VALUES, MeterValuesRequestPayload.class);
		
		//charge point confirm
		this.confTypesMapping.put(OperationActions.REMOTE_START_TRANSACTION, RemoteStartTransactionConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.REMOTE_STOP_TRANSACTION, RemoteStopTransactionConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.CANCEL_RESERVATION, CancelReservationConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.CHANGE_AVAILABILITY, ChangeAvailabilityConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.SET_CHARGING_PROFILE, SetChargingProfileConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.CHANGE_CONFIGURATION_PROFILE, ChangeConfigurationConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.CLEAR_CACHE, ClearCacheConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.CLEAR_CHARGING_PROFILE, ClearChargingProfileConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.DATA_TRANSFER, DataTransferConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.GET_COMPOSITE_SCHEDULE, GetCompositeScheduleConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.GET_CONFIGURATION, GetConfigurationConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.GET_DIAGNOSTICS, GetDiagnosticsConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.GET_LOCAL_LIST_VERSION, GetLocalListVersionConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.RESET, ResetConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.SEND_LOCAL_LIST, SendLocalListConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.TRIGGER_MESSAGE, TriggerMessageConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.UNLOCK_CONNECTOR, UnlockConnectorConfirmPayload.class);
		this.confTypesMapping.put(OperationActions.UPDATE_FIRMWARE, UpdateFirmwareConfirmPayload.class);
	}
}
