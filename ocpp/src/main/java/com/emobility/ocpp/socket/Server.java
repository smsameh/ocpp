package com.emobility.ocpp.socket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import com.emobility.ocpp.profile.config.OcppConfig;

/**
 * 
 * @author Salah Abu Msameh
 */
@Configuration
@EnableWebSocket
public class Server implements WebSocketConfigurer {
	
	@Autowired
	private OcppConfig ocppConfig;
	
	public static final String PATH_ATT_UNIQUE_IDENTIFIER = "chargePointUniqueIdentifier";
	
	@Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(ocppConfig.getWebSocketHandler(), "/ocpp/{"+ PATH_ATT_UNIQUE_IDENTIFIER +"}")
        //.setHandshakeHandler(new DefaultHandshakeHandler(new TomcatRequestUpgradeStrategy()))
        .addInterceptors(ocppConfig.getHandshakeInterceptor())
        .setAllowedOrigins("*");
    }
}
