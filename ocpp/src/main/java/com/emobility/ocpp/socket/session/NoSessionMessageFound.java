package com.emobility.ocpp.socket.session;

/**
 * 
 * @author Salah Abu Msameh
 */
public class NoSessionMessageFound extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 * @param string
	 */
	public NoSessionMessageFound(String cause) {
		super(cause);
	}
}
