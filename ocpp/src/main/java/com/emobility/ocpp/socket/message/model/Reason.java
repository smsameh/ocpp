package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class Reason {

	@XmlElement
	private int reasonCode;
	
	/**
	 * 
	 * @param reasonCode
	 */
	public Reason(int reasonCode) {
		this.reasonCode = reasonCode;
	}

	public int getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(int reasonCode) {
		this.reasonCode = reasonCode;
	}
}
