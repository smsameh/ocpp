package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class TriggerMessageConfirmPayload implements Payload {

	private TriggerMessageStatus status;
	
	@XmlElement
	public TriggerMessageStatus getStatus() {
		return status;
	}
	
	public void setStatus(TriggerMessageStatus status) {
		this.status = status;
	}
}
