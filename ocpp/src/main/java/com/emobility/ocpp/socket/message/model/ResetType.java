package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum ResetType {

	Hard, //Full reboot of Charge Point software.
	Soft, //Return to initial status, gracefully terminating any transactions in progress.
}
