package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class GetLocalListVersionConfirmPayload implements Payload {

	private int listVersion;
	
	@XmlElement
	public int getListVersion() {
		return listVersion;
	}
	
	public void setListVersion(int listVersion) {
		this.listVersion = listVersion;
	}
}
