package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class RemoteStopTransactionConfirmPayload implements Payload {

	@XmlElement
	private RemoteStartStopStatus status;
	
	public RemoteStartStopStatus getStatus() {
		return status;
	}
	
	public void setStatus(RemoteStartStopStatus status) {
		this.status = status;
	}
}
