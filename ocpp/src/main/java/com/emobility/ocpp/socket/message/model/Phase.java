package com.emobility.ocpp.socket.message.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum Phase {

	L1, // Measured on L1
	L2, // Measured on L2
	L3, // Measured on L3
	N, // Measured on Neutral
	L1_N, // Measured on L1 with respect to Neutral conductor
	L2_N, // Measured on L2 with respect to Neutral conductor
	L3_N, // Measured on L3 with respect to Neutral conductor
	L1_L2, // Measured between L1 and L2
	L2_L3, // Measured between L2 and L3
	L3_L1; // Measured between L3 and L1
}
