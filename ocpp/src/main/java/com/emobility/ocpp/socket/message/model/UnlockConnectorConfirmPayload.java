package com.emobility.ocpp.socket.message.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Salah Abu Msameh
 */
@XmlRootElement
public class UnlockConnectorConfirmPayload implements Payload {

	private UnlockStatus status;
	
	@XmlElement
	public UnlockStatus getStatus() {
		return status;
	}
	
	public void setStatus(UnlockStatus status) {
		this.status = status;
	}
}
