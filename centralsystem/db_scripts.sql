---------------------------
-- Central System Tables --
---------------------------

-------------------- Charge Points Tables --------------------
CREATE TABLE charge_points (
	CP_ID INT PRIMARY KEY AUTO_INCREMENT,
	UNIQUE_IDENTIFIER VARCHAR(100) UNIQUE, 
	CP_NAME VARCHAR(200),
	CP_SERIAL_NO VARCHAR(100) NOT NULL,
	CP_MODEL VARCHAR(100),
	CP_VENDOR VARCHAR(200),
	CP_STATUS INT
);

--1.1
ALTER TABLE charge_points MODIFY CP_STATUS varchar(20);
UPDATE charge_points SET CP_STATUS = 'active';

--1.2
ALTER TABLE charge_points ADD COLUMN PRICE_ID INT;
ALTER TABLE charge_points ADD CONSTRAINT CP_PRICE_FK FOREIGN KEY (PRICE_ID) REFERENCES cp_prices(PRICE_ID);
ALTER TABLE charge_points ADD COLUMN IP_ADDRESS VARCHAR(20);
ALTER TABLE charge_points ADD COLUMN PORT VARCHAR(10);
ALTER TABLE charge_points ADD COLUMN ADDRESS VARCHAR(500);
ALTER TABLE charge_points ADD COLUMN CITY VARCHAR(100);
ALTER TABLE charge_points ADD COLUMN CONTACT_PERSON VARCHAR(100);

CREATE TABLE cp_prices (
	PRICE_ID INT PRIMARY KEY AUTO_INCREMENT,
	AC_ERU_KWH DECIMAL,
	AC_ERU_HOUR DECIMAL,
	REVERSE_ERU_MIN DECIMAL,
	DC_ERU_KWH DECIMAL,
	DC_ERU_MIN DECIMAL
);

CREATE TABLE users (
  USER_ID VARCHAR(20) NOT NULL,
  FULLNAME VARCHAR(100),
  STATUS BIT(1) NOT NULL,
  ORGANIZATION_ID INT(11),
  SUBSCRIPTION_DATE DATETIME,
  PAYMENT_METHOD VARCHAR(10),
  PAYMENT_INFO VARCHAR(100),
  PRIMARY KEY (USER_ID)
);

--1.2
ALTER TABLE users MODIFY STATUS INT(1);

-------------------- Events Auditor Tables --------------------
CREATE TABLE event_operations (
	OPERATION_ID INT PRIMARY KEY,
	OPERATION_NAME VARCHAR(200) NOT NULL
);

CREATE TABLE event_fields (
	FIELD_ID INT PRIMARY KEY,
	FIELD_DESC VARCHAR(100) NOT NULL
);

CREATE TABLE event_operation_fields_mapping (
	OPERATION_ID INT,
	FIELD_ID INT,
	JSON_ATT_KEY VARCHAR(50),
	CONSTRAINT MAPPING_PRIMARY_KEY PRIMARY KEY (OPERATION_ID, FIELD_ID),
	CONSTRAINT MAPPING_OPERATION_FK FOREIGN KEY (OPERATION_ID) REFERENCES event_operations (OPERATION_ID),
	CONSTRAINT MAPPING_FIELD_FK FOREIGN KEY (FIELD_ID) REFERENCES event_fields (FIELD_ID)
);

CREATE TABLE bss_events_master (
	EVENT_ID INT PRIMARY KEY AUTO_INCREMENT,
	CP_UNIQUE_IDENTIFIER VARCHAR(100),
	MSG_TYPE_ID INT,
	OPERATION_ID INT,
	EVENT_DATE DATE,
	CONSTRAINT EVENTS_MASTER_OPERATION_FK FOREIGN KEY (OPERATION_ID) REFERENCES event_operations (OPERATION_ID)
);

CREATE TABLE bss_events_details (
	EVENT_DETAIL_ID INT PRIMARY KEY AUTO_INCREMENT,
	EVENT_ID INT,
	FIELD_ID INT,
	FIELD_VALUE VARCHAR(1000),
	CONSTRAINT DETAILS_MASTER_FK FOREIGN KEY (EVENT_ID) REFERENCES bss_events_master (EVENT_ID),
	CONSTRAINT EVENTS_DETAILS_FIELD_FK FOREIGN KEY (FIELD_ID) REFERENCES event_fields (FIELD_ID)
);
---------------------------
----- DML Statements ------
---------------------------

------ Event Auditor Operations -------------
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(1, 'Authorization Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(2, 'Authorization Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(3, 'Boot Notification Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(4, 'Boot Notification Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(5, 'Data Transfer Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(6, 'Data Transfer Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(7, 'Diagnostics Status Notification Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(8, 'Diagnostics Status Notification Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(9, 'Firmware Status Notification Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(10, 'Firmware Status Notification Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(11, 'Heartbeat Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(12, 'Heartbeat Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(13, 'Meter Value Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(14, 'Meter Value Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(15, 'Start Transaction Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(16, 'Start Transaction Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(17, 'Status Notification Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(18, 'Status Notification Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(19, 'Stop Transaction Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(20, 'Stop Transaction Confirm');

--v1.1
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(50, 'Remote Start Transaction Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(51, 'Remote Start Transaction Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(52, 'Remote Stop Transaction Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(53, 'Remote Stop Transaction Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(54, 'Cancel Reservation Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(55, 'Cancel Reservation Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(56, 'Change Availability Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(57, 'Change Availability Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(58, 'Set Charging Profile Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(59, 'Set Charging Profile Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(60, 'Change Configuration Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(61, 'Change Configuration Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(62, 'Clear Cache Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(63, 'Clear Cache Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(64, 'Clear Charging Profile Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(65, 'Clear Charging Profile Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(66, 'Get Composite Schedule Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(67, 'Get Composite Schedule Confirm');

--v1.2
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(68, 'Get Configuration Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(69, 'Get Configuration Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(72, 'Get Local List Version Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(73, 'Get Local List Version Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(74, 'Reserve Now Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(75, 'Reserve Now Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(76, 'Reset Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(77, 'Reset Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(78, 'Send Local List Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(79, 'Send Local List Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(80, 'Trigger Message Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(81, 'Trigger Message Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(82, 'Unlock Connector Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(83, 'Unlock Connector Confirm');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(84, 'Update Firmware Request');
INSERT INTO event_operations (OPERATION_ID, OPERATION_NAME) VALUES(85, 'Update Firmware Confirm');


------ Event Auditor Fields -------------
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(1, 'Id Token');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(2, 'Charge Box Serial Number');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(3, 'Charge Point Model');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(4, 'Charge Point Serial Number');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(5, 'Charge Point Vendor');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(6, 'Firmware Version');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(7, 'iccid');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(8, 'imsi');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(9, 'Meter Serial Number');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(10, 'Meter Type');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(11, 'Current Time');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(12, 'Interval');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(13, 'Status');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(14, 'Vendor Id');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(15, 'Message Id');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(16, 'Data');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(17, 'Connector Id');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(18, 'Transaction Id');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(19, 'Timestamp');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(20, 'Sample Value');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(21, 'Reading Context');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(22, 'Value Format');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(23, 'Measurand');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(24, 'Phase');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(25, 'Location');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(26, 'Unit');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(27, 'Meter Start');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(28, 'Reservation Id');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(29, 'Expiry Date');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(30, 'ParentId Tag');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(31, 'Meter Stop');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(32, 'Reason Code');

--v1.1
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(33, 'Charging Profile Id');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(34, 'Stack Level');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(35, 'Charging Profile Purpose');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(36, 'Charging Profile Kind');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(37, 'Recurrency Kind');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(38, 'Valid From');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(39, 'Valid To');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(40, 'Duration');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(41, 'Start Schedule');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(42, 'Charging Rate Unit');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(43, 'Min Charging Rate');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(44, 'Start Period');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(45, 'Limit');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(46, 'Number Phases');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(47, 'Availability Type');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(48, 'Charging Schedule Period');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(49, 'Key');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(50, 'Value');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(51, 'Id');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(52, 'Schedule Start Date');

--v1.2
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(53, 'Readonly');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(54, 'List Version');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(55, 'Reset Type');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(56, 'Update Type');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(57, 'Message Trigger');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(58, 'Location');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(59, 'Retries');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(60, 'Retrieve Date');
INSERT INTO event_fields (FIELD_ID, FIELD_DESC) VALUES(61, 'Retry Interval');


------------------ operation fields mapping ----------------------
-------------------------------------------------------------------
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(1, 1, 'idToken');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(2, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(2, 29, 'expiryDate');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(2, 30, 'parentIdTag');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(3, 2, 'chargeBoxSerialNumber');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(3, 3, 'chargePointModel');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(3, 4, 'chargePointSerialNumber');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(3, 5, 'chargePointVendor');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(3, 6, 'firmwareVersion');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(3, 7, 'iccid');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(3, 8, 'imsi');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(3, 9, 'meterSerialNumber');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(3, 10, 'meterType');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(4, 11, 'currentTime');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(4, 12, 'interval');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(4, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(5, 14, 'vendorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(5, 15, 'messageId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(5, 16, 'data');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(6, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(6, 16, 'data');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(7, 13, 'diagnosticsStatus');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(9, 13, 'firmwareStatus');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(12, 11, 'currentTime');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(13, 11, 'timestamp');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(13, 17, 'connectorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(13, 18, 'transactionId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(13, 19, 'timestamp');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(13, 20, 'value');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(13, 21, 'readingContext');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(13, 22, 'valueFormat');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(13, 23, 'measurand');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(13, 24, 'phase');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(13, 25, 'location');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(13, 26, 'unit');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(15, 1, 'idToken');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(15, 17, 'connectorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(15, 19, 'timestamp');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(15, 27, 'meterStart');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(15, 28, 'reservationId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(16, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(16, 18, 'transactionId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(16, 29, 'expiryDate');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(16, 30, 'parentIdTag');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(17, 1, 'idToken');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(17, 17, 'connectorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(17, 19, 'timestamp');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(17, 27, 'meterStart');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(17, 28, 'reservationId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(18, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 1, 'idToken');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 18, 'transactionId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 19, 'timestamp');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 20, 'value');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 21, 'readingContext');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 22, 'valueFormat');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 23, 'measurand');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 24, 'phase');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 25, 'location');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 26, 'unit');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 31, 'meterStop');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(19, 32, 'reasonCode');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(20, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(20, 29, 'expiryDate');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(20, 30, 'parentIdTag');

--v1.1
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 1, 'idToken');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 17, 'connectorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 18, 'transactionId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 33, 'chargingProfileId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 34, 'stackLevel');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 35, 'chargingProfilePurpose');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 36, 'chargingProfileKind');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 37, 'recurrencyKind');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 38, 'validFrom');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 39, 'validTo');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 40, 'duration');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 41, 'startSchedule');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 42, 'chargingRateUnit');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 43, 'minChargingRate');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 44, 'startPeriod');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 45, 'limit');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(50, 46, 'numberPhases');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(51, 4, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(52, 18, 'transactionId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(53, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(54, 28, 'reservationId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(55, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(56, 4, '');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(56, 17, 'connectorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(56, 47, 'type');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(57, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 17, 'connectorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 18, 'transactionId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 33, 'chargingProfileId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 34, 'stackLevel');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 35, 'chargingProfilePurpose');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 36, 'chargingProfileKind');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 37, 'recurrencyKind');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 38, 'validFrom');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 39, 'validTo');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 40, 'duration');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 41, 'startSchedule');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 42, 'chargingRateUnit');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 43, 'minChargingRate');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(58, 48, 'chargingSchedulePeriod');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(59, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(60, 49, 'key');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(60, 50, 'value');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(61, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(63, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(64, 17, 'connectorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(64, 34, 'stackLevel');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(64, 35, 'chargingProfilePurpose');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(64, 51, 'id');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(65, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(66, 17, 'connectorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(66, 40, 'duration');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(66, 42, 'chargingRateUnit');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(67, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(67, 17, 'connectorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(67, 40, 'duration');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(67, 41, 'startSchedule');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(67, 42, 'chargingRateUnit');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(67, 43, 'minChargingRate');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(67, 44, 'startPeriod');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(67, 45, 'limit');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(67, 46, 'numberPhases');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES(67, 52, 'scheduleStart');

--v1.2
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (68, 49, 'key');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (69, 49, 'key');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (69, 53, 'readonly');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (69, 50, 'value');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (73, 54, 'listVersion');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (76, 55, 'type');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (77, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (78, 54, 'listVersion');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (78, 1, 'idToken');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (78, 29, 'expiryDate');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (78, 30, 'parentIdTag');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (78, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (78, 56, 'updateType');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (79, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (80, 17, 'connectorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (80, 57, 'requestedMessage');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (81, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (82, 17, 'connectorId');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (83, 13, 'status');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (84, 58, 'location');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (84, 59, 'retries');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (84, 60, 'retrieveDate');
INSERT INTO event_operation_fields_mapping (OPERATION_ID, FIELD_ID, JSON_ATT_KEY) VALUES (84, 61, 'retryInterval');


