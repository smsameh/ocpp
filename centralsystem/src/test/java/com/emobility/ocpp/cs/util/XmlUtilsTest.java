package com.emobility.ocpp.cs.util;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.emobility.ocpp.cs.patch.PatchContent;

/**
 * 
 * @author Salah Abu Msameh
 */
public class XmlUtilsTest {

	@Test
	public void testConvertToXML() {
		
		PatchContent patchContent = new PatchContent();
		
		List<String> sqls = new ArrayList<String>();
		
		sqls.add("SELECT * FROM TABLE");
		sqls.add("UPDATE TABLE SET TABLE");
		sqls.add("DELETE * FROM TABLE");
		sqls.add("ALTER TABLE ADD COLUMN NAME");
		
		patchContent.setPatchVersion("1.1");
		patchContent.setScripts(sqls);
		
		XmlUtils.convertToXML(patchContent);
	}
}
