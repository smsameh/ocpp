
$(function() {
	
	$("#start-at-date").datetimepicker();
	$("#ended-at-date").datetimepicker();
	
	$("#event-log-operations").change(function() {
		$("#btn-event-filter").attr("href", "/admin/events/" + $(this).find(":selected").val());
	});
	
	/**
	 * remote start transaction  
	 */
	$("#btn-remote-charge").click(function() {
		
		var userId = $('#control-user-id').val();
		var cpUniqueIdentifier = $('#control-cp-uid').val();
		
		if (!userId.trim()) {
			return alert('Please enter the user Id');
		}
		
		if (!cpUniqueIdentifier.trim()) {
		    return;
		}
		
		$.get('/api/remote-start-transaction/' + cpUniqueIdentifier + '/0/' + userId);
	});
	
	/**
	 * remote stop transaction  
	 */
	$("#btn-remote-stop").click(function() {
		
		var userId = $('#control-user-id').val();
		
		if (!userId.trim()) {
			return alert('Please enter the user Id');
		}		
		
		$.get('http://localhost:8080/api/remote-stop-transaction/' + userId);
	});
	
	/**
	 * Release 
	 */
	$("#btn-release").click(function() {
	
//		var userId = $('#control-user-id').val();
//		
//		if (!userId.trim()) {
//			return alert('Please enter the user Id');
//		}
//		
//		var cpUniqueIdentifier = $('#control-cp-uid').val();
//		
//		if (!cpUniqueIdentifier.trim()) {
//		    return;
//		}
		
//		$.get('/reset/' + cpUniqueIdentifier + '/Soft');
	});
	
	/**
	 * Cancel reservation  
	 */
	$("#btn-cancel-res").click(function() {
		
		var cpUniqueIdentifier = $('#control-cp-uid').val();
		
		if (!cpUniqueIdentifier.trim()) {
		    return;
		}
		
		var resId = $('#control-res-id').val();
		
		if (!resId.trim()) {
			return alert('Please enter the user Id');
		}
		
		$.get('/api/cancel-reservation/' + cpUniqueIdentifier + '/' + resId);
	});
	
	/**
	 * Clear cache
	 */
	$("#btn-clear-cache").click(function() {
		
		var cpUniqueIdentifier = $('#control-cp-uid').val();
		
		if (!cpUniqueIdentifier.trim()) {
		    return;
		}
		
		$.get('/api/clear-cache/' + cpUniqueIdentifier);
	});
	
	/**
	 * Reboot station
	 */
	$("#btn-reboot-station").click(function() {
		
		var cpUniqueIdentifier = $('#control-cp-uid').val();
		
		if (!cpUniqueIdentifier.trim()) {
		    return;
		}
		
		$.get('/reset/' + cpUniqueIdentifier + '/Soft');
	});
});

/**
 * 
 * @param button
 * @param cpId
 * @returns
 */
function deleteChargePoint(button, cpId) {
	
	if(confirm("Are you sure you want to delete charge point")) {
		
		//call ajax remove 
		$.post('/admin/cp/delete/' + cpId);
		
		var row = $(button).closest("TR");
		
		if(row != null) {
			row.remove();
		}
		
    } else{
        return false;
    }
}

/**
 * 
 * @param cpId
 * @returns
 */
function savePrice(cpId) {
	
	var form = $('#save-cp-price-form');
	
	if(form == null) {
		return;
	}
	
	form.submit(function (event) {
		
		//event.preventDefault();
		
	    $.ajax({
	    	type: 'POST',
	    	url: '/admin/cp/edit/price/' + cpId,
	    	data: form.serialize(),
	    	success: function(response) {
	    		// if the response contains any errors, replace the form
		        if ($(response).find('.has-error').length) {
		        	form.replaceWith(response);
		        } else {
		        	form.replaceWith(response);
		        }
	    	}
	    });
	});
}

/**
 * 
 * @param button
 * @param userId
 * @returns
 */
function deleteUser(button, userId) {
	
	if(confirm("Are you sure you want to delete charge point")) {
		
		//call ajax remove 
		$.post('/admin/user/delete/' + userId);
		
		var row = $(button).closest("TR");
		
		if(row != null) {
			row.remove();
		}
		
    } else{
        return false;
    }
}
