package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.emobility.ocpp.cs.api.model.SetChargingProfileApiPayload;
import com.emobility.ocpp.profile.operation.SetChargingProfileOperation;
import com.emobility.ocpp.socket.message.RequestMessage;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class SetChargingProfileController extends ApiBaseController {
	
	@Autowired
	private SetChargingProfileOperation setChargingProfile;
	
	@GetMapping(path = "/set-charging-profile/{cpUniqueIdentifier}/{connectorId}")
	public void setChargingProfile(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier,
								   @PathVariable("connectorId") String connectorId) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		if(StringUtils.isEmpty(connectorId)) {
			connectorId = "0";
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, new SetChargingProfileApiPayload(cpUniqueIdentifier, 
				Integer.parseInt(connectorId)));
		
		setChargingProfile.submitMessage(reqMsg);
	}
}
