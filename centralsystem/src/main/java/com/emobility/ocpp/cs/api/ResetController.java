package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.emobility.ocpp.cs.api.model.ResetApiPayload;
import com.emobility.ocpp.profile.operation.ResetOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.ResetType;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@RestController
public class ResetController extends ApiBaseController {

	@Autowired
	private ResetOperation reset;
	
	@GetMapping(path = "/reset/{cpUniqueIdentifier}/{resetType}")
	public void chargingConfiguration(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier,
									  @PathVariable("resetType") String resetType) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		ResetType type = null;
		
		try {
			type = ResetType.valueOf(resetType);
			
		} catch(IllegalArgumentException ex) {
			Log.error(GetCompositeScheduleController.class, "Invalid reset type value, the value should be one of [Hard, Soft]");
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, new ResetApiPayload(cpUniqueIdentifier, type));
		reset.submitMessage(reqMsg);
	}
}
