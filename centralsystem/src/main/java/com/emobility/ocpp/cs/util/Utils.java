package com.emobility.ocpp.cs.util;

/**
 * 
 * @author Salah Abu Msameh
 */
public class Utils {
	
	/**
	 * 
	 * @param strArr
	 * @return
	 */
	public static long[] convertToLongArray(String[] strArr) {
		
		if(strArr == null || strArr.length == 0) {
			return null;
		}
		
		long[] resultArr = new long[strArr.length];
		
		for(int i = 0; i < strArr.length; i++) {
			resultArr[i] = Long.parseLong(strArr[i]);
		}
		
		return resultArr;
	}
}
