package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.emobility.ocpp.cs.api.model.TriggerMessageApiPayload;
import com.emobility.ocpp.profile.operation.TriggerMessageOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.MessageTrigger;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class TriggerMessageController extends ApiBaseController {

	@Autowired
	private TriggerMessageOperation triggerMessage;
	
	@GetMapping(path = "/trigger-message/{cpUniqueIdentifier}/{connectorId}/{requestedMessage}")
	public void chargingConfiguration(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier,
									  @PathVariable("connectorId") String connectorId,
									  @PathVariable("requestedMessage") String requestedMessage) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		int connId = 0;
		
		try {
			connId = Integer.parseInt(connectorId);
			
		} catch (NumberFormatException ex) {
			Log.error(TriggerMessageController.class, "Invalid connector id value, " + ex.getMessage());
			return;
		}
		
		MessageTrigger requestedMsg = null;
		
		try {
			requestedMsg = MessageTrigger.valueOf(requestedMessage);
			
		} catch(IllegalArgumentException ex) {
			Log.error(TriggerMessageController.class, "Invalid trigger message type value, "
					+ "the value should be one of [BootNotification, DiagnosticsStatusNotification, FirmwareStatusNotification, "
					+ "Heartbeat, MeterValues, StatusNotification]");
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, new TriggerMessageApiPayload(cpUniqueIdentifier, connId, requestedMsg));
		triggerMessage.submitMessage(reqMsg);
	}
}
