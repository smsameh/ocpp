package com.emobility.ocpp.cs.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.emobility.ocpp.cs.persistence.entity.BSSEventMaster;
import com.emobility.ocpp.cs.persistence.entity.EventOperation;
import com.emobility.ocpp.cs.persistence.entity.OperationFieldMapping;

/**
 * BSS-Events repository responsible for storing all the charge points events requested to the central system
 * 
 * @author Salah Abu Msameh
 */
@Repository
public class BSSEventsDao extends DaoBase {
	
	/**
	 * 
	 * @param operationId
	 */
	public List<OperationFieldMapping> findOperationFields(long operationId) {
		
		return em.createQuery("from OperationFieldMapping WHERE operationId = :operationId", OperationFieldMapping.class)
				.setParameter("operationId", operationId)
				.getResultList();
	}
	
	/**
	 * 
	 * @return
	 */
	public List<BSSEventMaster> findAllEvents() {
		return em.createQuery("from BSSEventMaster", BSSEventMaster.class).getResultList();
	}
	
	/**
	 * 
	 * @return
	 */
	public List<BSSEventMaster> findEventsByOperationId(long operationId) {
		
		return em.createQuery("FROM BSSEventMaster WHERE operationId = :operationId", BSSEventMaster.class)
				.setParameter("operationId", operationId)
				.getResultList();
	}
	
	/**
	 * 
	 * @return
	 */
	public List<BSSEventMaster> findEventsByOperationIds(long... operationIds) {
		
		return em.createQuery("FROM BSSEventMaster WHERE operationId IN :operationIds", BSSEventMaster.class)
				.setParameter("operationIds", operationIds)
				.getResultList();
	}
	
	/**
	 * 
	 * @param eventId
	 * @return
	 */
	public Map<String, String> findDetailsByEventId(long eventId) {
		
		String sql = "SELECT EF.FIELD_DESC, ED.FIELD_VALUE FROM bss_events_details ED, event_fields EF\r\n" + 
				"WHERE EF.FIELD_ID = ED.FIELD_ID\r\n" + 
				"AND ED.EVENT_ID = :eventId";
		
		List<Object[]> rows = em.createNativeQuery(sql)
				.setParameter("eventId", eventId)
				.getResultList();
		
		Map<String, String> detailsMap = new HashMap<String, String>();
		rows.forEach(row -> detailsMap.put(String.valueOf(row[0]), String.valueOf(row[1])));
		
		return detailsMap;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<EventOperation> findOperations() {
		return em.createQuery("from EventOperation", EventOperation.class).getResultList();
	}
}
