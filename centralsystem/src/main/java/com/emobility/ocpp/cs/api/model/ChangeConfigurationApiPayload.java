package com.emobility.ocpp.cs.api.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ChangeConfigurationApiPayload extends ApiPayload {

	private String key;
	private String value;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @param key
	 * @param value
	 */
	public ChangeConfigurationApiPayload(String cpUniqueIdentifier, String key, String value) {
		super(cpUniqueIdentifier);
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}
