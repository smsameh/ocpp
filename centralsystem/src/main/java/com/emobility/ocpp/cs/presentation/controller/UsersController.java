package com.emobility.ocpp.cs.presentation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.emobility.ocpp.cs.persistence.entity.User;
import com.emobility.ocpp.cs.presentation.Pages;
import com.emobility.ocpp.cs.presentation.form.UserFB;
import com.emobility.ocpp.cs.service.UserService;
import com.emobility.ocpp.cs.service.UserStatus;

/**
 * Users controller
 * 
 * @author Salah Abu Msameh
 */
@Controller
@RequestMapping("/admin/user")
public class UsersController extends BaseController {
	
	@Autowired
	private UserService userSrv;
	
	
	/**
	 * view all users
	 * 
	 * @return
	 */
	@GetMapping(path = "/users")
	public String users(Model model) {
		model.addAttribute("users", userSrv.getAllUsers());
		return Pages.USERS;
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping(path = "/add")
	public String addUser(Model model) {
		
		model.addAttribute("userFB", new UserFB());
		addCommonAtt(model);
		
		return Pages.ADD_USER;
	}

	/**
	 * 
	 * @param userFB
	 * @param bindingResult
	 * @return
	 */
	@RequestMapping(path = "/add", method = RequestMethod.POST)
	public String addUser(@Valid UserFB userFB, BindingResult bindingResult) {
		
		if(bindingResult.hasErrors()) {
			return Pages.ADD_USER;
		}
		
		if(userSrv.createUser(userFB)) {
			return Pages.REDIRECT_USERS;
		}
		
		bindingResult.reject("global", "Unable to create user, please refer to logs for more details");
		
		return Pages.ADD_USER;
	}
	
	/**
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping(path = "/edit/{userId}")
	public String editUser(Model model, @PathVariable String userId) {
		
		User user = userSrv.getUser(userId);
		
		if(user == null) {
			return Pages.REDIRECT_USERS;
		}
		
		model.addAttribute("userFB", new UserFB(user));
		addCommonAtt(model);
		
		return Pages.EDIT_USER;
	}

	/**
	 * 
	 * @param userFB
	 * @param bindingResult
	 * @return
	 */
	@RequestMapping(path = "/edit", method = RequestMethod.POST)
	public String editUser(@Valid UserFB userFB, BindingResult bindingResult) {
		
		if(bindingResult.hasErrors()) {
			return Pages.EDIT_USER;
		}
		
		if(userSrv.updateUser(userFB)) {
			return Pages.REDIRECT_USERS;
		}
		
		bindingResult.reject("global", "Unable to edit user, please refer to logs for more details");
		
		return Pages.EDIT_USER;
	}
	
	/**
	 * Delete charge point<br>
	 * 
	 * Note: This will be fired throw ajax request
	 * 
	 * @param chargePointFB
	 * @param cpId
	 * @param bindingResult
	 * @return
	 */
	@RequestMapping(path = "/delete/{userId}", method = RequestMethod.POST)
	@ResponseBody
	public String deleteChargePoint(@PathVariable String userId) {
		
		User user = userSrv.getUser(userId);
		
		if(user != null) {
			userSrv.deleteUser(userId);
			return "User [" + userId + "] deleted successfully";
		}
		
		return "Error, failed to delete user [" + userId + "]";
	}
	
	/**
	 * 
	 * @param model
	 */
	private void addCommonAtt(Model model) {
		model.addAttribute("userStatus", UserStatus.values());
	}
}
