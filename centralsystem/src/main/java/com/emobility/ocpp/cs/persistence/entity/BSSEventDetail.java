package com.emobility.ocpp.cs.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "BSS_EVENTS_DETAILS")
public class BSSEventDetail {
	
	private long eventDetailId;
	private long eventMasterId;
	private long fieldId;
	private String fieldValue;
	
//	private BSSEventMaster eventMaster;
	
	/**
	 * 
	 */
	public BSSEventDetail() {}
	
	/**
	 * 
	 * @param fieldId
	 * @param fieldValue
	 */
	public BSSEventDetail(long fieldId, String fieldValue) {
		this.fieldId = fieldId;
		this.fieldValue = fieldValue;
	}
	
	/**
	 * 
	 * @param fieldId
	 * @param fieldValue
	 */
	public BSSEventDetail(long eventMasterId, long fieldId, String fieldValue) {
		this.eventMasterId = eventMasterId;
		this.fieldId = fieldId;
		this.fieldValue = fieldValue;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EVENT_DETAIL_ID")
	public long getEventDetailId() {
		return eventDetailId;
	}

	public void setEventDetailId(long eventDetailId) {
		this.eventDetailId = eventDetailId;
	}
	
	@Column(name = "EVENT_ID")
	public long getEventMasterId() {
		return eventMasterId;
	}
	
	public void setEventMasterId(long eventId) {
		this.eventMasterId = eventId;
	}
	
	@Column(name = "FIELD_ID")
	public long getFieldId() {
		return fieldId;
	}
	
	public void setFieldId(long fieldId) {
		this.fieldId = fieldId;
	}
	
	@Column(name = "FIELD_VALUE")
	public String getFieldValue() {
		return fieldValue;
	}
	
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	
//	@ManyToOne
//	public BSSEventMaster getEventMaster() {
//		return eventMaster;
//	}
//
//	public void setEventMaster(BSSEventMaster eventMaster) {
//		this.eventMaster = eventMaster;
//	}
}
