package com.emobility.ocpp.cs.persistence.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Charge point user entity
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "USERS")
public class User {

	private String userId;
	private String fullName;
	private int status;// (Active/Inactive/Suspended)
	private int organizationId;
	private Date subscriptionDate;
	private String paymentMethod;
	private String paymentInfo;
	
	@Id
	@Column(name = "USER_ID")
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Column(name = "FULLNAME")
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	@Column(name = "STATUS")
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	@Column(name = "ORGANIZATION_ID")
	public int getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}
	
	@Column(name = "SUBSCRIPTION_DATE")
	@Temporal(TemporalType.DATE)
	public Date getSubscriptionDate() {
		return subscriptionDate;
	}
	
	public void setSubscriptionDate(Date subscriptionDate) {
		this.subscriptionDate = subscriptionDate;
	}
	
	@Column(name = "PAYMENT_METHOD")
	public String getPaymentMethod() {
		return paymentMethod;
	}
	
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	@Column(name = "PAYMENT_INFO")
	public String getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(String paymentInfo) {
		this.paymentInfo = paymentInfo;
	}
}
