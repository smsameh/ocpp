package com.emobility.ocpp.cs.api.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class CancelReservationApiPayload extends ApiPayload {

	private int reservationId;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @param reservationId
	 */
	public CancelReservationApiPayload(String cpUniqueIdentifier, int reservationId) {
		super(cpUniqueIdentifier);
		this.reservationId = reservationId;
	}

	public int getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}
}
