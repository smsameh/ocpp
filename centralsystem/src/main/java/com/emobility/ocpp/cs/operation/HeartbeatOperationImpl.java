package com.emobility.ocpp.cs.operation;

import java.io.IOException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.HeartbeatOperation;
import com.emobility.ocpp.socket.message.ConfirmMessage;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.model.HeartbeatConfirmPayload;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class HeartbeatOperationImpl extends HeartbeatOperation {
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Override
	public void handleReceivedMessage(Message requestMessage) {
		
		//audit request
		auditorSrv.audit(AuditOperations.HEARTBEAT_REQUEST, requestMessage);
		
		ConfirmMessage confMsg = new ConfirmMessage(requestMessage.getSessionId(), requestMessage.getUniqueId(), 
				new HeartbeatConfirmPayload(new Date()));
		
		try {
			msgTransmitter.sendMessage(confMsg);
			
		} catch (NoSessionMessageFound | IOException e) {
			Log.error(HeartbeatOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		//audit confirm
		auditorSrv.audit(AuditOperations.HEARTBEAT_CONFIRM, confMsg);
	}
}
