package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.emobility.ocpp.cs.api.model.ChangeAvailabilityApiPayload;
import com.emobility.ocpp.profile.operation.ChangeAvailabilityOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.AvailabilityType;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
@RequestMapping("/api")
public class ChangeAvailabilityController extends ApiBaseController {

	@Autowired
	private ChangeAvailabilityOperation changeAvailability;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @param connectorId
	 * @param type
	 */
	@GetMapping(path = "/change-availability/{cpUniqueIdentifier}/{connectorId}/{type}")
	public void changeAvailability(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier, 
								   @PathVariable("connectorId") String connectorId,
								   @PathVariable("type") String type) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		try {
			AvailabilityType.valueOf(type);
		} catch(IllegalArgumentException ex) {
			Log.error(RemoteStartTransactionController.class, "Invalid change type, the type should be one of [Inoperative, Operative]");
			return;
		}
		
		if(StringUtils.isEmpty(connectorId)) {
			connectorId = "0";
		}
		
		RequestMessage reqMsg = new RequestMessage(null, null, new ChangeAvailabilityApiPayload(cpUniqueIdentifier, 
				Integer.parseInt(connectorId), AvailabilityType.valueOf(type)));
		
		changeAvailability.submitMessage(reqMsg);
	}
}
