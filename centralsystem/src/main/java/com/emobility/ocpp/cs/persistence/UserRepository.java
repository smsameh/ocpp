package com.emobility.ocpp.cs.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.emobility.ocpp.cs.persistence.entity.User;

/**
 * Users Repository</br>
 * Use UserDao instead
 * @author Salah Abu Msameh
 */
@Repository
@Deprecated
public interface UserRepository extends CrudRepository<User, String> {
	
}
