package com.emobility.ocpp.cs.operation;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.api.model.CancelReservationApiPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.CancelReservationOperation;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.CancelReservationRequestPayload;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class CancelReservationOperationImpl extends CancelReservationOperation {
	
	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof CancelReservationApiPayload)) {
			Log.error(RemoteStartTransactionOperationImpl.class, "Invalid payload type for [CancelReservationOperation] operation");
			return;
		}
		
		CancelReservationApiPayload payload = (CancelReservationApiPayload) message.getPayload();
		
		String cpUniqueIdentifier = payload.getCpUniqueIdentifier();
		int reservationId = payload.getReservationId();
		
		//1. validate charge point
		if(cpSrv.getChargePoint(cpUniqueIdentifier) == null) {
			Log.error(RemoteStartTransactionOperationImpl.class, "No charge point registered found for unique identifier [" + cpUniqueIdentifier + "]");
			return;
		}
		
		String sessionId = cpSrv.getLinkedSessionId(cpUniqueIdentifier);
		
		if(StringUtils.isEmpty(sessionId)) {
			Log.error(RemoteStartTransactionOperationImpl.class,  "No active session found for charge point [" + cpUniqueIdentifier + "]");
			return;
		}
				
		String messageId = Utils.generateMessageId();
		RequestMessage reqMsg = new RequestMessage(messageId, OperationActions.CANCEL_RESERVATION, 
				new CancelReservationRequestPayload(reservationId));
		
		reqMsg.setSessionId(sessionId);
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(messageId, reqMsg);
			
		} catch (NoSessionMessageFound | IOException e) {
			Log.error(CancelReservationOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.CANCEL_RESERVATION_REQUEST, reqMsg);
	}
	
	@Override
	public void handleReceivedMessage(Message message) {
		
		auditorSrv.audit(AuditOperations.CANCEL_RESERVATION_REQUEST, message);
		msgTracker.removeMessage(message.getUniqueId());
	}
}
