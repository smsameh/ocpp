package com.emobility.ocpp.cs.api.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ReserveNowApiPayload extends ApiPayload {
	
	private String userId;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @param userId
	 */
	public ReserveNowApiPayload(String cpUniqueIdentifier, String userId) {
		super(cpUniqueIdentifier);
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
