package com.emobility.ocpp.cs.operation;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.FirmwareStatusNotificationOperation;
import com.emobility.ocpp.socket.message.ConfirmMessage;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.model.FirmwareStatusNotificationConfirmPayload;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class FirmwareStatusNotificationOperationImpl extends FirmwareStatusNotificationOperation {
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Override
	public void handleReceivedMessage(Message requestMessage) {
		
		//audit request
		auditorSrv.audit(AuditOperations.FIRMWARE_STATUS_NOTIFICATION_REQUEST, requestMessage);
		
		ConfirmMessage confMsg = new ConfirmMessage(requestMessage.getSessionId(), requestMessage.getUniqueId(), 
				new FirmwareStatusNotificationConfirmPayload());
		
		try {
			msgTransmitter.sendMessage(confMsg);
		} catch (NoSessionMessageFound | IOException e) {
			Log.error(FirmwareStatusNotificationOperationImpl.class, e.getMessage());
		}
		
		//audit request
		auditorSrv.audit(AuditOperations.FIRMWARE_STATUS_NOTIFICATION_CONFIRM, confMsg);
	}
}
