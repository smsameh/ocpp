package com.emobility.ocpp.cs.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "EVENT_OPERATIONS")
public class EventOperation {

	@Id
	@Column(name = "OPERATION_ID")
	private long operationId;
	
	@Column(name = "OPERATION_NAME")
	private String operationName;
	
	public long getOperationId() {
		return operationId;
	}
	
	public void setOperationId(long operationId) {
		this.operationId = operationId;
	}
	
	public String getOperationName() {
		return operationName;
	}
	
	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}
}
