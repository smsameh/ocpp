package com.emobility.ocpp.cs.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.persistence.ChargePointDao;
import com.emobility.ocpp.cs.persistence.ChargePointRepository;
import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.persistence.entity.Price;
import com.emobility.ocpp.cs.presentation.form.ChargePointFB;
import com.emobility.ocpp.cs.presentation.form.PricingFB;
import com.emobility.ocpp.socket.message.model.ChargePointStatus;
import com.emobility.ocpp.util.Log;

/**
 * This service is responsible for all charge point registry information and actions
 * 
 * @author Salah Abu Msameh
 */
@Service
public class ChargePointService {
	
	@Autowired
	private ChargePointRepository cpRepo;
	
	@Autowired
	private ChargePointDao cpDao;
	
	private boolean loaded = false;
	
	private Map<String, ChargePoint> chargePointsByUniqueIdentifierCache = new ConcurrentHashMap<String, ChargePoint>();
	private Map<String, String> chargePointBySessionCache = new ConcurrentHashMap<String, String>();

	/**
	 * Check if the charge point registered to the system or not
	 * 
	 * @param uniqueIdentifier
	 * @return true if the charge point is already registered to the system, false otherwise
	 */
	public boolean isRegistered(String uniqueIdentifier) {
		
		if(StringUtils.isEmpty(uniqueIdentifier)) {
			return false;
		}
		
		ChargePoint cp = getChargePoint(uniqueIdentifier);
		
		if(cp != null) {
			return true;
		}
		
		if((cp = cpRepo.findByUniqueIdentifier(uniqueIdentifier)) != null) {
			this.chargePointsByUniqueIdentifierCache.put(uniqueIdentifier, cp);
			return true;
		}
		
		return false;
	}
	
	/**
	 * register new charge point based on the given charge point form bean
	 * 
	 * @param chargePointFB
	 * @return
	 */
	public boolean registerChargePoint(ChargePointFB chargePointFB) {
		
		ChargePoint cp = new ChargePoint();
		
		cp.setUniqueIdentifier(chargePointFB.getUniqueIdentifier());
		cp.setName(chargePointFB.getName());
		cp.setSerialNo(chargePointFB.getSerialNo());
		cp.setModel(chargePointFB.getModel());
		cp.setVendor(chargePointFB.getVendor());
		cp.setStatus(ChargePointStatus.Available.toString());
		cp.setIpAdress(chargePointFB.getIpAddress());
		cp.setPort(chargePointFB.getPort());
		cp.setAdress(chargePointFB.getAddress());
		cp.setCity(chargePointFB.getCity());
		cp.setContactPerson(chargePointFB.getContactPerson());
		
		ChargePoint savedCP = cpRepo.save(cp);
		
		if(savedCP != null) {
			this.chargePointsByUniqueIdentifierCache.put(chargePointFB.getUniqueIdentifier(), savedCP);
			return true;
		}
		
		return false;
	}
	
	/**
	 * update given charge point
	 * 
	 * @param chargePointFB
	 * @return
	 */
	public boolean updateChargePoint(ChargePointFB chargePointFB) {
		
		ChargePoint cp = getChargePointById(chargePointFB.getId());
		
		//1. remove old charge point
		this.chargePointsByUniqueIdentifierCache.remove(cp.getUniqueIdentifier());
		
		//2. do update
		cp.setUniqueIdentifier(chargePointFB.getUniqueIdentifier());
		cp.setName(chargePointFB.getName());
		cp.setSerialNo(chargePointFB.getSerialNo());
		cp.setModel(chargePointFB.getModel());
		cp.setVendor(chargePointFB.getVendor());
		cp.setStatus(ChargePointStatus.Available.toString());
		cp.setIpAdress(chargePointFB.getIpAddress());
		cp.setPort(chargePointFB.getPort());
		cp.setAdress(chargePointFB.getAddress());
		cp.setCity(chargePointFB.getCity());
		cp.setContactPerson(chargePointFB.getContactPerson());
		
		ChargePoint savedCP = (ChargePoint) cpDao.update(cp);
		
		if(savedCP != null) {
			this.chargePointsByUniqueIdentifierCache.put(savedCP.getUniqueIdentifier(), savedCP);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Update price values for the given charge point
	 * 
	 * @param cpId
	 * @param pricingFB
	 * @return
	 */
	public boolean updateChargePointPrice(long cpId, PricingFB pricingFB) {
		
		ChargePoint cp = getChargePointById(cpId);
		
		//1. remove old charge point
		this.chargePointsByUniqueIdentifierCache.remove(cp.getUniqueIdentifier());
		
		//2. do update
		Price p = cp.getPrice();
		
		if(p == null) {
			p = new Price();
			cp.setPrice(p);
		}
		
		p.setAcEuroKwh(pricingFB.getAcEuroKwh());
		p.setAcEuroHour(pricingFB.getAcEuroHour());
		p.setRevEuroMinute(pricingFB.getRevEuroMinute());
		p.setDcEuroKwh(pricingFB.getDcEuroKwh());
		p.setDcEuroMinute(pricingFB.getDcEuroMinute());
		
		ChargePoint savedCP = (ChargePoint) cpDao.update(cp);
		
		if(savedCP != null) {
			this.chargePointsByUniqueIdentifierCache.put(savedCP.getUniqueIdentifier(), savedCP);
			return true;
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public boolean deleteChargePoint(long id) {
		
		ChargePoint cp = getChargePointById(id);
		
		cpDao.delete(cp);
		this.chargePointsByUniqueIdentifierCache.remove(cp.getUniqueIdentifier());
		
		return true;
	}
	
	/**
	 * 
	 * @param cpId
	 * @return
	 */
	public ChargePoint getChargePointById(long cpId) {
		
		if(cpId <= 0) {
			return null;
		}
		
		ChargePoint cp = chargePointsByUniqueIdentifierCache.values().stream()
				.filter(currCP -> currCP.getId() == cpId)
				.findFirst()
				.orElse(null);
		
		if(cp != null) {
			return cp;
		}
		
		if((cp = cpRepo.findOne(cpId)) != null) {
			this.chargePointsByUniqueIdentifierCache.put(cp.getUniqueIdentifier(), cp);
		}
		
		return cp;
	}
	
	/**
	 * 
	 * @param uniqueIdentifier
	 * @return
	 */
	public ChargePoint getChargePoint(String uniqueIdentifier) {
		
		if(StringUtils.isEmpty(uniqueIdentifier)) {
			return null;
		}
		
		ChargePoint cp = this.chargePointsByUniqueIdentifierCache.get(uniqueIdentifier);
		
		if(cp != null) {
			return cp;
		}
		
		if((cp = cpRepo.findByUniqueIdentifier(uniqueIdentifier)) != null) {
			this.chargePointsByUniqueIdentifierCache.put(uniqueIdentifier, cp);
			return cp;
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param cpSerialNo
	 * @return
	 */
	public ChargePoint getChargePointBySerialNo(String cpSerialNo) {
		
		if(StringUtils.isEmpty(cpSerialNo)) {
			return null;
		}
		
		ChargePoint cp = this.chargePointsByUniqueIdentifierCache.values().stream()
				.filter(chargePoint -> chargePoint.getSerialNo().equals(cpSerialNo))
				.findAny()
				.orElse(null);
		
		if(cp != null) {
			return cp;
		}
		
		if((cp = cpRepo.findBySerialNo(cpSerialNo)) != null) {
			this.chargePointsByUniqueIdentifierCache.put(cp.getUniqueIdentifier(), cp);
			return cp;
		}
		
		return null;
	}
	
	/**
	 * 
	 * @return
	 */
	public Collection<ChargePoint> getAllChargePoints() {
		
		if(!loaded) {
			loadChargePoints();
		}
		
		return this.chargePointsByUniqueIdentifierCache.values();
	}

	/**
	 * 
	 * @param sessionId
	 * @return linked charge point if exist, null otherwise
	 */
	public ChargePoint getLinkedChargePoint(String sessionId) {
		
		String uniqueIdentifier = this.chargePointBySessionCache.get(sessionId);
		
		if(StringUtils.isEmpty(uniqueIdentifier)) {
			return null;
		}
		
		return getChargePoint(uniqueIdentifier);
	}
	
	/**
	 * Link session id to the corresponding charge point
	 * 
	 * @param id
	 * @param extractConnectionUniqueIdentifier
	 */
	public void linkSessionToChargePoint(String sessionId, String chargePointUniqueId) {
		this.chargePointBySessionCache.put(sessionId, chargePointUniqueId);
	}
	
	/**
	 * Link session id to the corresponding charge point
	 * 
	 * @param id
	 * @param extractConnectionUniqueIdentifier
	 */
	public void delinkSession(String sessionId) {
		this.chargePointBySessionCache.remove(sessionId);
	}
	
	/**
	 * 
	 * @param sessionId
	 * @return
	 */
	public String getUniqueIdentefier(String sessionId) {
		
		if(StringUtils.isEmpty(sessionId)) {
			return null;
		}
		
		return this.chargePointBySessionCache.get(sessionId);
	}
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @return linked session id for the given charge point unique identifier, null otherwise
	 */
	public String getLinkedSessionId(String cpUniqueIdentifier) {
		
		if(StringUtils.isEmpty(cpUniqueIdentifier)) {
			return null;
		}
		
		for(Entry<String, String> entry : this.chargePointBySessionCache.entrySet()) {
			if(cpUniqueIdentifier.equals(entry.getValue())) {
				return entry.getKey();
			}
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param type
	 */
	public void updateChargePointStatus(String sessionId, String status) {
		
		ChargePoint cp = getLinkedChargePoint(sessionId);
		
		if(cp == null) {
			Log.error(ChargePointService.class, "Failed to update charge point status, charge point is null");
			return;
		}
		
		//change status
		cp.setStatus(status);
		cp = (ChargePoint) cpDao.update(cp);
		
		//update cache with the new reference
		chargePointsByUniqueIdentifierCache.put(cp.getUniqueIdentifier(), cp);
	}
	
	/**
	 * 
	 */
	private void loadChargePoints() {
		
		List<ChargePoint> cps = (List<ChargePoint>) cpRepo.findAll();
		cps.forEach(cp -> this.chargePointsByUniqueIdentifierCache.put(cp.getUniqueIdentifier(), cp));
		
		loaded = true;
	}
}
