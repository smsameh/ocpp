package com.emobility.ocpp.cs.api.model;

import com.emobility.ocpp.socket.message.model.UpdateType;

/**
 * 
 * @author Salah Abu Msameh
 */
public class SendLocalListApiPayload extends ApiPayload {

	private int listVersion;
	private String userId;
	private UpdateType updateType;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @param listVersion
	 * @param userId
	 * @param updateType
	 */
	public SendLocalListApiPayload(String cpUniqueIdentifier, int listVersion, String userId, UpdateType updateType) {
		super(cpUniqueIdentifier);
		this.listVersion = listVersion;
		this.userId = userId;
		this.updateType = updateType;
	}

	public int getListVersion() {
		return listVersion;
	}
	
	public void setListVersion(int listVersion) {
		this.listVersion = listVersion;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public UpdateType getUpdateType() {
		return updateType;
	}
	
	public void setUpdateType(UpdateType updateType) {
		this.updateType = updateType;
	}
}
