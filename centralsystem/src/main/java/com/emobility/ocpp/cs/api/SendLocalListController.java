package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.emobility.ocpp.cs.api.model.SendLocalListApiPayload;
import com.emobility.ocpp.profile.operation.SendLocalListOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.UpdateType;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@RestController
public class SendLocalListController extends ApiBaseController {

	@Autowired
	private SendLocalListOperation sendLocalList;
	
	@GetMapping(path = "/reset/{cpUniqueIdentifier}/{userId}/{listVersion}/{updateType}")
	public void chargingConfiguration(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier,
									  @PathVariable("userId") String userId,
									  @PathVariable("listVersion") String listVersion,
									  @PathVariable("updateType") String updateType) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		if(StringUtils.isEmpty(userId)) {
			Log.error(SendLocalListController.class, "Invalid user id value");
			return;
		}
		
		int listVer = 0;
		
		try {
			listVer = Integer.parseInt(listVersion);
			
		} catch (NumberFormatException ex) {
			Log.error(SendLocalListController.class, "Invalid list version value");
			return;
		}
		
		UpdateType type = null;
		
		try {
			type = UpdateType.valueOf(updateType);
			
		} catch(IllegalArgumentException ex) {
			Log.error(SendLocalListController.class, "Invalid update type value, the value should be one of [Differential, Full]");
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, 
				new SendLocalListApiPayload(cpUniqueIdentifier, listVer, userId, type));
		sendLocalList.submitMessage(reqMsg);
	}
}
