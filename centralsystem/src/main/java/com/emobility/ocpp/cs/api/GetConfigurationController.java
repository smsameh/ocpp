package com.emobility.ocpp.cs.api;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.emobility.ocpp.cs.api.model.GetConfigurationApiPayload;
import com.emobility.ocpp.profile.operation.GetConfigurationOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@RestController
public class GetConfigurationController extends ApiBaseController {

	@Autowired
	private GetConfigurationOperation getConfiguration;
	
	@GetMapping(path = "/get-configuration/{cpUniqueIdentifier}/{key}")
	public void chargingConfiguration(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier,
									  @PathVariable("key") String key) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		if(StringUtils.isEmpty(key)) {
			Log.error(GetConfigurationController.class, "Keys can't be empty value");
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, new GetConfigurationApiPayload(cpUniqueIdentifier, 
				Arrays.asList(new String[] {key, "key2", "key3"})));
		getConfiguration.submitMessage(reqMsg);
	}
}
