package com.emobility.ocpp.cs.operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.api.model.GetCompositeScheduleApiPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.GetCompositeScheduleOperation;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.ChargingRateUnitType;
import com.emobility.ocpp.socket.message.model.GetCompositeScheduleRequestPayload;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class GetCompositeScheduleOperationImpl extends GetCompositeScheduleOperation {
	
	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof GetCompositeScheduleApiPayload)) {
			Log.error(ClearCacheOperationImpl.class, "Invalid payload type for [GetCompositeScheduleOperation] operation");
			return;
		}
		
		GetCompositeScheduleApiPayload payload = (GetCompositeScheduleApiPayload) message.getPayload();
		
		String cpUniqueIdentifier = payload.getCpUniqueIdentifier();
		int connectorId = payload.getConnectorId();
		int duration = payload.getDuration();
		ChargingRateUnitType chargingRateUnit = payload.getChargingRateUnit();
		
		//1. validate charge point
		ChargePoint cp = cpSrv.getChargePoint(cpUniqueIdentifier);
		
		if(cp == null) {
			Log.error(GetCompositeScheduleOperationImpl.class, "No charge point registered found for unique identifier [" + cpUniqueIdentifier + "]");
			return;
		}
		
		String sessionId = cpSrv.getLinkedSessionId(cpUniqueIdentifier);
		
		if(StringUtils.isEmpty(sessionId)) {
			Log.error(GetCompositeScheduleOperationImpl.class,  "No active session found for charge point [" + cpUniqueIdentifier + "]");
			return;
		}
		
		//1. create request message and payload
		GetCompositeScheduleRequestPayload reqPayload = new GetCompositeScheduleRequestPayload();
		
		reqPayload.setConnectorId(connectorId);
		reqPayload.setDuration(duration);
		reqPayload.setChargingRateUnit(chargingRateUnit);
		
		String messageId = Utils.generateMessageId();
		
		RequestMessage reqMsg = new RequestMessage(messageId, OperationActions.GET_COMPOSITE_SCHEDULE, reqPayload);
		reqMsg.setSessionId(sessionId);
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(messageId, reqMsg);
			
		} catch (Exception e) {
			Log.error(RemoteStartTransactionOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.GET_COMPOSITE_SCHEDULE_REQUEST, reqMsg);
	}

	@Override
	public void handleReceivedMessage(Message message) {
		
		auditorSrv.audit(AuditOperations.GET_COMPOSITE_SCHEDULE_CONFIRM, message);
		msgTracker.removeMessage(message.getUniqueId());
	}
}
