package com.emobility.ocpp.cs.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "SYSTEM_CONFIGURATIONS")
public class SystemConfig {
	
	@Id
	@Column(name = "CONFIG_KEY")
	private String configKey;
	
	@Column(name = "CONFIG_VALUE")
	private String configValue;

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}
}
