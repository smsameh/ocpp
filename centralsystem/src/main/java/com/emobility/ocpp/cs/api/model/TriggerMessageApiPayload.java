package com.emobility.ocpp.cs.api.model;

import com.emobility.ocpp.socket.message.model.MessageTrigger;

/**
 * 
 * @author Salah Abu Msameh
 */
public class TriggerMessageApiPayload extends ApiPayload {

	private int connectorId;
	private MessageTrigger requestedMessage;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @param connectorId
	 * @param requestedMessage
	 */
	public TriggerMessageApiPayload(String cpUniqueIdentifier, int connectorId, MessageTrigger requestedMessage) {
		super(cpUniqueIdentifier);
		this.connectorId = connectorId;
		this.requestedMessage = requestedMessage;
	}

	public int getConnectorId() {
		return connectorId;
	}
	
	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}
	
	public MessageTrigger getRequestedMessage() {
		return requestedMessage;
	}
	
	public void setRequestedMessage(MessageTrigger requestedMessage) {
		this.requestedMessage = requestedMessage;
	}
}
