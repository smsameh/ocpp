package com.emobility.ocpp.cs.presentation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.emobility.ocpp.cs.presentation.Pages;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class AdminController extends BaseController {
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("/")
	public String home() {
		return Pages.DEFAULT;
	}
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("/admin")
	public String adminHome() {
		return Pages.DEFAULT;
	}
}
