package com.emobility.ocpp.cs.presentation.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.presentation.Pages;
import com.emobility.ocpp.cs.presentation.form.ChargePointFB;
import com.emobility.ocpp.cs.presentation.form.PricingFB;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.socket.message.model.ChargePointStatus;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
@RequestMapping("/admin/cp")
public class ChargePointsController extends AdminController {
	
	@Autowired
	private ChargePointService chargePointSrv;

	/**
	 * charge points list
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping(path = "/chargePoints")
	public String chargePointsHome(Model model) {
		model.addAttribute("chargePoints", chargePointSrv.getAllChargePoints());
		return Pages.CHARGE_POINTS;
	}
	
	/**
	 * View all related charge point info
	 * 
	 * @param model
	 * @param cpId
	 * @return
	 */
	@GetMapping(path = "/view/{cpId}")
	public String viewChargePoint(Model model, @PathVariable String cpId) {
		
		ChargePoint cp = chargePointSrv.getChargePointById(Long.parseLong(cpId));
		
		if(cp == null) {
			return Pages.CHARGE_POINTS;
		}
		
		//1. populate charge point info
		model.addAttribute("selectedCP", new ChargePointFB(cp));
		model.addAttribute("priceFB", new PricingFB(cp.getPrice()));
		model.addAttribute("socketId", chargePointSrv.getLinkedSessionId(cp.getUniqueIdentifier()));
		
		return Pages.VIEW_CHARGE_POINT;
	}
	
	/**
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping(path = "/add")
	public String addChargePoint(Model model) {
		
		model.addAttribute("chargePointFB", new ChargePointFB());
		addChargePointStatusAtt(model);
		
		return Pages.ADD_CHARGE_POINT;
	}
	
	/**
	 * 
	 * @param chargePointFB
	 */
	@RequestMapping(path = "/add", method = RequestMethod.POST)
	public String addChargePoint(@Valid ChargePointFB chargePointFB, BindingResult bindingResult) {
		
		if(bindingResult.hasErrors() || invalidForm(bindingResult, chargePointFB)) {
			return Pages.ADD_CHARGE_POINT;
		}
		
		if(chargePointSrv.registerChargePoint(chargePointFB)) {
			return Pages.REDIRECT_CHARGE_POINTS;
		}
		
		bindingResult.reject("global", "Unable to add charge point, please refer to logs for more details");
		
		return Pages.ADD_CHARGE_POINT;
	}
	
	/**
	 * 
	 * @param model
	 * @param cpId
	 * @return
	 */
	@GetMapping(path = "/edit/{cpId}")
	public String editChargePoint(Model model, @PathVariable String cpId) {
		
		ChargePoint cp = chargePointSrv.getChargePointById(Long.parseLong(cpId));
		
		if(cp == null) {
			return Pages.REDIRECT_CHARGE_POINTS;
		}
		
		model.addAttribute("chargePointFB", new ChargePointFB(cp));
		addChargePointStatusAtt(model);
		
		return Pages.EDIT_CHARGE_POINT;
	}

	/**
	 * 
	 * @param chargePointFB
	 * @param cpId
	 * @param bindingResult
	 * @return
	 */
	@RequestMapping(path = "/edit/{cpId}", method = RequestMethod.POST)
	public String editChargePoint(@Valid ChargePointFB chargePointFB, @PathVariable String cpId, BindingResult bindingResult) {
		
		if(bindingResult.hasErrors() || invalidForm(bindingResult, chargePointFB)) {
			return Pages.EDIT_CHARGE_POINT;
		}
		
		chargePointFB.setId(Long.parseLong(cpId));
		
		if(chargePointSrv.updateChargePoint(chargePointFB)) {
			return Pages.REDIRECT_CHARGE_POINTS;
		}
		
		return Pages.EDIT_CHARGE_POINT;
	}
	
	/**
	 * Edit charge point price
	 * 
	 * @param chargePointFB
	 * @param cpId
	 * @param bindingResult
	 * @return
	 */
	@RequestMapping(path = "/edit/price/{cpId}", method = RequestMethod.POST)
	@ResponseBody
	public String editChargePointPrice(@Valid PricingFB pricingFB, @PathVariable String cpId, BindingResult bindingResult) {
		
		if(bindingResult.hasErrors()) {
			return "Error, " + bindingResult.getAllErrors().get(0).getDefaultMessage();
		}
		
		Long chargePointId = null;
		
		try {
			chargePointId = Long.parseLong(cpId);
		} catch (NumberFormatException ex) {
			return "Invalid charge point id value";
		}
		
		if(chargePointSrv.getChargePointById(chargePointId) == null) {
			return "Error, Invalid charge point";
		}
		
		if(chargePointSrv.updateChargePointPrice(chargePointId, pricingFB)) {
			return "Price saved successfully";
		}
		
		return "Error, Unable to save price, please refer to logs for more details";
	}
	
	/**
	 * Delete charge point<br>
	 * 
	 * Note: This will be fired throw ajax request
	 * 
	 * @param chargePointFB
	 * @param cpId
	 * @param bindingResult
	 * @return
	 */
	@RequestMapping(path = "/delete/{cpId}", method = RequestMethod.POST)
	@ResponseBody
	public String deleteChargePoint(@PathVariable String cpId) {
		
		ChargePoint cp = chargePointSrv.getChargePointById(Long.parseLong(cpId));
		
		if(cp != null) {
			chargePointSrv.deleteChargePoint(cp.getId());
			return "Charge Point [" + cp.getName() + "] deleted successfully";
		}
		
		return "Error, failed to delete charge point";
	}
	
	/**
	 * 
	 * @param bindingResult
	 * @param chargePointFB
	 * @return
	 */
	private boolean invalidForm(BindingResult bindingResult, ChargePointFB chargePointFB) {
		
		Pattern p = Pattern.compile("\\s");
		Matcher m = p.matcher(chargePointFB.getUniqueIdentifier().trim());
		
		if(m.find()) {
			bindingResult.reject("general", "Unqiue Identifier should not contain a white space, it should be one text");
			return true;
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param model
	 */
	private void addChargePointStatusAtt(Model model) {
		model.addAttribute("cpStatus", new String[]{ChargePointStatus.Available.toString(), ChargePointStatus.Unavailable.toString(),
				ChargePointStatus.SuspendedEV.toString()});
	}
}
