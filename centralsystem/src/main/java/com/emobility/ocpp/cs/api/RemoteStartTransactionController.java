package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.emobility.ocpp.cs.api.model.RemoteStartTransactionAPIPayload;
import com.emobility.ocpp.profile.operation.RemoteStartTransactionOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
@RequestMapping("/api")
public class RemoteStartTransactionController extends ApiBaseController {
	
	@Autowired
	private RemoteStartTransactionOperation remoteStartTransaction;
	
	/**
	 * 
	 */
	@GetMapping(path = "/remote-start-transaction/{cpUniqueIdentifier}/{connectorId}/{userId}")
	public void remoteStartTransaction(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier, 
			@PathVariable("connectorId") String connectorId, @PathVariable("userId") String userId) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		if(StringUtils.isEmpty(userId)) {
			Log.error(RemoteStartTransactionController.class, "User id cannot be null");
			return;
		}
		
		if(StringUtils.isEmpty(connectorId)) {
			connectorId = "0";
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, 
				new RemoteStartTransactionAPIPayload(userId, cpUniqueIdentifier, Integer.parseInt(connectorId)));
		
		remoteStartTransaction.submitMessage(reqMsg);
	}
}
