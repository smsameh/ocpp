package com.emobility.ocpp.cs.api.model;

import com.emobility.ocpp.socket.message.model.ResetType;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ResetApiPayload extends ApiPayload {

	private ResetType type;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @param type
	 */
	public ResetApiPayload(String cpUniqueIdentifier, ResetType type) {
		super(cpUniqueIdentifier);
		this.type = type;
	}

	public ResetType getType() {
		return type;
	}
	
	public void setType(ResetType type) {
		this.type = type;
	}
}
