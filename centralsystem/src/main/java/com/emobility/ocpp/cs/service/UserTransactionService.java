package com.emobility.ocpp.cs.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.util.Utils;

/**
 * Charge Points users transactions service, this service responsible for tracking the users transaction
 * 
 * @author Salah Abu Msameh
 */
@Service
public class UserTransactionService {

	private Map<String, UserTransactionInfo> usersTransactions = new ConcurrentHashMap<String, UserTransactionInfo>();
	
	/**
	 * Register a new transaction and return the transaction id
	 * 
	 * @param userId
	 * @param cpUniqueIdentifier
	 * @return
	 */
	public int registerUserTransaction(String userId, String cpUniqueIdentifier) {
		
		int transactionId = Utils.generateTransactionId();
		this.usersTransactions.put(userId, new UserTransactionInfo(transactionId, cpUniqueIdentifier));
		
		return transactionId;
	}
	
	/**
	 * Removes the user transaction from the registry, which means that the transaction has end
	 * 
	 * @param userId
	 */
	public void endUserTransaction(String userId) {
		
		if(!StringUtils.isEmpty(userId)) {
			this.usersTransactions.remove(userId);
		}
	}
	
	/**
	 * 
	 * @param userId
	 * @return user current transaction id if exist, null otherwise
	 */
	public UserTransactionInfo getTransactionId(String userId) {
		
		if(StringUtils.isEmpty(userId)) {
			return null;
		}
		
		return this.usersTransactions.get(userId);
	}
	
	/**
	 * 
	 * @param userId
	 * @return true if the given user has already active transaction, false otherwise
	 */
	public boolean hasActiveTransaction(String userId) {
		
		UserTransactionInfo trxInfo = getTransactionId(userId);
		
		if(trxInfo != null) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Helper class for holding user transaction information
	 * 
	 * @author Salah Abu Msameh
	 */
	public class UserTransactionInfo  {
		
		private Integer transactionId;
		private String cpUniqueIdentifier;
		
		/**
		 * 
		 * @param transactionId
		 * @param cpUniqueIdentifier
		 */
		public UserTransactionInfo(Integer transactionId, String cpUniqueIdentifier) {
			this.transactionId = transactionId;
			this.cpUniqueIdentifier = cpUniqueIdentifier;
		}

		public Integer getTransactionId() {
			return transactionId;
		}
		
		public String getCpUniqueIdentifier() {
			return cpUniqueIdentifier;
		}
	}
}
