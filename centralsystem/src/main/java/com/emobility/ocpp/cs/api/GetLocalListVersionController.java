package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.emobility.ocpp.cs.api.model.GetLocalListVersionOperationApiPayload;
import com.emobility.ocpp.profile.operation.GetLocalListVersionOperation;
import com.emobility.ocpp.socket.message.RequestMessage;

/**
 * 
 * @author Salah Abu Msameh
 */
@RestController
public class GetLocalListVersionController extends ApiBaseController {

	@Autowired
	private GetLocalListVersionOperation getLocalListVersion;
	
	@GetMapping(path = "/get-local-list-version/{cpUniqueIdentifier}")
	public void chargingConfiguration(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, new GetLocalListVersionOperationApiPayload(cpUniqueIdentifier));
		getLocalListVersion.submitMessage(reqMsg);
	}
}
