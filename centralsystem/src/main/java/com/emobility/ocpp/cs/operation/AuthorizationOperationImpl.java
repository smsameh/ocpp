package com.emobility.ocpp.cs.operation;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.cs.service.UserService;
import com.emobility.ocpp.profile.operation.AuthorizationOperation;
import com.emobility.ocpp.socket.message.ConfirmMessage;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.model.AuthorizationConfirmPayload;
import com.emobility.ocpp.socket.message.model.AuthorizationRequestPayload;
import com.emobility.ocpp.socket.message.model.AuthorizationStatus;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class AuthorizationOperationImpl extends AuthorizationOperation {
	
	@Autowired
	private UserService userSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Override
	public void handleReceivedMessage(Message reqMessage) {
		
		//audit request
		auditorSrv.audit(AuditOperations.AUTHORIE_REQUEST, reqMessage);
		
		String userTokenId = ((AuthorizationRequestPayload)reqMessage.getPayload()).getIdToken().getIdToken();
		AuthorizationStatus status = userSrv.Authorize(userTokenId);
		
		ConfirmMessage confMsg = new ConfirmMessage(reqMessage.getSessionId(), reqMessage.getUniqueId(), 
				AuthorizationConfirmPayload.create(0L, userTokenId, status));
		
		try {
			msgTransmitter.sendMessage(confMsg);
			
		} catch (NoSessionMessageFound | IOException e) {
			Log.error(AuthorizationOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		//audit confirm
		auditorSrv.audit(AuditOperations.AUTHORIE_CONFIRM, confMsg);
	}
}
