package com.emobility.ocpp.cs.persistence;

/**
 * 
 * @author Salah Abu Msameh
 */
public class AuditOperations {
	
	//charge point operation request/confirm ids
	public static final int AUTHORIE_REQUEST = 1;
	public static final int AUTHORIE_CONFIRM = 2;
	public static final int BOOT_NOTIFICATION_REQUEST = 3;
	public static final int BOOT_NOTIFICATION_CONFIRM = 4;
	public static final int DATA_TRANSFER_REQUEST = 5;
	public static final int DATA_TRANSFER_CONFIRM = 6;
	public static final int DIAGNOSTICS_STATUS_NOTIFICATION_REQUEST = 7;
	public static final int DIAGNOSTICS_STATUS_NOTIFICATION_CONFIRM = 8;
	public static final int FIRMWARE_STATUS_NOTIFICATION_REQUEST = 9;
	public static final int FIRMWARE_STATUS_NOTIFICATION_CONFIRM = 10;
	public static final int HEARTBEAT_REQUEST = 11;
	public static final int HEARTBEAT_CONFIRM = 12;
	public static final int METER_VALUE_REQUEST = 13;
	public static final int METER_VALUE_CONFIRM = 14;
	public static final int START_TRANSACTION_REQUEST = 15;
	public static final int START_TRANSACTION_CONFIRM = 16;
	public static final int STATUS_NOTIFICATION_REQUEST = 17;
	public static final int STATUS_NOTIFICATION_CONFIRM = 18;
	public static final int STOP_TRANSACTION_REQUEST = 19;
	public static final int STOP_TRANSACTION_CONFIRM = 20;
	
	//central system operation
	public static final int REMOTE_START_TRANSACTION_REQUEST = 50;
	public static final int REMOTE_START_TRANSACTION_CONFIRM = 51;
	public static final int REMOTE_STOP_TRANSACTION_REQUEST = 52;
	public static final int REMOTE_STOP_TRANSACTION_CONFIRM = 53;
	public static final int CANCEL_RESERVATION_REQUEST = 54;
	public static final int CANCEL_RESERVATION_CONFIRM = 55;
	public static final int CHANGE_AVAILABILITY_REQUEST = 56;
	public static final int CHANGE_AVAILABILITY_CONFIRM = 57;
	public static final int SET_CHARGING_PROFILE_REQUEST = 58;
	public static final int SET_CHARGING_PROFILE_CONFIRM = 59;
	public static final int CHANGE_CONFIGURATION_REQUEST = 60;
	public static final int CHANGE_CONFIGURATION_CONFIRM = 61;
	public static final int CLEAR_CACHE_REQUEST = 62;
	public static final int CLEAR_CACHE_CONFIRM = 63;
	public static final int CLEAR_CHARGING_PROFILE_REQUEST = 64;
	public static final int CLEAR_CHARGING_PROFILE_CONFIRM = 65;
	public static final int GET_COMPOSITE_SCHEDULE_REQUEST = 66;
	public static final int GET_COMPOSITE_SCHEDULE_CONFIRM = 67;
	public static final int GET_CONFIGURATION_REQUEST = 68;
	public static final int GET_CONFIGURATION_CONFIRM = 69;
	public static final int GET_DIAGNOSTICS_REQUEST = 70;
	public static final int GET_DIAGNOSTICS_CONFIRM = 71;
	public static final int GET_LOCAL_LIST_VERSION_REQUEST = 72;
	public static final int GET_LOCAL_LIST_VERSION_CONFIRM = 73;
	public static final int RESERVE_NOW_REQUEST = 74;
	public static final int RESERVE_NOW_CONFIRM = 75;
	public static final int RESET_REQUEST = 76;
	public static final int RESET_CONFIRM = 77;
	public static final int SEND_LOCAL_LIST_REQUEST = 78;
	public static final int SEND_LOCAL_LIST_CONFIRM = 79;
	public static final int TRIGGER_MESSAGE_REQUEST = 80;
	public static final int TRIGGER_MESSAGE_CONFIRM = 81;
	public static final int UNLOCK_CONNECTOR_REQUEST = 82;
	public static final int UNLOCK_CONNECTOR_CONFIRM = 83;
	public static final int UPDATE_FIRMWARE_REQUEST = 84;
	public static final int UPDATE_FIRMWARE_CONFIRM = 85;
}
