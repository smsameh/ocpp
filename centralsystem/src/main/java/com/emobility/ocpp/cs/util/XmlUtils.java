package com.emobility.ocpp.cs.util;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class XmlUtils {
	
	/**
	 * 
	 * @param type
	 * @param xml
	 * @return
	 */
	public static <T> T convertToObject(Class<T> type, String xml) {
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(type);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			
			return (T) jaxbUnmarshaller.unmarshal(new StringReader(xml));
			
		} catch (JAXBException ex) {
			Log.error(XmlUtils.class, "Unable to parse give xml to an object of type >> " + type + "\nXML Content: " + xml);
			ex.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * 
	 * @param obj
	 * @return
	 * @throws JAXBException 
	 */
	public static void convertToXML(Object obj) {
		
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(obj, System.out);
			
		} catch (JAXBException ex) {
			ex.printStackTrace();
		}
	}
}
