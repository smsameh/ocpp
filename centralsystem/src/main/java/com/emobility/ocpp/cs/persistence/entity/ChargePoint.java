package com.emobility.ocpp.cs.persistence.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "CHARGE_POINTS")
public class ChargePoint {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CP_ID")
	private long id;
	
	@Column(name = "UNIQUE_IDENTIFIER", unique = true)
	private String uniqueIdentifier;
	
	@Column(name = "CP_NAME")
	private String name;
	
	@Column(name = "CP_SERIAL_NO")
	private String serialNo;
	
	@Column(name = "CP_MODEL")
	private String model;
	
	@Column(name = "CP_VENDOR")
	private String vendor;
	
	@Column(name = "CP_STATUS")
	private String status;
	
	@Column(name = "IP_ADDRESS")
	private String ipAdress;
	
	@Column(name = "PORT")
	private String port;
	
	@Column(name = "ADDRESS")
	private String adress;
	
	@Column(name = "CITY")
	private String city;
	
	@Column(name = "CONTACT_PERSON")
	private String contactPerson;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PRICE_ID")
	private Price price;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getUniqueIdentifier() {
		return uniqueIdentifier;
	}
	
	public void setUniqueIdentifier(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSerialNo() {
		return serialNo;
	}
	
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public String getVendor() {
		return vendor;
	}
	
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Price getPrice() {
		return price;
	}
	
	public void setPrice(Price price) {
		this.price = price;
	}
	
	public String getIpAdress() {
		return ipAdress;
	}
	
	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}
	
	public String getPort() {
		return port;
	}
	
	public void setPort(String port) {
		this.port = port;
	}
	
	public String getAdress() {
		return adress;
	}
	
	public void setAdress(String adress) {
		this.adress = adress;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getContactPerson() {
		return contactPerson;
	}
	
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
}
