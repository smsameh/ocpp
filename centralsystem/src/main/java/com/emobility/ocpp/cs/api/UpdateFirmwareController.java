package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.emobility.ocpp.cs.api.model.UpdateFirmwareApiPayload;
import com.emobility.ocpp.profile.operation.UpdateFirmwareOperation;
import com.emobility.ocpp.socket.message.RequestMessage;

/**
 * 
 * @author Salah Abu Msameh
 */
@RestController
public class UpdateFirmwareController extends ApiBaseController {

	@Autowired
	private UpdateFirmwareOperation updateFirmware;
	
	@GetMapping(path = "/update-firmware/{cpUniqueIdentifier}")
	public void chargingConfiguration(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, new UpdateFirmwareApiPayload(cpUniqueIdentifier));
		updateFirmware.submitMessage(reqMsg);
	}
}
