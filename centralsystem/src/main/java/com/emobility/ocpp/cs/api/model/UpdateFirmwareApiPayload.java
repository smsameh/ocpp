package com.emobility.ocpp.cs.api.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class UpdateFirmwareApiPayload extends ApiPayload {
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 */
	public UpdateFirmwareApiPayload(String cpUniqueIdentifier) {
		super(cpUniqueIdentifier);
	}
}
