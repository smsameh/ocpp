package com.emobility.ocpp.cs.operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.api.model.GetLocalListVersionOperationApiPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.GetLocalListVersionOperation;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.GetLocalListVersionRequestPayload;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class GetLocalListVersionOperationImpl extends GetLocalListVersionOperation {

	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof GetLocalListVersionOperationApiPayload)) {
			Log.error(GetLocalListVersionOperationImpl.class, "Invalid payload type for [GetLocalListVersionOperation] operation");
			return;
		}
		
		GetLocalListVersionOperationApiPayload payload = (GetLocalListVersionOperationApiPayload) message.getPayload();
		String cpUniqueIdentifier = payload.getCpUniqueIdentifier();
		
		//1. validate charge point
		ChargePoint cp = cpSrv.getChargePoint(cpUniqueIdentifier);
		
		if(cp == null) {
			Log.error(GetLocalListVersionOperationImpl.class, "No charge point registered found for unique identifier [" + cpUniqueIdentifier + "]");
			return;
		}
		
		String sessionId = cpSrv.getLinkedSessionId(cpUniqueIdentifier);
		
		if(StringUtils.isEmpty(sessionId)) {
			Log.error(GetLocalListVersionOperationImpl.class,  "No active session found for charge point [" + cpUniqueIdentifier + "]");
			return;
		}
		
		//1. create request message and payload
		GetLocalListVersionRequestPayload reqPayload = new GetLocalListVersionRequestPayload();
		
		String messageId = Utils.generateMessageId();
		
		RequestMessage reqMsg = new RequestMessage(messageId, OperationActions.GET_LOCAL_LIST_VERSION, reqPayload);
		reqMsg.setSessionId(sessionId);
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(messageId, reqMsg);
			
		} catch (Exception e) {
			Log.error(GetLocalListVersionOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.GET_LOCAL_LIST_VERSION_REQUEST, reqMsg);
	}

	@Override
	public void handleReceivedMessage(Message message) {
		
		auditorSrv.audit(AuditOperations.GET_LOCAL_LIST_VERSION_CONFIRM, message);
		msgTracker.removeMessage(message.getUniqueId());
	}
}
