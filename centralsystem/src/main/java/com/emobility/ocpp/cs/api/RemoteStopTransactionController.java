package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.emobility.ocpp.cs.api.model.RemoteStopTransactionAPIPayload;
import com.emobility.ocpp.profile.operation.RemoteStopTransactionOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class RemoteStopTransactionController extends ApiBaseController {
	
	@Autowired
	private RemoteStopTransactionOperation remoteStopTransaction;
	
	/**
	 * 
	 * @param userId
	 */
	@GetMapping(path = "/remote-stop-transaction/{cpUniqueIdentifier}/{userId}")
	public void remoteStopTransaction(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier,
									  @PathVariable("userId") String userId) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		if(StringUtils.isEmpty(userId)) {
			Log.error(RemoteStartTransactionController.class, "User id cannot be null");
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, new RemoteStopTransactionAPIPayload(cpUniqueIdentifier, userId));
		remoteStopTransaction.submitMessage(reqMsg);
	}
}
