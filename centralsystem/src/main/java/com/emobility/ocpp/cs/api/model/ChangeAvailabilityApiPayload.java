package com.emobility.ocpp.cs.api.model;

import com.emobility.ocpp.socket.message.model.AvailabilityType;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ChangeAvailabilityApiPayload extends ApiPayload {
	
	private int connectorId;
	private AvailabilityType type;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @param connectorId
	 * @param type
	 */
	public ChangeAvailabilityApiPayload(String cpUniqueIdentifier, int connectorId, AvailabilityType type) {
		super(cpUniqueIdentifier);
		this.connectorId = connectorId;
		this.type = type;
	}

	public int getConnectorId() {
		return connectorId;
	}
	
	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}
	
	public AvailabilityType getType() {
		return type;
	}
	
	public void setType(AvailabilityType type) {
		this.type = type;
	}
}
