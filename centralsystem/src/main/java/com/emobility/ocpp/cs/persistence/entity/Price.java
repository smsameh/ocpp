package com.emobility.ocpp.cs.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "CP_PRICES")
public class Price {
	
	@Id
	@Column(name = "PRICE_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long priceId;
	
	@Column(name = "AC_ERU_KWH")
	private double acEuroKwh;
	
	@Column(name = "AC_ERU_HOUR")
	private double acEuroHour;
	
	@Column(name = "REVERSE_ERU_MIN")
	private double revEuroMinute;
	
	@Column(name = "DC_ERU_KWH")
	private double dcEuroKwh;
	
	@Column(name = "DC_ERU_MIN")
	private double dcEuroMinute;

	public long getPriceId() {
		return priceId;
	}

	public void setPriceId(long priceId) {
		this.priceId = priceId;
	}

	public double getAcEuroKwh() {
		return acEuroKwh;
	}

	public void setAcEuroKwh(double acEuroKwh) {
		this.acEuroKwh = acEuroKwh;
	}

	public double getAcEuroHour() {
		return acEuroHour;
	}

	public void setAcEuroHour(double acEuroHour) {
		this.acEuroHour = acEuroHour;
	}

	public double getRevEuroMinute() {
		return revEuroMinute;
	}

	public void setRevEuroMinute(double revEuroMinute) {
		this.revEuroMinute = revEuroMinute;
	}

	public double getDcEuroKwh() {
		return dcEuroKwh;
	}

	public void setDcEuroKwh(double dcEuroKwh) {
		this.dcEuroKwh = dcEuroKwh;
	}

	public double getDcEuroMinute() {
		return dcEuroMinute;
	}

	public void setDcEuroMinute(double dcEuroMinute) {
		this.dcEuroMinute = dcEuroMinute;
	}
}
