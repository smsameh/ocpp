package com.emobility.ocpp.cs.persistence.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "BSS_EVENTS_MASTER")
public class BSSEventMaster {
	
	private long eventId;
	private String cpUniqueIdentefier;
	private int msgTypeId;
	private long operationId;
	private Date eventDate;
	
	private List<BSSEventDetail> eventDetails = new ArrayList<BSSEventDetail>();
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EVENT_ID")
	public long getEventId() {
		return eventId;
	}
	
	public void setEventId(long eventId) {
		this.eventId = eventId;
	}
	
	@Column(name = "CP_UNIQUE_IDENTIFIER")
	public String getCpUniqueIdentefier() {
		return cpUniqueIdentefier;
	}

	public void setCpUniqueIdentefier(String cpUniqueIdentefier) {
		this.cpUniqueIdentefier = cpUniqueIdentefier;
	}
	
	@Column(name = "MSG_TYPE_ID")
	public int getMsgTypeId() {
		return msgTypeId;
	}
	
	public void setMsgTypeId(int msgTypeId) {
		this.msgTypeId = msgTypeId;
	}
	
	@Column(name = "OPERATION_ID")
	public long getOperationId() {
		return operationId;
	}
	
	public void setOperationId(long operationId) {
		this.operationId = operationId;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "EVENT_DATE")
	public Date getEventDate() {
		return eventDate;
	}
	
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	public List<BSSEventDetail> getEventDetails() {
		return eventDetails;
	}

	public void setEventDetails(List<BSSEventDetail> eventDetails) {
		this.eventDetails = eventDetails;
	}
}
