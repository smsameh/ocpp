package com.emobility.ocpp.cs.operation;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.cs.api.model.RemoteStopTransactionAPIPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.cs.service.UserService;
import com.emobility.ocpp.cs.service.UserTransactionService;
import com.emobility.ocpp.cs.service.UserTransactionService.UserTransactionInfo;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.profile.operation.RemoteStopTransactionOperation;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.AuthorizationStatus;
import com.emobility.ocpp.socket.message.model.RemoteStopTransactionRequestPayload;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class RemoteStopTransactionOperationImpl extends RemoteStopTransactionOperation {
	
	@Autowired
	private UserService userSrv;
	
	@Autowired
	private UserTransactionService userTrxSrv;
	
	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof RemoteStopTransactionAPIPayload)) {
			Log.error(RemoteStopTransactionOperationImpl.class, "Invalid payload type for [RemoteStopTransaction] operation");
			return;
		}
		
		String userId = ((RemoteStopTransactionAPIPayload) message.getPayload()).getUserId();
		
		if(!userTrxSrv.hasActiveTransaction(userId)) {
			Log.error(RemoteStopTransactionOperationImpl.class, "No active transaction found for user [" + userId + "]");
			return;
		}
		
		AuthorizationStatus authStatus = userSrv.Authorize(userId, true);
		
		if(authStatus != AuthorizationStatus.Accepted) {
			Log.error(RemoteStopTransactionOperationImpl.class, "Connot submit request for user status [" + authStatus + "]");
			return;
		}
		
		UserTransactionInfo trxInfo = userTrxSrv.getTransactionId(userId);
		String messageId = Utils.generateMessageId();
		
		RequestMessage reqMsg = new RequestMessage(messageId, OperationActions.REMOTE_STOP_TRANSACTION, 
				new RemoteStopTransactionRequestPayload(trxInfo.getTransactionId()));
		
		reqMsg.setSessionId(cpSrv.getLinkedSessionId(trxInfo.getCpUniqueIdentifier()));
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(messageId, reqMsg);
			
		} catch (NoSessionMessageFound | IOException e) {
			Log.error(RemoteStartTransactionOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.REMOTE_STOP_TRANSACTION_REQUEST, reqMsg);
	}

	@Override
	public void handleReceivedMessage(Message message) {
		//audit message
		auditorSrv.audit(AuditOperations.REMOTE_STOP_TRANSACTION_CONFIRM, message);
		
		//remove message from tracker
		msgTracker.removeMessage(message.getUniqueId());
	}
}
