package com.emobility.ocpp.cs.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * This class contains all the different system configurations
 * 
 * @author Salah Abu Msameh
 */
@Configuration
public class SystemConfigs {
	
	@Value("${server.mode}")
	private String serverMode;
	
	@Value("${heartbeat.interval}")
	private int hearBeatInterva;
	
	@Value("${event.audit.enabled}")
	private boolean eventAuditEnabled;
	
	@Value("${system.patch.version}")
	private String patchVersion;
	
	@Value("${system.version}")
	private String systemVersion;
	
	/**
	 * 
	 * @return
	 */
	public int getHeartBeatRequestInterval() {
		return this.hearBeatInterva;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isEventAuditEnabled() {
		return this.eventAuditEnabled;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPatchVersion() {
		return this.patchVersion;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getSystemVersion() {
		
//		if(StringUtils.isEmpty(this.systemVersion)) {
//			SystemConfig config = systemConfRepo.findOne(SystemConfigsRepository.KEY_SYSTEM_VERSION);
//			if(config != null) {
//				this.systemVersion = config.getConfigKey();
//			}
//		}
		
		return this.systemVersion;
	}
}
