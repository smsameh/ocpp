package com.emobility.ocpp.cs.operation;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.DiagnosticsStatusNotificationOperation;
import com.emobility.ocpp.socket.message.ConfirmMessage;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.model.DiagnosticsStatusNotificationConfirmPayload;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class DiagnosticsStatusNotificationOperationImpl extends DiagnosticsStatusNotificationOperation {
	
	@Autowired
	private EventsAuditorService auditorSrv;

	@Override
	public void handleReceivedMessage(Message requestMessage) {
		
		//audit event request
		auditorSrv.audit(AuditOperations.DIAGNOSTICS_STATUS_NOTIFICATION_REQUEST, requestMessage);
		
		ConfirmMessage confMsg = new ConfirmMessage(requestMessage.getSessionId(), requestMessage.getUniqueId(), 
				new DiagnosticsStatusNotificationConfirmPayload());
		
		try {
			msgTransmitter.sendMessage(confMsg);
		} catch (NoSessionMessageFound | IOException e) {
			Log.error(DiagnosticsStatusNotificationOperationImpl.class, e.getMessage());
		}
		
		//audit event confirm
		auditorSrv.audit(AuditOperations.DIAGNOSTICS_STATUS_NOTIFICATION_CONFIRM, confMsg);
	}
}
