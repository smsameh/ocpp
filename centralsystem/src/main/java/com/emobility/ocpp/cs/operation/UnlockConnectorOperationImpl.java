package com.emobility.ocpp.cs.operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.api.model.UnlockConnectorApiPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.profile.operation.UnlockConnectorOperation;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.UnlockConnectorRequestPayload;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class UnlockConnectorOperationImpl extends UnlockConnectorOperation {

	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof UnlockConnectorApiPayload)) {
			Log.error(UnlockConnectorOperationImpl.class, "Invalid payload type for [UnlockConnectorOperation] operation");
			return;
		}
		
		UnlockConnectorApiPayload payload = (UnlockConnectorApiPayload) message.getPayload();
		
		String cpUniqueIdentifier = payload.getCpUniqueIdentifier();
		int connectorId = payload.getConnectorId();
		
		//1. validate charge point
		ChargePoint cp = cpSrv.getChargePoint(cpUniqueIdentifier);
		
		if(cp == null) {
			Log.error(UnlockConnectorOperationImpl.class, "No charge point registered found for unique identifier [" + cpUniqueIdentifier + "]");
			return;
		}
		
		String sessionId = cpSrv.getLinkedSessionId(cpUniqueIdentifier);
		
		if(StringUtils.isEmpty(sessionId)) {
			Log.error(UnlockConnectorOperationImpl.class,  "No active session found for charge point [" + cpUniqueIdentifier + "]");
			return;
		}
		
		//1. create request message and payload
		UnlockConnectorRequestPayload reqPayload = new UnlockConnectorRequestPayload();
		reqPayload.setConnectorId(connectorId);
		
		String messageId = Utils.generateMessageId();
		
		RequestMessage reqMsg = new RequestMessage(messageId, OperationActions.UNLOCK_CONNECTOR, reqPayload);
		reqMsg.setSessionId(sessionId);
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(messageId, reqMsg);
			
		} catch (Exception e) {
			Log.error(UnlockConnectorOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.UNLOCK_CONNECTOR_REQUEST, reqMsg);
	}

	@Override
	public void handleReceivedMessage(Message message) {
		
		auditorSrv.audit(AuditOperations.UNLOCK_CONNECTOR_CONFIRM, message);
		msgTracker.removeMessage(message.getUniqueId());
	}
}
