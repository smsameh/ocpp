package com.emobility.ocpp.cs.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.socket.message.handler.ServerMessageHandler;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class CentralSystemMessageHandler extends ServerMessageHandler {
	
	@Autowired
	private ChargePointService cpSrv;
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		
		//call the parent, this is mandatory
		//TODO, we have to create another solution to link the session with the charge point info
		super.afterConnectionEstablished(session);
		
		cpSrv.linkSessionToChargePoint(session.getId(), 
				Utils.extractConnectionUniqueIdentifier(session.getUri().getPath()));
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {

		//call the parent, this is mandatory
		//TODO, we have to create another solution to link the session with the charge point info
		super.afterConnectionClosed(session, status);
		
		cpSrv.delinkSession(session.getId());
	}
}
