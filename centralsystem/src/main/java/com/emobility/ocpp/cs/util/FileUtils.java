package com.emobility.ocpp.cs.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class FileUtils {
	
	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public static String loadResourceFileContent(String fileName) {
		
		Resource resource = new ClassPathResource(fileName);
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		
		try {
			br = new BufferedReader(new InputStreamReader(resource.getInputStream()));
			String line;
			
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (IOException e) {
			Log.error(FileUtils.class, e.getMessage());
			e.printStackTrace();
			
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {}
			}
		}
		
		return sb.toString();
	}
}
