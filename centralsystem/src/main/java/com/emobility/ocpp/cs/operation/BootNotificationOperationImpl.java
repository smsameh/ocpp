package com.emobility.ocpp.cs.operation;

import java.io.IOException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.cs.config.SystemConfigs;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.BootNotificationOperation;
import com.emobility.ocpp.socket.message.ConfirmMessage;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.model.BootNotificationConfirmPayload;
import com.emobility.ocpp.socket.message.model.BootNotificationRequestPayload;
import com.emobility.ocpp.socket.message.model.RegistrationStatus;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class BootNotificationOperationImpl extends BootNotificationOperation {
	
	@Autowired
	private SystemConfigs systemConfig;
	
	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Override
	public void handleReceivedMessage(Message requestMessage) {
		
		String cpSerialNo = ((BootNotificationRequestPayload)requestMessage.getPayload()).getChargePointSerialNumber();
		
		//audit request
		auditorSrv.audit(AuditOperations.BOOT_NOTIFICATION_REQUEST, requestMessage);
		
		ChargePoint cp = cpSrv.getChargePointBySerialNo(cpSerialNo);
		BootNotificationConfirmPayload confirmPayload = new BootNotificationConfirmPayload();
		ConfirmMessage confMessage = new ConfirmMessage(requestMessage.getSessionId(), requestMessage.getUniqueId(), confirmPayload);
		
		if(cp != null) {
			
			confirmPayload.setCurrentTime(new Date());
			confirmPayload.setInterval(systemConfig.getHeartBeatRequestInterval());
			confirmPayload.setStatus(RegistrationStatus.Accepted);
			
		} else {
			confirmPayload.setStatus(RegistrationStatus.Rejected);
		}
		
		try {
			msgTransmitter.sendMessage(confMessage);
			
		} catch (NoSessionMessageFound | IOException e) {
			Log.error(BootNotificationOperation.class, e.getMessage());
		}
		
		auditorSrv.audit(AuditOperations.BOOT_NOTIFICATION_CONFIRM, confMessage);
	}
}
