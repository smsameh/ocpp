package com.emobility.ocpp.cs.api.model;

import com.emobility.ocpp.socket.message.model.ChargingRateUnitType;

/**
 * 
 * @author Salah Abu Msameh
 */
public class GetCompositeScheduleApiPayload extends ApiPayload {

	private int connectorId;
	private int duration;
	private ChargingRateUnitType chargingRateUnit;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @param connectorId
	 * @param duration
	 * @param chargingRateUnit
	 */
	public GetCompositeScheduleApiPayload(String cpUniqueIdentifier, int connectorId, int duration,
			ChargingRateUnitType chargingRateUnit) {
		super(cpUniqueIdentifier);
		this.connectorId = connectorId;
		this.duration = duration;
		this.chargingRateUnit = chargingRateUnit;
	}

	public int getConnectorId() {
		return connectorId;
	}
	
	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}
	
	public int getDuration() {
		return duration;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public ChargingRateUnitType getChargingRateUnit() {
		return chargingRateUnit;
	}
	
	public void setChargingRateUnit(ChargingRateUnitType chargingRateUnit) {
		this.chargingRateUnit = chargingRateUnit;
	}
}
