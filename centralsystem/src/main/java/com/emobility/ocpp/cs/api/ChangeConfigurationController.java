package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.emobility.ocpp.cs.api.model.ChangeConfigurationApiPayload;
import com.emobility.ocpp.profile.operation.ChangeConfigurationOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class ChangeConfigurationController extends ApiBaseController {

	@Autowired
	private ChangeConfigurationOperation chargingConfiguration;
	
	@GetMapping(path = "/change-configuration/{cpUniqueIdentifier}/{key}/{value}")
	public void chargingConfiguration(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier,
								   	  @PathVariable("key") String key,
								   	  @PathVariable("value") String value) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		if(StringUtils.isEmpty(key)) {
			Log.error(ChangeConfigurationController.class, "key connot be null");
			return;
		}
		
		if(StringUtils.isEmpty(value)) {
			Log.error(ChangeConfigurationController.class, "value connot be null");
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, new ChangeConfigurationApiPayload(
				cpUniqueIdentifier, key, value));
		
		chargingConfiguration.submitMessage(reqMsg);
	}
}
