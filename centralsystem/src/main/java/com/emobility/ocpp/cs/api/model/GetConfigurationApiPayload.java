package com.emobility.ocpp.cs.api.model;

import java.util.List;

/**
 * 
 * @author Salah Abu Msameh
 */
public class GetConfigurationApiPayload extends ApiPayload {

	private List<String> keys;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @param keys
	 */
	public GetConfigurationApiPayload(String cpUniqueIdentifier, List<String> keys) {
		super(cpUniqueIdentifier);
		this.keys = keys;
	}

	public List<String> getKeys() {
		return keys;
	}

	public void setKeys(List<String> keys) {
		this.keys = keys;
	}
}
