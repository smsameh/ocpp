package com.emobility.ocpp.cs.operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.api.model.ChangeConfigurationApiPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.ChangeConfigurationOperation;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.ChangeConfigurationRequestPayload;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ChangeConfigurationOperationImpl extends ChangeConfigurationOperation {
	
	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof ChangeConfigurationApiPayload)) {
			Log.error(ChangeConfigurationOperationImpl.class, "Invalid payload type for [ChangeConfigurationOperation] operation");
			return;
		}
		
		ChangeConfigurationApiPayload payload = (ChangeConfigurationApiPayload) message.getPayload();
		
		String cpUniqueIdentifier = payload.getCpUniqueIdentifier();
		String key = payload.getKey();
		String value = payload.getValue();
		
		//1. validate charge point
		ChargePoint cp = cpSrv.getChargePoint(cpUniqueIdentifier);
		
		if(cp == null) {
			Log.error(RemoteStartTransactionOperationImpl.class, 
					"No charge point registered found for unique identifier [" + cpUniqueIdentifier + "]");
			return;
		}
		
		String sessionId = cpSrv.getLinkedSessionId(cpUniqueIdentifier);
		
		if(StringUtils.isEmpty(sessionId)) {
			Log.error(RemoteStartTransactionOperationImpl.class,  "No active session found for charge point [" + cpUniqueIdentifier + "]");
			return;
		}
		
		//1. create request message and payload
		ChangeConfigurationRequestPayload reqPayload = new ChangeConfigurationRequestPayload();
		
		reqPayload.setKey(key);
		reqPayload.setValue(value);
		
		String messageId = Utils.generateMessageId();
		
		RequestMessage reqMsg = new RequestMessage(messageId, OperationActions.CHANGE_CONFIGURATION_PROFILE, reqPayload);
		reqMsg.setSessionId(sessionId);
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(messageId, reqMsg);
			
		} catch (Exception e) {
			Log.error(RemoteStartTransactionOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.CHANGE_CONFIGURATION_REQUEST, reqMsg);
	}

	@Override
	public void handleReceivedMessage(Message message) {
		auditorSrv.audit(AuditOperations.CHANGE_CONFIGURATION_CONFIRM, message);
		msgTracker.removeMessage(message.getUniqueId());
	}
}
