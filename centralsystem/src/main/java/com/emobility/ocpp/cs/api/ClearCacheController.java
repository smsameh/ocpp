package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.emobility.ocpp.cs.api.model.ClearCacheApiPayload;
import com.emobility.ocpp.profile.operation.ClearCacheOperation;
import com.emobility.ocpp.socket.message.RequestMessage;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class ClearCacheController extends ApiBaseController {

	@Autowired
	private ClearCacheOperation clearCache;
	
	@GetMapping(path = "/clear-cache/{cpUniqueIdentifier}")
	public void chargingConfiguration(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, new ClearCacheApiPayload(cpUniqueIdentifier));
		
		clearCache.submitMessage(reqMsg);
	}
}
