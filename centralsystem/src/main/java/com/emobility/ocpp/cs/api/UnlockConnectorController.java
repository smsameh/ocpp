package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.emobility.ocpp.cs.api.model.UnlockConnectorApiPayload;
import com.emobility.ocpp.profile.operation.UnlockConnectorOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@RestController
public class UnlockConnectorController extends ApiBaseController {
	
	@Autowired
	private UnlockConnectorOperation unlockConnector;
	
	@GetMapping(path = "/unlock-connector/{cpUniqueIdentifier}/{connectorId}")
	public void chargingConfiguration(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier,
									  @PathVariable("connectorId") String connectorId) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		int connId = 0;
		
		try {
			connId = Integer.parseInt(connectorId);
			
		} catch (NumberFormatException ex) {
			Log.error(TriggerMessageController.class, "Invalid connector id value, " + ex.getMessage());
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, new UnlockConnectorApiPayload(cpUniqueIdentifier, connId));
		unlockConnector.submitMessage(reqMsg);
	}
}
