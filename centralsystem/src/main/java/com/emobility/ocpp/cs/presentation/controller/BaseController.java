package com.emobility.ocpp.cs.presentation.controller;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

/**
 * 
 * @author Salah Abu Msameh
 */
public class BaseController {
	
	public static final String GENERAL_ERROR = "general";

	/**
	 * Add general form error 
	 * 
	 * @param bindingResult
	 * @param errorMsg
	 */
	public void addFormError(BindingResult bindingResult, String errorMsg) {
		bindingResult.addError(new ObjectError(GENERAL_ERROR, errorMsg));
	}
}
