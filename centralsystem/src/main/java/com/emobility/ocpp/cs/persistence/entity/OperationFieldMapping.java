package com.emobility.ocpp.cs.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Salah Abu Msameh
 */
@Entity
@Table(name = "EVENT_OPERATION_FIELDS_MAPPING")
public class OperationFieldMapping implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "OPERATION_ID")
	private long operationId;
	
	@Id
	@Column(name = "FIELD_ID")
	private long fieldId;
	
	@Column(name = "JSON_ATT_KEY")
	private String jsonAttributeKey;

	public long getOperationId() {
		return operationId;
	}

	public void setOperationId(long operationId) {
		this.operationId = operationId;
	}

	public long getFieldId() {
		return fieldId;
	}

	public void setFieldId(long fieldId) {
		this.fieldId = fieldId;
	}

	public String getJsonAttributeKey() {
		return jsonAttributeKey;
	}

	public void setJsonAttributeKey(String jsonAttributeKey) {
		this.jsonAttributeKey = jsonAttributeKey;
	}
	
	@Override
	public int hashCode() {
		return new Long(this.operationId).hashCode() + new Long(this.fieldId).hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof OperationFieldMapping)) {
			return false;
		}
		
		OperationFieldMapping mapping = (OperationFieldMapping) obj;
		
		if(this.operationId == mapping.getOperationId() && 
				this.fieldId == mapping.getFieldId() &&
				this.jsonAttributeKey.equals(mapping.getJsonAttributeKey())) {
			return true;
		}
		
		return false;
	}
}
