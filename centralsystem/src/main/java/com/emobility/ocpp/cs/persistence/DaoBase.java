package com.emobility.ocpp.cs.persistence;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 * Defines common behaviour
 * 
 * @author Salah Abu Msameh
 */
public abstract class DaoBase {
	
	@PersistenceContext
	protected EntityManager em;
	
	/**
	 * 
	 * @param eventId
	 * @return
	 */
	public <T> T findById(Class<T> entityClass, long eventId) {
		return em.find(entityClass, eventId);
	}
	
	/**
	 * save the given entity into db
	 * 
	 * @param entity
	 */
	@Transactional
	public void save(Object entity) {
		em.persist(entity);
	}
	
	/**
	 * delete the given entity into db
	 * 
	 * @param entity
	 */
	@Transactional
	public void delete(Object entity) {
		em.remove(em.contains(entity) ? entity : em.merge(entity));
	}
	
	/**
	 * update the given entity into db
	 * 
	 * @param entity
	 */
	@Transactional
	public Object update(Object entity) {
		return em.merge(entity);
	}
	
	/**
	 * execute native query
	 * 
	 * @param sql
	 */
	public void execute(String sql) {
		em.createNativeQuery(sql).executeUpdate();
	}
}
