package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.emobility.ocpp.cs.api.model.CancelReservationApiPayload;
import com.emobility.ocpp.profile.operation.CancelReservationOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
@RequestMapping("/api")
public class CancelReservationController extends ApiBaseController {

	@Autowired
	private CancelReservationOperation cancelReservation;
	
	/**
	 * 
	 */
	@GetMapping(path = "/cancel-reservation/{cpUniqueIdentifier}/{reservationId}")
	public void cancelReservation(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier, 
								  @PathVariable("reservationId") String reservationIdVal) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		if(StringUtils.isEmpty(reservationIdVal)) {
			Log.error(CancelReservationController.class, "Cancel reservation failed, reservation id cannot be null");
			return;
		}
		
		int reservationId = 0;
		
		try {
			reservationId = Integer.parseInt(reservationIdVal);
		} catch (NumberFormatException ex) {
			Log.error(CancelReservationController.class, "Invalid reservation id value, reservation Id should be positive number");
			return;
		}
		
		if(reservationId <= 0) {
			Log.error(CancelReservationController.class, "Invalid reservation id value, reservation Id should be positive number");
			return;
		}
		
		//generate request message
		cancelReservation.submitMessage(new RequestMessage(null, null, new CancelReservationApiPayload(cpUniqueIdentifier, reservationId)));
	}
}
