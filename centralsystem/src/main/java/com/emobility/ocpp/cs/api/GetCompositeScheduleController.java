package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.emobility.ocpp.cs.api.model.GetCompositeScheduleApiPayload;
import com.emobility.ocpp.profile.operation.GetCompositeScheduleOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.ChargingRateUnitType;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@RestController
public class GetCompositeScheduleController extends ApiBaseController {
	
	@Autowired
	private GetCompositeScheduleOperation getCompositeSchedule;
	
	@GetMapping(path = "/get-composite-schedule/{cpUniqueIdentifier}/{connectiorId}/{duration}/{chargingRateUnit}")
	public void chargingConfiguration(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier,
									  @PathVariable("connectiorId") String connectorIdVal,
									  @PathVariable("duration") String durationVal,
									  @PathVariable("chargingRateUnit") String chargingRateUnitVal) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		int connId = 0;
		int duration = 0;
		
		try {
			connId = Integer.parseInt(connectorIdVal);
			duration = Integer.parseInt(durationVal);
			
		} catch (NumberFormatException ex) {
			Log.error(ClearChargingProfileController.class, ex.getMessage());
			return;
		}
		
		ChargingRateUnitType chargingRateUnit = null;
		
		try {
			chargingRateUnit = ChargingRateUnitType.valueOf(chargingRateUnitVal);
			
		} catch(IllegalArgumentException ex) {
			Log.error(GetCompositeScheduleController.class, "Invalid charging rate unit, the unit should be one of [W, A]");
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, 
				new GetCompositeScheduleApiPayload(cpUniqueIdentifier, connId, duration, chargingRateUnit));
		getCompositeSchedule.submitMessage(reqMsg);
	}
}
