package com.emobility.ocpp.cs.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This controller responsible for triggering the server related actions to the connected central points
 * 
 * @author Salah Abu Msameh
 */
@Controller
@RequestMapping("/api")
public class ServerOperationController {

//	@Autowired
//	private MessageTransmitter msgTransmitter;
	
	/**
	 * Send cancel reservation request
	 */
	@GetMapping(path = "/cancelReservation/{chargePointId}")
	public void sendCancelReservationRequest(@PathVariable("chargePointId") String chargePointId) {
		
//		try {
//			msgTransmitter.sendMessage(chargePointId, new CancelReservationPayload("11254566").buildRequest());
//		} catch (NoSessionFoundException | IOException e) {
//			e.printStackTrace();
//		}
	}
}
