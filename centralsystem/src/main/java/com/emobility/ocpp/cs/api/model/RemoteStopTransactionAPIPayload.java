package com.emobility.ocpp.cs.api.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class RemoteStopTransactionAPIPayload extends ApiPayload {

	private String userId;
	
	/**
	 * 
	 * @param userId
	 */
	public RemoteStopTransactionAPIPayload(String cpUniqueIdentifier, String userId) {
		super(cpUniqueIdentifier);
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
