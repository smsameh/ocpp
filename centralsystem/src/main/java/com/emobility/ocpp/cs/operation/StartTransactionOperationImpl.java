package com.emobility.ocpp.cs.operation;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.cs.service.UserService;
import com.emobility.ocpp.cs.service.UserTransactionService;
import com.emobility.ocpp.profile.operation.StartTransactionOperation;
import com.emobility.ocpp.socket.message.ConfirmMessage;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.model.AuthorizationStatus;
import com.emobility.ocpp.socket.message.model.IdTagInfo;
import com.emobility.ocpp.socket.message.model.StartTransactionConfirmPayload;
import com.emobility.ocpp.socket.message.model.StartTransactionRequestPayload;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class StartTransactionOperationImpl extends StartTransactionOperation {
	
	@Autowired
	private UserService userSrv;
	
	@Autowired
	private UserTransactionService userTransSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private ChargePointService cpSrv;
	
	@Override
	public void handleReceivedMessage(Message requestMessage) {
		
		//audit request
		auditorSrv.audit(AuditOperations.START_TRANSACTION_REQUEST, requestMessage);
		
		StartTransactionRequestPayload startTransReq = (StartTransactionRequestPayload) requestMessage.getPayload();
		
		//1. do sanity check
		String userTokenId = startTransReq.getIdToken().getIdToken();
		AuthorizationStatus status = userSrv.Authorize(userTokenId);
		
		StartTransactionConfirmPayload confTrans = new StartTransactionConfirmPayload();
		String sessionId = requestMessage.getSessionId();
		
		if(AuthorizationStatus.Accepted == status) {
			confTrans.setIdTagInfo(IdTagInfo.create(0, userTokenId, AuthorizationStatus.Accepted));
			confTrans.setTransactionId(userTransSrv.registerUserTransaction(userTokenId, cpSrv.getUniqueIdentefier(sessionId)));
		} else {
			confTrans.setIdTagInfo(IdTagInfo.create(0, userTokenId, status));
		}
		
		ConfirmMessage confMsg = new ConfirmMessage(sessionId, requestMessage.getUniqueId(), confTrans);
		
		try {
			msgTransmitter.sendMessage(confMsg);
			
		} catch (NoSessionMessageFound | IOException e) {
			Log.error(StartTransactionOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		//audit request
		auditorSrv.audit(AuditOperations.START_TRANSACTION_CONFIRM, confMsg);
	}
}
