package com.emobility.ocpp.cs.presentation.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.emobility.ocpp.cs.persistence.entity.ChargePoint;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ChargePointFB {
	
	private long id;
	
	@NotNull
	private String name;
	
	@NotNull
	private String uniqueIdentifier;
	
	@NotNull
	@Size(min = 1, max = 32)
	private String serialNo;
	
	@NotNull
	private String model;
	
	@NotNull
	private String vendor;
	
	@NotNull
	private String status;
	
	private String ipAddress;
	private String port;
	private String address;
	private String city;
	private String contactPerson;
	
	/**
	 * 
	 */
	public ChargePointFB() {}
	
	/**
	 * 
	 * @param cp
	 */
	public ChargePointFB(ChargePoint cp) {
		
		this.id = cp.getId();
		this.name = cp.getName();
		this.uniqueIdentifier = cp.getUniqueIdentifier();
		this.serialNo = cp.getSerialNo();
		this.model = cp.getModel();
		this.vendor = cp.getVendor();
		this.status = cp.getStatus();
		this.ipAddress = cp.getIpAdress();
		this.port = cp.getPort();
		this.address = cp.getAdress();
		this.city = cp.getCity();
		this.contactPerson = cp.getContactPerson();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUniqueIdentifier() {
		return uniqueIdentifier;
	}
	
	public void setUniqueIdentifier(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}
	
	public String getSerialNo() {
		return serialNo;
	}
	
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public String getVendor() {
		return vendor;
	}
	
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	public String getPort() {
		return port;
	}
	
	public void setPort(String port) {
		this.port = port;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getContactPerson() {
		return contactPerson;
	}
	
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
}
