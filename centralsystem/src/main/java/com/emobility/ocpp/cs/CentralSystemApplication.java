package com.emobility.ocpp.cs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 
 * @author Salah Abu Msameh
 */
@SpringBootApplication
@ComponentScan("com.emobility.ocpp")
public class CentralSystemApplication {
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(CentralSystemApplication.class, args);
	}
}
