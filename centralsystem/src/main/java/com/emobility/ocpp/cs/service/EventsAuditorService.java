package com.emobility.ocpp.cs.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.config.SystemConfigs;
import com.emobility.ocpp.cs.persistence.BSSEventsDao;
import com.emobility.ocpp.cs.persistence.entity.BSSEventDetail;
import com.emobility.ocpp.cs.persistence.entity.BSSEventMaster;
import com.emobility.ocpp.cs.persistence.entity.EventOperation;
import com.emobility.ocpp.cs.persistence.entity.OperationFieldMapping;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.util.JSONUtils;
import com.emobility.ocpp.util.Log;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * 
 * @author Salah Abu Msameh
 */
@Service
public class EventsAuditorService {
	
	@Autowired
	private BSSEventsDao bssEventDao;
	
	@Autowired
	private SystemConfigs systemConfigs;
	
	@Autowired
	private ChargePointService cpSrv;
	
	private final BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(100);
	private ExecutorService executor = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, queue);
	
	private Map<Long, List<OperationFieldMapping>> fieldsMappingCache = new HashMap<Long, List<OperationFieldMapping>>();
	private List<EventOperation> operations = null;
	
	/**
	 * 
	 * @param operationId
	 * @param cpSerialNo
	 * @param msg
	 */
	public void audit(int operationId, Message msg) {
		
		if(!systemConfigs.isEventAuditEnabled()) {
			return;
		}
		
		//add to executor
		executor.execute(new MessageTask(operationId, cpSrv.getUniqueIdentefier(msg.getSessionId()), msg));
	}
	
	/**
	 * helper class
	 */
	@Configurable
	class MessageTask implements Runnable {
		
		private int operationId;
		private String cpUniqueIdentefier;
		private Message msg;
		
		/**
		 * 
		 * @param operationId
		 * @param cpSerialNo
		 * @param msg
		 */
		public MessageTask(int operationId, String cpUniqueIdentefier, Message msg) {
			this.operationId = operationId;
			this.cpUniqueIdentefier = cpUniqueIdentefier;
			this.msg = msg;
		}
		
		@Override
		public void run() {
			
			BSSEventMaster eventMaster = getEventMaster(this.operationId, this.cpUniqueIdentefier, this.msg.getMessageType().getMessageTypeId());
			List<OperationFieldMapping> fieldMappings = getOperationFields(operationId);
			
			JsonElement payloadJsonElement = new JsonParser().parse(JSONUtils.parse(msg.getPayload()));
			List<BSSEventDetail> details = new ArrayList<BSSEventDetail>();
			eventMaster.setEventDetails(details);
			
			for(OperationFieldMapping fieldMapping : fieldMappings) {
				
				List<String> values = new ArrayList<String>();
				JSONUtils.getJsonAttKeyValue(payloadJsonElement, fieldMapping.getJsonAttributeKey(), values);
				
				for(String value : values) {
					bssEventDao.save(new BSSEventDetail(eventMaster.getEventId(), fieldMapping.getFieldId(), value));
				}
			}
			
			//save to db
			//bssEventRepo.save(eventMaster);
		}
		
		/**
		 * 
		 * @param operationId
		 * @return
		 */
		private List<OperationFieldMapping> getOperationFields(long operationId) {
			
			List<OperationFieldMapping> operationFields = fieldsMappingCache.get(operationId);
			
			if(operationFields != null) {
				return operationFields;
			}
			
			if((operationFields = bssEventDao.findOperationFields(operationId)) != null) {
				fieldsMappingCache.put(operationId, operationFields);
			}
			
			return operationFields;
		}

		/**
		 * 
		 * @param operationId
		 * @param cpSerialNo
		 * @param messageTypeId
		 * @return
		 */
		private BSSEventMaster getEventMaster(int operationId, String cpUniqueIdentefier, int messageTypeId) {
			
			BSSEventMaster eventMaster = new BSSEventMaster();
			
			eventMaster.setOperationId(operationId);
			eventMaster.setCpUniqueIdentefier(cpUniqueIdentefier);
			eventMaster.setEventDate(new Date());
			eventMaster.setMsgTypeId(messageTypeId);
			
			bssEventDao.save(eventMaster);
			
			return eventMaster;
		}
	}
	
	/**
	 * 
	 * @param operationId
	 * @return
	 */
	public List<BSSEventMaster> getEvents(String operationId) {
		
		if(!StringUtils.isEmpty(operationId)) {
			try {
				long oprId = Long.parseLong(operationId);
				return bssEventDao.findEventsByOperationId(oprId);
				
			} catch (NumberFormatException ex) {
				Log.error(EventsAuditorService.class, "Invalide operation id number, ignore and get all events");
			}
		}
		
		return bssEventDao.findAllEvents();
	}
	
	/**
	 * 
	 * @param eventId
	 * @return
	 */
	public BSSEventMaster getEvent(long eventId) {
		return bssEventDao.findById(BSSEventMaster.class, eventId);
	}
	
	/**
	 * 
	 * @param eventId
	 * @return
	 */
	public Map<String, String> getEventDetails(long eventId) {
		return bssEventDao.findDetailsByEventId(eventId);
	}
	
//	/**
//	 * 
//	 * @param operationId
//	 * @return
//	 */
//	public List<BSSEventMaster> getEvents(long... operationIds) {
//		
//		if(operationIds != null && operationIds.length == 0) {
//			try {
//				return bssEventRepo.findEventsByOperationIds(operationIds);
//				
//			} catch (NumberFormatException ex) {
//				Log.error(EventsAuditorService.class, "Invalide operation id number, ignore and get all events");
//			}
//		}
//		
//		return bssEventRepo.findAllEvents();
//	}
	
	/**
	 * 
	 * @return
	 */
	public List<EventOperation> getEventOperations() {
		
		if(operations == null) {
			this.operations = bssEventDao.findOperations();
		}
		
		return this.operations;
	}
	
	/**
	 * Shutdown executor
	 */
	public void shutdown() {
		this.executor.shutdown();
	}
}
