package com.emobility.ocpp.cs.operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.api.model.SendLocalListApiPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.profile.operation.SendLocalListOperation;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.AuthorizationData;
import com.emobility.ocpp.socket.message.model.IdToken;
import com.emobility.ocpp.socket.message.model.SendLocalListRequestPayload;
import com.emobility.ocpp.socket.message.model.UpdateType;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class SendLocalListOperationImpl extends SendLocalListOperation {

	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof SendLocalListApiPayload)) {
			Log.error(SendLocalListOperationImpl.class, "Invalid payload type for [SendLocalListOperation] operation");
			return;
		}
		
		SendLocalListApiPayload payload = (SendLocalListApiPayload) message.getPayload();
		
		String cpUniqueIdentifier = payload.getCpUniqueIdentifier();
		String userId = payload.getUserId();
		int listVersion = payload.getListVersion();
		UpdateType type = payload.getUpdateType();
		
		//1. validate charge point
		ChargePoint cp = cpSrv.getChargePoint(cpUniqueIdentifier);
		
		if(cp == null) {
			Log.error(SendLocalListOperationImpl.class, "No charge point registered found for unique identifier [" + cpUniqueIdentifier + "]");
			return;
		}
		
		String sessionId = cpSrv.getLinkedSessionId(cpUniqueIdentifier);
		
		if(StringUtils.isEmpty(sessionId)) {
			Log.error(SendLocalListOperationImpl.class,  "No active session found for charge point [" + cpUniqueIdentifier + "]");
			return;
		}
		
		//1. create request message and payload
		SendLocalListRequestPayload reqPayload = new SendLocalListRequestPayload();
		AuthorizationData authData = new AuthorizationData(new IdToken(userId), null);
		
		reqPayload.setLocalAuthorizationList(authData);
		reqPayload.setListVersion(listVersion);
		reqPayload.setUpdateType(type);
		
		String messageId = Utils.generateMessageId();
		
		RequestMessage reqMsg = new RequestMessage(messageId, OperationActions.SEND_LOCAL_LIST, reqPayload);
		reqMsg.setSessionId(sessionId);
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(messageId, reqMsg);
			
		} catch (Exception e) {
			Log.error(SendLocalListOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.SEND_LOCAL_LIST_REQUEST, reqMsg);
	}

	@Override
	public void handleReceivedMessage(Message message) {
		
		auditorSrv.audit(AuditOperations.SEND_LOCAL_LIST_CONFIRM, message);
		msgTracker.removeMessage(message.getUniqueId());
	}
}
