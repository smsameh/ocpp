package com.emobility.ocpp.cs.presentation.form;

import com.emobility.ocpp.cs.persistence.entity.Price;

/**
 * 
 * @author Salah Abu Msameh
 */
public class PricingFB {

	private double acEuroKwh;
	private double acEuroHour;
	private double revEuroMinute;
	private double dcEuroKwh;
	private double dcEuroMinute;
	
	public PricingFB() {}
	
	/**
	 * 
	 * @param price
	 */
	public PricingFB(Price price) {
		
		if(price == null) {
			return;
		}
		
		this.acEuroKwh = price.getAcEuroKwh();
		this.acEuroHour = price.getAcEuroHour();
		this.revEuroMinute = price.getRevEuroMinute();
		this.dcEuroKwh = price.getDcEuroKwh();
		this.dcEuroMinute = price.getDcEuroMinute();
	}

	public double getAcEuroKwh() {
		return acEuroKwh;
	}
	
	public void setAcEuroKwh(double acEuroKwh) {
		this.acEuroKwh = acEuroKwh;
	}
	
	public double getAcEuroHour() {
		return acEuroHour;
	}
	
	public void setAcEuroHour(double acEuroHour) {
		this.acEuroHour = acEuroHour;
	}
	
	public double getRevEuroMinute() {
		return revEuroMinute;
	}
	
	public void setRevEuroMinute(double revEuroMinute) {
		this.revEuroMinute = revEuroMinute;
	}
	
	public double getDcEuroKwh() {
		return dcEuroKwh;
	}
	
	public void setDcEuroKwh(double dcEuroKwh) {
		this.dcEuroKwh = dcEuroKwh;
	}
	
	public double getDcEuroMinute() {
		return dcEuroMinute;
	}
	
	public void setDcEuroMinute(double dcEuroMinute) {
		this.dcEuroMinute = dcEuroMinute;
	}
}
