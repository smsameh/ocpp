package com.emobility.ocpp.cs.api.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class GetLocalListVersionOperationApiPayload extends ApiPayload {
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 */
	public GetLocalListVersionOperationApiPayload(String cpUniqueIdentifier) {
		super(cpUniqueIdentifier);
	}
}
