package com.emobility.ocpp.cs.presentation.form;

import java.util.Date;

import javax.validation.constraints.Size;

import com.emobility.ocpp.cs.persistence.entity.User;

/**
 * User form bean
 * 
 * @author Salah Abu Msameh
 */
public class UserFB {
	
	private String userId;
	private String fullName;
	private int status;// (Active/Inactive/Suspended)
	private int organizationId;
	private Date subscriptionDate;
	
	@Size(min = 1, max = 10)
	private String paymentMethod;
	
	@Size(min = 1, max = 100)
	private String paymentInfo;
	
	/**
	 * 
	 */
	public UserFB() {}
	
	/**
	 * 
	 * @param user
	 */
	public UserFB(User user) {
		
		if(user == null) {
			return;
		}
		
		this.userId = user.getUserId();
		this.fullName = user.getFullName();
		this.status = user.getStatus();
		this.organizationId = user.getOrganizationId();
		this.paymentMethod = user.getPaymentMethod();
		this.paymentInfo = user.getPaymentInfo();
	}

	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getOrganizationId() {
		return organizationId;
	}
	
	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}
	
	public Date getSubscriptionDate() {
		return subscriptionDate;
	}
	
	public void setSubscriptionDate(Date subscriptionDate) {
		this.subscriptionDate = subscriptionDate;
	}
	
	public String getPaymentMethod() {
		return paymentMethod;
	}
	
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	public String getPaymentInfo() {
		return paymentInfo;
	}
	
	public void setPaymentInfo(String paymentInfo) {
		this.paymentInfo = paymentInfo;
	}
}
