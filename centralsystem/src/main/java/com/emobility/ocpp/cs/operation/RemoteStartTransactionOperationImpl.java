package com.emobility.ocpp.cs.operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.api.model.RemoteStartTransactionAPIPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.cs.service.UserService;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.profile.operation.RemoteStartTransactionOperation;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.AuthorizationStatus;
import com.emobility.ocpp.socket.message.model.ChargingProfile;
import com.emobility.ocpp.socket.message.model.ChargingProfileKindType;
import com.emobility.ocpp.socket.message.model.ChargingProfilePurposeType;
import com.emobility.ocpp.socket.message.model.ChargingRateUnitType;
import com.emobility.ocpp.socket.message.model.ChargingSchedule;
import com.emobility.ocpp.socket.message.model.ChargingSchedulePeriod;
import com.emobility.ocpp.socket.message.model.IdToken;
import com.emobility.ocpp.socket.message.model.RemoteStartTransactionRequestPayload;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class RemoteStartTransactionOperationImpl extends RemoteStartTransactionOperation {
	
	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private UserService userSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof RemoteStartTransactionAPIPayload)) {
			Log.error(RemoteStartTransactionOperationImpl.class, "Invalid payload type for [RemoteStartTransaction] operation");
			return;
		}
		
		RemoteStartTransactionAPIPayload payload = (RemoteStartTransactionAPIPayload) message.getPayload();
		
		String userId = payload.getUserId();
		String cpUniqueIdentifier = payload.getCpUniqueIdentifier();
		int connectorId = payload.getConnectorId();
		
		//1. validate charge point
		ChargePoint cp = cpSrv.getChargePoint(cpUniqueIdentifier);
		
		if(cp == null) {
			Log.error(RemoteStartTransactionOperationImpl.class, 
					"No charge point registered found for unique identifier [" + cpUniqueIdentifier + "]");
			return;
		}
		
		String sessionId = cpSrv.getLinkedSessionId(cpUniqueIdentifier);
		
		if(StringUtils.isEmpty(sessionId)) {
			Log.error(RemoteStartTransactionOperationImpl.class,  "No active session found for charge point [" + cpUniqueIdentifier + "]");
			return;
		}
		
		//2. validate user
		AuthorizationStatus authStatus = userSrv.Authorize(userId);
		
		if(authStatus != AuthorizationStatus.Accepted) {
			Log.error(RemoteStartTransactionOperationImpl.class, "Connot submit request for user status [" + authStatus + "]");
			return;
		}
		
		//1. create request message and payload
		RemoteStartTransactionRequestPayload remoteStartReq = new RemoteStartTransactionRequestPayload();
		
		remoteStartReq.setConnectorId(connectorId);
		remoteStartReq.setIdToken(new IdToken(userId));
		remoteStartReq.setChargingProfile(getChargingProfile());
		
		String messageId = Utils.generateMessageId();
		
		RequestMessage reqMsg = new RequestMessage(messageId, OperationActions.REMOTE_START_TRANSACTION, remoteStartReq);
		reqMsg.setSessionId(sessionId);
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(messageId, reqMsg);
			
		} catch (Exception e) {
			Log.error(RemoteStartTransactionOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.REMOTE_START_TRANSACTION_REQUEST, reqMsg);
	}

	@Override
	public void handleReceivedMessage(Message message) {
		
		// audit confirm 
		auditorSrv.audit(AuditOperations.REMOTE_START_TRANSACTION_CONFIRM, message);
		
		//remove message from tracker
		msgTracker.removeMessage(message.getUniqueId());
	}
	
	/**
	 * 
	 * @return
	 */
	private ChargingProfile getChargingProfile() {
		
		ChargingProfile chargingProfile = new ChargingProfile();
		
		chargingProfile.setChargingProfileId(1);
		chargingProfile.setChargingProfilePurpose(ChargingProfilePurposeType.TxProfile);
		chargingProfile.setChargingProfileKind(ChargingProfileKindType.Relative);
		
		ChargingSchedule chargeSchedule = new ChargingSchedule();
		
		chargeSchedule.setChargingRateUnit(ChargingRateUnitType.W);
		chargeSchedule.setChargingSchedulePeriod(new ChargingSchedulePeriod(100, 200, 0));
		
		chargingProfile.setChargingSchedule(chargeSchedule);
		
		return chargingProfile;
	}
}
