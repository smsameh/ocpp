package com.emobility.ocpp.cs.operation;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.api.model.ChangeAvailabilityApiPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.ChangeAvailabilityOperation;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.AvailabilityStatus;
import com.emobility.ocpp.socket.message.model.AvailabilityType;
import com.emobility.ocpp.socket.message.model.ChangeAvailabilityConfirmPayload;
import com.emobility.ocpp.socket.message.model.ChangeAvailabilityRequestPayload;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ChangeAvailabilityOperationImpl extends ChangeAvailabilityOperation {
	
	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;

	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof ChangeAvailabilityApiPayload)) {
			Log.error(RemoteStartTransactionOperationImpl.class, "Invalid payload type for [ChangeAvailabilityOperation] operation");
			return;
		}
		
		ChangeAvailabilityApiPayload payload = (ChangeAvailabilityApiPayload) message.getPayload();
		
		String cpUniqueIdentifier = payload.getCpUniqueIdentifier();
		int connectorId = payload.getConnectorId();
		AvailabilityType type = payload.getType();
		
		//1. validate charge point
		if(cpSrv.getChargePoint(cpUniqueIdentifier) == null) {
			Log.error(RemoteStartTransactionOperationImpl.class, "No charge point registered found for unique identifier [" + cpUniqueIdentifier + "]");
			return;
		}
		
		String sessionId = cpSrv.getLinkedSessionId(cpUniqueIdentifier);
		
		if(StringUtils.isEmpty(sessionId)) {
			Log.error(RemoteStartTransactionOperationImpl.class,  "No active session found for charge point [" + cpUniqueIdentifier + "]");
			return;
		}
		
		String uniqueMsgId = Utils.generateMessageId();
		
		RequestMessage reqMsg = new RequestMessage(uniqueMsgId, OperationActions.CHANGE_AVAILABILITY, 
				new ChangeAvailabilityRequestPayload(connectorId, type));
		
		reqMsg.setSessionId(sessionId);
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(uniqueMsgId, reqMsg);
			
		} catch (NoSessionMessageFound | IOException e) {
			Log.error(CancelReservationOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.CHANGE_AVAILABILITY_REQUEST, reqMsg);
	}

	@Override
	public void handleReceivedMessage(Message message) {
		
		ChangeAvailabilityConfirmPayload payload = (ChangeAvailabilityConfirmPayload) message.getPayload();
		
		if(payload.getStatus() == AvailabilityStatus.Accepted) {
			AvailabilityType type = ((ChangeAvailabilityRequestPayload) msgTracker.getSubmittedMessage(message.getUniqueId()).getPayload()).getType();
			cpSrv.updateChargePointStatus(message.getSessionId(), type.toString());
		}
		
		auditorSrv.audit(AuditOperations.CHANGE_AVAILABILITY_CONFIRM, message);
		msgTracker.removeMessage(message.getUniqueId());
	}
}
