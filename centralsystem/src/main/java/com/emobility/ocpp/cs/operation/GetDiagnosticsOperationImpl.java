package com.emobility.ocpp.cs.operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.api.model.GetDiagnosticsApiPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.GetDiagnosticsOperation;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.GetDiagnosticsRequestPayload;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class GetDiagnosticsOperationImpl extends GetDiagnosticsOperation {

	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof GetDiagnosticsApiPayload)) {
			Log.error(GetDiagnosticsOperationImpl.class, "Invalid payload type for [GetDiagnosticsOperation] operation");
			return;
		}
		
		GetDiagnosticsApiPayload payload = (GetDiagnosticsApiPayload) message.getPayload();
		
		String cpUniqueIdentifier = payload.getCpUniqueIdentifier();
		
		//1. validate charge point
		ChargePoint cp = cpSrv.getChargePoint(cpUniqueIdentifier);
		
		if(cp == null) {
			Log.error(GetDiagnosticsOperationImpl.class, "No charge point registered found for unique identifier [" + cpUniqueIdentifier + "]");
			return;
		}
		
		String sessionId = cpSrv.getLinkedSessionId(cpUniqueIdentifier);
		
		if(StringUtils.isEmpty(sessionId)) {
			Log.error(GetDiagnosticsOperationImpl.class,  "No active session found for charge point [" + cpUniqueIdentifier + "]");
			return;
		}
		
		//1. create request message and payload
		GetDiagnosticsRequestPayload reqPayload = new GetDiagnosticsRequestPayload();
		
		reqPayload.setLocation("url");
		reqPayload.setRetries(3);
		reqPayload.setRetryInterval(30);
		
		String messageId = Utils.generateMessageId();
		
		RequestMessage reqMsg = new RequestMessage(messageId, OperationActions.GET_DIAGNOSTICS, reqPayload);
		reqMsg.setSessionId(sessionId);
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(messageId, reqMsg);
			
		} catch (Exception e) {
			Log.error(GetDiagnosticsOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.GET_DIAGNOSTICS_REQUEST, reqMsg);
	}

	@Override
	public void handleReceivedMessage(Message message) {
		
		auditorSrv.audit(AuditOperations.GET_DIAGNOSTICS_CONFIRM, message);
		msgTracker.removeMessage(message.getUniqueId());
	}
}
