package com.emobility.ocpp.cs.operation;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.DataTransferOperartion;
import com.emobility.ocpp.socket.message.ConfirmMessage;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.model.DataTransferConfirmPayload;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class DataTransferOperartionImple extends DataTransferOperartion {
	
	@Autowired
	private EventsAuditorService auditorSrv;

	@Override
	public void handleReceivedMessage(Message requestMessage) {
		
		auditorSrv.audit(AuditOperations.DATA_TRANSFER_REQUEST, requestMessage);
		
		//handle data
		ConfirmMessage confMsg = new ConfirmMessage(requestMessage.getSessionId(), 
				requestMessage.getUniqueId(), new DataTransferConfirmPayload());
		
		try {
			msgTransmitter.sendMessage(confMsg);
		} catch (NoSessionMessageFound | IOException e) {
			Log.error(DataTransferOperartionImple.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.DATA_TRANSFER_REQUEST, confMsg);
	}
}
