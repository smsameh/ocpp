package com.emobility.ocpp.cs.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.emobility.ocpp.cs.persistence.entity.ChargePoint;

/**
 * 
 * @author Salah Abu Msameh
 */
@Repository
public interface ChargePointRepository extends CrudRepository<ChargePoint, Long> {
	
	/**
	 * Find charge point by the given unique charge point name
	 * 
	 * @param chargePointName
	 * @return
	 */
	public ChargePoint findByUniqueIdentifier(@Param("cpName") String chargePointName);
	
	/**
	 * Find charge point by the given unique charge point name
	 * 
	 * @param chargePointName
	 * @return
	 */
	public ChargePoint findBySerialNo(@Param("cpSerialNo") String cpSerialNo);
}
