package com.emobility.ocpp.cs.api.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class ClearCacheApiPayload extends ApiPayload {

	public ClearCacheApiPayload(String cpUniqueIdentifier) {
		super(cpUniqueIdentifier);
	}
}
