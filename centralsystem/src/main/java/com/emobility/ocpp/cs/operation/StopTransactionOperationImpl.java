package com.emobility.ocpp.cs.operation;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.cs.service.UserService;
import com.emobility.ocpp.cs.service.UserTransactionService;
import com.emobility.ocpp.profile.operation.StopTransactionOperation;
import com.emobility.ocpp.socket.message.ConfirmMessage;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.model.AuthorizationStatus;
import com.emobility.ocpp.socket.message.model.IdTagInfo;
import com.emobility.ocpp.socket.message.model.StopTransactionConfirmPayload;
import com.emobility.ocpp.socket.message.model.StopTransactionRequestPayload;
import com.emobility.ocpp.socket.session.NoSessionMessageFound;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class StopTransactionOperationImpl extends StopTransactionOperation {
	
	@Autowired
	private UserService userSrv;
	
	@Autowired
	private UserTransactionService userTransSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Override
	public void handleReceivedMessage(Message requestMessage) {
		
		//audit request
		auditorSrv.audit(AuditOperations.STOP_TRANSACTION_REQUEST, requestMessage);
		
		StopTransactionRequestPayload stopTransReq = (StopTransactionRequestPayload) requestMessage.getPayload();
		
		//1. do sanity check
		String userTokenId = stopTransReq.getIdToken().getIdToken();
		AuthorizationStatus status = userSrv.Authorize(userTokenId, true);
		
		StopTransactionConfirmPayload stopTransConf = new StopTransactionConfirmPayload();
		
		if(AuthorizationStatus.Accepted == status) {
			stopTransConf.setIdTagInfo(IdTagInfo.create(0, stopTransReq.getIdToken().getIdToken(), AuthorizationStatus.Accepted));
		} else {
			stopTransConf.setIdTagInfo(IdTagInfo.create(0, stopTransReq.getIdToken().getIdToken(), status));
		}
		
		ConfirmMessage confMsg = new ConfirmMessage(requestMessage.getSessionId(), requestMessage.getUniqueId(), stopTransConf);
		
		try {
			msgTransmitter.sendMessage(confMsg);
			
			//end transaction
			userTransSrv.endUserTransaction(userTokenId);
			
		} catch (NoSessionMessageFound | IOException e) {
			Log.error(StopTransactionOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		//audit confirm
		auditorSrv.audit(AuditOperations.STOP_TRANSACTION_CONFIRM, confMsg);
	}
}
