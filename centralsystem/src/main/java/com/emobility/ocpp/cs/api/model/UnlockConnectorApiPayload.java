package com.emobility.ocpp.cs.api.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class UnlockConnectorApiPayload extends ApiPayload {

	private int connectorId;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @param connectorId
	 */
	public UnlockConnectorApiPayload(String cpUniqueIdentifier, int connectorId) {
		super(cpUniqueIdentifier);
		this.connectorId = connectorId;
	}

	public int getConnectorId() {
		return connectorId;
	}
	
	public void setConnectorId(int connectorId) {
		this.connectorId = connectorId;
	}
}
