package com.emobility.ocpp.cs.api.model;

import com.emobility.ocpp.socket.message.model.Payload;

/**
 * 
 * @author Salah Abu Msameh
 */
public abstract class ApiPayload implements Payload {

	protected String cpUniqueIdentifier;
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 */
	public ApiPayload(String cpUniqueIdentifier) {
		this.cpUniqueIdentifier = cpUniqueIdentifier;
	}

	public String getCpUniqueIdentifier() {
		return cpUniqueIdentifier;
	}

	public void setCpUniqueIdentifier(String cpUniqueIdentifier) {
		this.cpUniqueIdentifier = cpUniqueIdentifier;
	}
}
