package com.emobility.ocpp.cs.presentation;

/**
 * 
 * @author Salah Abu Msameh
 */
public class Pages {

	public static final String DEFAULT = "default";
	
	public static final String CHARGE_POINTS = "cp/chargePoints";
	public static final String VIEW_CHARGE_POINT = "cp/viewChargePoint";
	public static final String ADD_CHARGE_POINT = "cp/addChargePoint";
	public static final String EDIT_CHARGE_POINT = "cp/editChargePoint";
	public static final String REDIRECT_CHARGE_POINTS = "redirect:/admin/" + CHARGE_POINTS;
	
	//users
	public static final String USERS = "user/users";
	public static final String REDIRECT_USERS = "redirect:/admin/" + USERS;
	public static final String ADD_USER = "user/addUser";
	public static final String EDIT_USER = "user/editUser";
	
	public static final String EVENTS_LOG = "events/eventsLog";
	public static final String EVENT_DETAILS = "events/eventDetails";
}
