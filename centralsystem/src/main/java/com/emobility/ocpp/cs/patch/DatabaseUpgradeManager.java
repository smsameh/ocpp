package com.emobility.ocpp.cs.patch;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.config.SystemConfigs;
import com.emobility.ocpp.cs.service.CommonDao;
import com.emobility.ocpp.cs.util.FileUtils;
import com.emobility.ocpp.cs.util.XmlUtils;

/**
 * This service is responsible for upgrading the central system database, for now the DML query are supported only
 * 
 * @author Salah Abu Msameh
 */
@Component
public class DatabaseUpgradeManager {

	@Autowired
	private SystemConfigs systemConfigs;
	
	@Autowired
	private CommonDao dao;
	
	/**
	 * upgrade central system database based on the current patch version
	 */
	@Transactional
	public void upgrade() {
		
		String patchVersion = systemConfigs.getPatchVersion();
		String systemVersion = systemConfigs.getSystemVersion();
		
		if(StringUtils.isEmpty(patchVersion) || StringUtils.isEmpty(systemVersion) || patchVersion.equals(systemVersion)) {
			return;
		}
		
		String patchFileContent = FileUtils.loadResourceFileContent(getPatchFileName(patchVersion));
		
		if(StringUtils.isEmpty(patchFileContent)) {
			return;
		}
		
		PatchContent patch = XmlUtils.convertToObject(PatchContent.class, patchFileContent);
		
		if(patch == null) {
			return;
		}
		
		for(String script : patch.getScripts()) {
			try {
				dao.execute(script);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param patchVersion
	 * @return
	 */
	private String getPatchFileName(String patchVersion) {
		return patchVersion.replace(".", "_").concat(".patch");
	}
}
