package com.emobility.ocpp.cs.operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.api.model.TriggerMessageApiPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.profile.operation.TriggerMessageOperation;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.MessageTrigger;
import com.emobility.ocpp.socket.message.model.TriggerMessageRequestPayload;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class TriggerMessageOperationImpl extends TriggerMessageOperation {

	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof TriggerMessageApiPayload)) {
			Log.error(TriggerMessageOperationImpl.class, "Invalid payload type for [TriggerMessageOperation] operation");
			return;
		}
		
		TriggerMessageApiPayload payload = (TriggerMessageApiPayload) message.getPayload();
		
		String cpUniqueIdentifier = payload.getCpUniqueIdentifier();
		int connectorId = payload.getConnectorId();
		MessageTrigger requestedMsg = payload.getRequestedMessage();
		
		//1. validate charge point
		ChargePoint cp = cpSrv.getChargePoint(cpUniqueIdentifier);
		
		if(cp == null) {
			Log.error(TriggerMessageOperationImpl.class, "No charge point registered found for unique identifier [" + cpUniqueIdentifier + "]");
			return;
		}
		
		String sessionId = cpSrv.getLinkedSessionId(cpUniqueIdentifier);
		
		if(StringUtils.isEmpty(sessionId)) {
			Log.error(TriggerMessageOperationImpl.class,  "No active session found for charge point [" + cpUniqueIdentifier + "]");
			return;
		}
		
		//1. create request message and payload
		TriggerMessageRequestPayload reqPayload = new TriggerMessageRequestPayload();
		
		reqPayload.setConnectorId(connectorId);
		reqPayload.setRequestedMessage(requestedMsg);
		
		String messageId = Utils.generateMessageId();
		
		RequestMessage reqMsg = new RequestMessage(messageId, OperationActions.TRIGGER_MESSAGE, reqPayload);
		reqMsg.setSessionId(sessionId);
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(messageId, reqMsg);
			
		} catch (Exception e) {
			Log.error(TriggerMessageOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.TRIGGER_MESSAGE_REQUEST, reqMsg);
	}

	@Override
	public void handleReceivedMessage(Message message) {
		
		auditorSrv.audit(AuditOperations.TRIGGER_MESSAGE_CONFIRM, message);
		msgTracker.removeMessage(message.getUniqueId());
	}
}
