package com.emobility.ocpp.cs.api.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class RemoteStartTransactionAPIPayload extends ApiPayload {
	
	private String userId;
	private int connectorId;
	
	
	/**
	 * 
	 * @param userId
	 * @param cpUniqueIdentifier
	 * @param connectorId
	 */
	public RemoteStartTransactionAPIPayload(String userId, String cpUniqueIdentifier, int connectorId) {
		super(cpUniqueIdentifier);
		this.userId = userId;
		this.connectorId = connectorId;
	}

	public int getConnectorId() {
		return connectorId;
	}
	
	public String getCpUniqueIdentifier() {
		return cpUniqueIdentifier;
	}
	
	public String getUserId() {
		return userId;
	}
}