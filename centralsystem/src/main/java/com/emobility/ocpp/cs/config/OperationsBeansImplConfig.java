package com.emobility.ocpp.cs.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.emobility.ocpp.cs.operation.AuthorizationOperationImpl;
import com.emobility.ocpp.cs.operation.BootNotificationOperationImpl;
import com.emobility.ocpp.cs.operation.CancelReservationOperationImpl;
import com.emobility.ocpp.cs.operation.ChangeAvailabilityOperationImpl;
import com.emobility.ocpp.cs.operation.ChangeConfigurationOperationImpl;
import com.emobility.ocpp.cs.operation.ClearCacheOperationImpl;
import com.emobility.ocpp.cs.operation.ClearChargingProfileOperationImpl;
import com.emobility.ocpp.cs.operation.DataTransferOperartionImple;
import com.emobility.ocpp.cs.operation.DiagnosticsStatusNotificationOperationImpl;
import com.emobility.ocpp.cs.operation.FirmwareStatusNotificationOperationImpl;
import com.emobility.ocpp.cs.operation.GetCompositeScheduleOperationImpl;
import com.emobility.ocpp.cs.operation.GetConfigurationOperationImpl;
import com.emobility.ocpp.cs.operation.GetDiagnosticsOperationImpl;
import com.emobility.ocpp.cs.operation.GetLocalListVersionOperationImpl;
import com.emobility.ocpp.cs.operation.HeartbeatOperationImpl;
import com.emobility.ocpp.cs.operation.MeterValuesOperationImpl;
import com.emobility.ocpp.cs.operation.RemoteStartTransactionOperationImpl;
import com.emobility.ocpp.cs.operation.RemoteStopTransactionOperationImpl;
import com.emobility.ocpp.cs.operation.ReserveNowOperationImpl;
import com.emobility.ocpp.cs.operation.ResetOperationImpl;
import com.emobility.ocpp.cs.operation.SendLocalListOperationImpl;
import com.emobility.ocpp.cs.operation.SetChargingProfileOperationImpl;
import com.emobility.ocpp.cs.operation.StartTransactionOperationImpl;
import com.emobility.ocpp.cs.operation.StatusNotificationOperationImpl;
import com.emobility.ocpp.cs.operation.StopTransactionOperationImpl;
import com.emobility.ocpp.cs.operation.TriggerMessageOperationImpl;
import com.emobility.ocpp.cs.operation.UnlockConnectorOperationImpl;
import com.emobility.ocpp.cs.operation.UpdateFirmwareOperationImpl;
import com.emobility.ocpp.profile.config.OperationsBeansConfig;
import com.emobility.ocpp.profile.operation.AuthorizationOperation;
import com.emobility.ocpp.profile.operation.BootNotificationOperation;
import com.emobility.ocpp.profile.operation.CancelReservationOperation;
import com.emobility.ocpp.profile.operation.ChangeAvailabilityOperation;
import com.emobility.ocpp.profile.operation.ChangeConfigurationOperation;
import com.emobility.ocpp.profile.operation.ClearCacheOperation;
import com.emobility.ocpp.profile.operation.ClearChargingProfileOperation;
import com.emobility.ocpp.profile.operation.DataTransferOperartion;
import com.emobility.ocpp.profile.operation.DiagnosticsStatusNotificationOperation;
import com.emobility.ocpp.profile.operation.FirmwareStatusNotificationOperation;
import com.emobility.ocpp.profile.operation.GetCompositeScheduleOperation;
import com.emobility.ocpp.profile.operation.GetConfigurationOperation;
import com.emobility.ocpp.profile.operation.GetDiagnosticsOperation;
import com.emobility.ocpp.profile.operation.GetLocalListVersionOperation;
import com.emobility.ocpp.profile.operation.HeartbeatOperation;
import com.emobility.ocpp.profile.operation.MeterValuesOperation;
import com.emobility.ocpp.profile.operation.RemoteStartTransactionOperation;
import com.emobility.ocpp.profile.operation.RemoteStopTransactionOperation;
import com.emobility.ocpp.profile.operation.ReserveNowOperation;
import com.emobility.ocpp.profile.operation.ResetOperation;
import com.emobility.ocpp.profile.operation.SendLocalListOperation;
import com.emobility.ocpp.profile.operation.SetChargingProfileOperation;
import com.emobility.ocpp.profile.operation.StartTransactionOperation;
import com.emobility.ocpp.profile.operation.StatusNotificationOperation;
import com.emobility.ocpp.profile.operation.StopTransactionOperation;
import com.emobility.ocpp.profile.operation.TriggerMessageOperation;
import com.emobility.ocpp.profile.operation.UnlockConnectorOperation;
import com.emobility.ocpp.profile.operation.UpdateFirmwareOperation;

/**
 * 
 * @author Salah Abu Msameh
 */
@Configuration
public class OperationsBeansImplConfig extends OperationsBeansConfig {
	
	@Bean
	@Override
	public BootNotificationOperation getBootNotificationOperation() {
		return new BootNotificationOperationImpl();
	}
	
	@Bean
	@Override
	public DataTransferOperartion getDataTransferOperartion() {
		return new DataTransferOperartionImple();
	}
	
	@Bean
	@Override
	public HeartbeatOperation getHeartbeatOperation() {
		return new HeartbeatOperationImpl();
	}
	
	@Bean
	@Override
	public AuthorizationOperation getAuthorizationConfirmOperation() {
		return new AuthorizationOperationImpl();
	}
	
	@Bean
	@Override
	public StartTransactionOperation getStartTransactionOperation() {
		return new StartTransactionOperationImpl();
	}
	
	@Bean
	@Override
	public StopTransactionOperation getStopTransactionOperation() {
		return new StopTransactionOperationImpl();
	}
	
	@Bean
	@Override
	public CancelReservationOperation getCancelReservationOperation() {
		return new CancelReservationOperationImpl();
	}

	@Override
	public DiagnosticsStatusNotificationOperation getDiagnosticsStatusNotificationOperation() {
		return new DiagnosticsStatusNotificationOperationImpl();
	}

	@Override
	public FirmwareStatusNotificationOperation getFirmwareStatusNotificationOperation() {
		return new FirmwareStatusNotificationOperationImpl();
	}

	@Override
	public MeterValuesOperation getMeterValuesOperation() {
		return new MeterValuesOperationImpl();
	}
	
	@Bean
	@Override
	public StatusNotificationOperation getStatusNotificationOperation() {
		return new StatusNotificationOperationImpl();
	}
	
	@Bean
	@Override
	public RemoteStartTransactionOperation getRemoteStartTransactionOperation() {
		return new RemoteStartTransactionOperationImpl();
	}
	
	@Bean
	@Override
	public RemoteStopTransactionOperation getRemoteStopTransactionOperation() {
		return new RemoteStopTransactionOperationImpl();
	}
	
	@Bean
	@Override
	public ChangeAvailabilityOperation getChangeAvailabilityOperation() {
		return new ChangeAvailabilityOperationImpl();
	}
	
	@Bean
	@Override
	public SetChargingProfileOperation getSetChargingProfileOperation() {
		return new SetChargingProfileOperationImpl();
	}
	
	@Bean
	@Override
	public ChangeConfigurationOperation getChangeConfigurationOperation() {
		return new ChangeConfigurationOperationImpl();
	}
	
	@Bean
	@Override
	public ClearCacheOperation getClearCacheOperation() {
		return new ClearCacheOperationImpl();
	}
	
	@Bean
	@Override
	public ClearChargingProfileOperation getClearChargingProfileOperation() {
		return new ClearChargingProfileOperationImpl();
	}
	
	@Bean
	@Override
	public GetCompositeScheduleOperation getGetCompositeScheduleOperation() {
		return new GetCompositeScheduleOperationImpl();
	}
	
	@Bean
	@Override
	public GetConfigurationOperation getGetConfigurationOperation() {
		return new GetConfigurationOperationImpl();
	}
	
	@Bean
	@Override
	public GetDiagnosticsOperation getGetDiagnosticsOperation() {
		return new GetDiagnosticsOperationImpl();
	}
	
	@Bean
	@Override
	public GetLocalListVersionOperation getGetLocalListVersionOperation() {
		return new GetLocalListVersionOperationImpl();
	}
	
	@Bean
	@Override
	public ReserveNowOperation getReserveNowOperation() {
		return new ReserveNowOperationImpl();
	}
	
	@Bean
	@Override
	public ResetOperation getResetOperation() {
		return new ResetOperationImpl();
	}
	
	@Bean
	@Override
	public SendLocalListOperation getSendLocalListOperation() {
		return new SendLocalListOperationImpl();
	}
	
	@Bean
	@Override
	public TriggerMessageOperation getTriggerMessageOperation() {
		return new TriggerMessageOperationImpl();
	}
	
	@Bean
	@Override
	public UnlockConnectorOperation getUnlockConnectorOperation() {
		return new UnlockConnectorOperationImpl();
	}
	
	@Bean
	@Override
	public UpdateFirmwareOperation getUpdateFirmwareOperation() {
		return new UpdateFirmwareOperationImpl();
	}
}
