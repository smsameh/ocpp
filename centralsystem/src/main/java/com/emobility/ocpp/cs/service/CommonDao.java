package com.emobility.ocpp.cs.service;

import org.springframework.stereotype.Component;

import com.emobility.ocpp.cs.persistence.DaoBase;

/**
 * 
 * @author Salah Abu Msameh
 */
@Component
public class CommonDao extends DaoBase {

}
