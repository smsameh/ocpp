package com.emobility.ocpp.cs.service;

/**
 * 
 * @author Salah Abu Msameh
 */
public enum UserStatus {

	ACTIVE(1),
	INACTIVE(2),
	SUSPENDED(3);
	
	public int statusId;

	/**
	 * 
	 * @param statusId
	 */
	private UserStatus(int statusId) {
		this.statusId = statusId;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getStatusId() {
		return statusId;
	}
}
