package com.emobility.ocpp.cs.control;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.util.StringUtils;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class CentralSystemHandshakeInterceptor implements HandshakeInterceptor {
	
	@Autowired
	private ChargePointService cpSrv;
	
	@Override
	public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
			Map<String, Object> attributes) throws Exception {
		
		String uniqueIdentifier = Utils.extractConnectionUniqueIdentifier(request.getURI().toString());
		
		if(StringUtils.isEmpty(uniqueIdentifier)) {
			return false;
		}
		
		if(cpSrv.isRegistered(uniqueIdentifier)) {
			return true;
		}
		
		return false;
	}
	
	@Override
	public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {
		// TODO Auto-generated method stub
	}
}
