package com.emobility.ocpp.cs.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.MappedInterceptor;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import com.emobility.ocpp.cs.control.CentralSystemHandshakeInterceptor;
import com.emobility.ocpp.cs.control.CentralSystemMessageHandler;
import com.emobility.ocpp.cs.control.ThymeleafLayoutInterceptor;
import com.emobility.ocpp.cs.control.listener.CentralSystemContextListener;
import com.emobility.ocpp.profile.config.OcppConfig;

/**
 * 
 * @author Salah Abu Msameh
 */
@Configuration
public class AppBeanConfig implements OcppConfig {

	/**
	 * Register the sustem context listener
	 * @return
	 */
	@Bean
	public CentralSystemContextListener getCentralSystemContextListener() {
		return new CentralSystemContextListener();
	}
	
	/**
	 * Register handshake interceptor
	 */
	@Bean
	@Override
	public HandshakeInterceptor getHandshakeInterceptor() {
		return new CentralSystemHandshakeInterceptor();
	}
	
	/**
	 * 
	 */
	@Bean
	@Override
	public WebSocketHandler getWebSocketHandler() {
		return new CentralSystemMessageHandler();
	}
	
	/**
	 * Register thymeleaf layout interceptor
	 * @return
	 */
	@Bean
	public MappedInterceptor thymeleafLayoutInterceptor() {
		return new MappedInterceptor(null, new ThymeleafLayoutInterceptor());
	}
}
