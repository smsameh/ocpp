package com.emobility.ocpp.cs.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.emobility.ocpp.cs.presentation.Pages;
import com.emobility.ocpp.cs.service.EventsAuditorService;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
@RequestMapping("/admin")
public class EventsLogController extends BaseController {
	
	@Autowired
	private EventsAuditorService eventsSrv;
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("/events")
	public String getEventLogs(Model model) {
		
		model.addAttribute("events", eventsSrv.getEvents(null));
		model.addAttribute("operations", eventsSrv.getEventOperations());
		
		return Pages.EVENTS_LOG;
	}
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("/events/{operationId}")
	public String getEventLogs(Model model, @PathVariable("operationId") String operationId) {
		
		model.addAttribute("events", eventsSrv.getEvents(operationId));
		model.addAttribute("operations", eventsSrv.getEventOperations());
		
		return Pages.EVENTS_LOG;
	}
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("/events/view/{eventId}")
	public String viewEvent(Model model, @PathVariable("eventId") String eventId) {
		
		if(StringUtils.isEmpty(eventId)) {
			return Pages.EVENTS_LOG; 
		}
		
		model.addAttribute("event", eventsSrv.getEvent(Long.parseLong(eventId)));
		model.addAttribute("eventDetails", eventsSrv.getEventDetails(Long.parseLong(eventId)));
		
		return Pages.EVENT_DETAILS;
	}
}
