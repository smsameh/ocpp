package com.emobility.ocpp.cs.operation;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.cs.api.model.SetChargingProfileApiPayload;
import com.emobility.ocpp.cs.persistence.AuditOperations;
import com.emobility.ocpp.cs.persistence.entity.ChargePoint;
import com.emobility.ocpp.cs.service.ChargePointService;
import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.profile.operation.OperationActions;
import com.emobility.ocpp.profile.operation.SetChargingProfileOperation;
import com.emobility.ocpp.socket.message.Message;
import com.emobility.ocpp.socket.message.MessageTracker;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.socket.message.model.ChargingProfile;
import com.emobility.ocpp.socket.message.model.ChargingProfileKindType;
import com.emobility.ocpp.socket.message.model.ChargingProfilePurposeType;
import com.emobility.ocpp.socket.message.model.ChargingRateUnitType;
import com.emobility.ocpp.socket.message.model.ChargingSchedule;
import com.emobility.ocpp.socket.message.model.ChargingSchedulePeriod;
import com.emobility.ocpp.socket.message.model.SetChargingProfileRequestPayload;
import com.emobility.ocpp.util.Log;
import com.emobility.ocpp.util.Utils;

/**
 * 
 * @author Salah Abu Msameh
 */
public class SetChargingProfileOperationImpl extends SetChargingProfileOperation {
	
	@Autowired
	private ChargePointService cpSrv;
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageTracker msgTracker;
	
	@Override
	public void submitMessage(Message message) {
		
		if(!(message.getPayload() instanceof SetChargingProfileApiPayload)) {
			Log.error(RemoteStartTransactionOperationImpl.class, "Invalid payload type for [SetChargingProfile] operation");
			return;
		}
		
		SetChargingProfileApiPayload payload = (SetChargingProfileApiPayload) message.getPayload();
		
		String cpUniqueIdentifier = payload.getCpUniqueIdentifier();
		int connectorId = payload.getConnectorId();
		
		//1. validate charge point
		ChargePoint cp = cpSrv.getChargePoint(cpUniqueIdentifier);
		
		if(cp == null) {
			Log.error(RemoteStartTransactionOperationImpl.class, 
					"No charge point registered found for unique identifier [" + cpUniqueIdentifier + "]");
			return;
		}
		
		//1. create request message and payload
		SetChargingProfileRequestPayload remoteStartReq = new SetChargingProfileRequestPayload();
		
		remoteStartReq.setConnectorId(connectorId);
		remoteStartReq.setChargingProfile(getChargingProfile());
		
		String messageId = Utils.generateMessageId();
		
		RequestMessage reqMsg = new RequestMessage(messageId, OperationActions.SET_CHARGING_PROFILE, remoteStartReq);
		reqMsg.setSessionId(cpSrv.getLinkedSessionId(cpUniqueIdentifier));
		
		try {
			msgTransmitter.sendMessage(reqMsg);
			msgTracker.registerSubmittedMessage(messageId, reqMsg);
			
		} catch (Exception e) {
			Log.error(RemoteStartTransactionOperationImpl.class, e.getMessage());
			e.printStackTrace();
		}
		
		auditorSrv.audit(AuditOperations.SET_CHARGING_PROFILE_REQUEST, reqMsg);
	}

	@Override
	public void handleReceivedMessage(Message message) {
		
		auditorSrv.audit(AuditOperations.SET_CHARGING_PROFILE_CONFIRM, message);
		
		//remove message from tracker
		msgTracker.removeMessage(message.getUniqueId());
	}
	
	/**
	 * 
	 * @return
	 */
	private ChargingProfile getChargingProfile() {
		
		ChargingProfile chargingProfile = new ChargingProfile();
		
		chargingProfile.setChargingProfileId(1);
		chargingProfile.setChargingProfilePurpose(ChargingProfilePurposeType.TxProfile);
		chargingProfile.setChargingProfileKind(ChargingProfileKindType.Relative);
		
		ChargingSchedule chargeSchedule = new ChargingSchedule();
		
		chargeSchedule.setChargingRateUnit(ChargingRateUnitType.W);
		chargeSchedule.setChargingSchedulePeriod(new ChargingSchedulePeriod(100, 200, 0));
		
		chargingProfile.setChargingSchedule(chargeSchedule);
		
		return chargingProfile;
	}
}
