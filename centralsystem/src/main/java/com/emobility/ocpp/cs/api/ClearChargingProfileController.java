package com.emobility.ocpp.cs.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.emobility.ocpp.cs.api.model.ClearChargingProfileApiPayload;
import com.emobility.ocpp.profile.operation.ClearChargingProfileOperation;
import com.emobility.ocpp.socket.message.RequestMessage;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@Controller
public class ClearChargingProfileController extends ApiBaseController {

	@Autowired
	private ClearChargingProfileOperation clearChargingProfile;
	
	@GetMapping(path = "/clear-charging-profile/{cpUniqueIdentifier}/{connectiorId}")
	public void chargingConfiguration(@PathVariable("cpUniqueIdentifier") String cpUniqueIdentifier,
									  @PathVariable("connectiorId") String connectorId) {
		
		if(!isValidChargePointUniqueIdentifier(cpUniqueIdentifier)) {
			return;
		}
		
		int connId = 0;
		
		try {
			connId = Integer.parseInt(connectorId);
		} catch (NumberFormatException ex) {
			Log.error(ClearChargingProfileController.class, ex.getMessage());
			return;
		}
		
		//generate request message
		RequestMessage reqMsg = new RequestMessage(null, null, new ClearChargingProfileApiPayload(cpUniqueIdentifier, connId));
		clearChargingProfile.submitMessage(reqMsg);
	}
}
