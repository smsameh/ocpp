package com.emobility.ocpp.cs.api.model;

/**
 * 
 * @author Salah Abu Msameh
 */
public class GetDiagnosticsApiPayload extends ApiPayload {
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 */
	public GetDiagnosticsApiPayload(String cpUniqueIdentifier) {
		super(cpUniqueIdentifier);
	}
}
