package com.emobility.ocpp.cs.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.emobility.ocpp.cs.persistence.UserDao;
import com.emobility.ocpp.cs.persistence.UserRepository;
import com.emobility.ocpp.cs.persistence.entity.User;
import com.emobility.ocpp.cs.presentation.form.UserFB;
import com.emobility.ocpp.socket.message.model.AuthorizationStatus;

/**
 * CS user logic and actions
 * 
 * @author Salah Abu Msameh
 */
@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UserTransactionService userTransSrv;
	
	/**users cache*/
	private Map<String, User> users = new ConcurrentHashMap<String, User>();
	private boolean loaded = false;
	
	/**
	 * 
	 * @return
	 */
	public Collection<User> getAllUsers() {
		
		if(!loaded) {
			loadAllUsers();
		}
		
		return this.users.values();
	}

	/**
	 * 
	 * @param userId
	 * @return
	 */
	public User getUser(String userId) {
		
		//1. try cache
		User user = users.get(userId);
		
		if(user != null) {
			return user;
		}
		
		//2. try db
		user = userRepo.findOne(userId);
		
		//3. update cache or return null
		if(user != null) {
			users.put(user.getUserId(), user);
			return user;
		} else {
			return null;
		}
	}
	
	/**
	 * validate the given user
	 * @return
	 */
	public AuthorizationStatus Authorize(String userId) {
		return Authorize(userId, false);
	}

	/**
	 * validate the given user
	 * @return
	 */
	public AuthorizationStatus Authorize(String userId, boolean finishTrx) {
		
		User user = getUser(userId);
		
		//1. check if the user exist
		if(user == null) {
			return AuthorizationStatus.Invalid;
		}
		
		//2. check status
		if(user.getStatus() != UserStatus.ACTIVE.statusId) {
			return AuthorizationStatus.Blocked;
		}
		
		//3. check user expiry date
//		if(expired???) {
//			return AuthorizationStatus.EXPIRED;
//		}
		
		//4. check if he has already started and un-finished transaction
		if(!finishTrx && userTransSrv.hasActiveTransaction(userId)) {
			return AuthorizationStatus.ConcurrentTx;
		}
		
		return AuthorizationStatus.Accepted;
	}
	
	/**
	 * 
	 * @param userFB
	 * @return
	 */
	public boolean createUser(UserFB userFB) {
		
		User user = new User();
		
		user.setUserId(userFB.getUserId());
		user.setFullName(userFB.getFullName());
		user.setStatus(userFB.getStatus());
		user.setSubscriptionDate(new Date());
		user.setOrganizationId(userFB.getOrganizationId());
		user.setPaymentMethod(userFB.getPaymentMethod());
		user.setPaymentInfo(userFB.getPaymentInfo());
		
		User savedUser = userRepo.save(user);
		
		if(savedUser != null) {
			this.users.put(savedUser.getUserId(), savedUser);
			return true;
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param userFB
	 * @return
	 */
	public boolean updateUser(UserFB userFB) {
		
		User user = getUser(userFB.getUserId());
		
		//user.setUserId(userFB.getUserId());
		user.setFullName(userFB.getFullName());
		user.setStatus(userFB.getStatus());
		user.setOrganizationId(userFB.getOrganizationId());
		user.setPaymentMethod(userFB.getPaymentMethod());
		user.setPaymentInfo(userFB.getPaymentInfo());
		
		User savedUser = (User) userDao.update(user);
		
		if(savedUser != null) {
			this.users.put(savedUser.getUserId(), savedUser);
			return true;
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param userId
	 */
	public void deleteUser(String userId) {
		
		if(StringUtils.isEmpty(userId)) {
			return;
		}
		
		User user = getUser(userId);
		
		userDao.delete(user);
		this.users.remove(userId);
	}
	
	/**
	 * 
	 */
	private void loadAllUsers() {
		
		List<User> allUsers = (List<User>) userRepo.findAll();
		allUsers.forEach(user -> this.users.put(user.getUserId(), user));
		
		loaded = true;
	}
}
