package com.emobility.ocpp.cs.control.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.emobility.ocpp.cs.patch.DatabaseUpgradeManager;

/**
 * 
 * @author Salah Abu Msameh
 */
@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {
	
	@Autowired
	private DatabaseUpgradeManager dbUpgradeMgr;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		dbUpgradeMgr.upgrade();
	}
}
