package com.emobility.ocpp.cs.api;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
@RestController
@RequestMapping("/api")
public class ApiBaseController {
	
	/**
	 * 
	 * @param cpUniqueIdentifier
	 * @return true if a valuid charge point identifier, false otherwise
	 */
	public boolean isValidChargePointUniqueIdentifier(String cpUniqueIdentifier) {
		
		if(StringUtils.isEmpty(cpUniqueIdentifier)) {
			Log.error(ApiBaseController.class, "Invalid charge point unique identifier value");
			return false;
		}
		
		return true;
	}
}
