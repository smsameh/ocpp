package com.emobility.ocpp.cs.control.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.beans.factory.annotation.Autowired;

import com.emobility.ocpp.cs.service.EventsAuditorService;
import com.emobility.ocpp.socket.message.MessageProcessor;
import com.emobility.ocpp.util.Log;

/**
 * 
 * @author Salah Abu Msameh
 */
public class CentralSystemContextListener implements ServletContextListener {
	
	@Autowired
	private EventsAuditorService auditorSrv;
	
	@Autowired
	private MessageProcessor msgProcessor;
	
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
		//shoutdown executor threads 
		Log.info(CentralSystemContextListener.class, "Shutdown executor threads");
		auditorSrv.shutdown();
		msgProcessor.shutdown();
	}
}
